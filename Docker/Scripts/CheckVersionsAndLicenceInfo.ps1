﻿param(
    [String]$msmVersion="0.0.0.0",
    [String]$auditVersion="0.0.0.0",
    [String]$licencePath='C:\Config\licence.mlic',
    [String]$connectionPath='C:\Config\connectionStrings.config'
)

$ErrorActionPreference = 'Stop'
$needsUpgradeSqlCommand = 
@"
IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'upgradedEncryptionScheme' AND Object_ID = Object_ID(N'dbo.systemConfiguration'))
BEGIN
EXEC('SELECT upgradedEncryptionScheme FROM systemConfiguration')
END
ELSE
BEGIN
SELECT CAST(1 AS BIT) AS upgradedEncryptionScheme
END;
"@

function Split-ConnectionString([string]$connectionString) {
    $parts = $connectionString.Split(';')
    $server = $parts[0].Split('=')[1]
    $database = $parts[1].Split('=')[1]
    $user = $parts[2].Split('=')[1]
    $password = $parts[3].Split('=')[1]
    return @{ Server = $server; Name = $database; User = $user; Password = $password }
}

Import-Module SqlServer
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null

$xml = [xml](Get-Content -Path $connectionPath)
$databaseConnectionString = $xml.appSettings.Add[1].value
$auditConnectionString = $xml.appSettings.Add[3].value
$licenceData = Get-Content -Path $licencePath
$databaseConnectionObject = Split-ConnectionString -connectionString $databaseConnectionString
$auditConnectionObject = Split-ConnectionString -connectionString $auditConnectionString

try 
{
    $sqlCon = New-Object Microsoft.Data.SqlClient.SqlConnection($databaseConnectionString)
    $serverConn = New-Object Microsoft.SqlServer.Management.Common.ServerConnection($sqlCon)
    $msmServer = New-Object Microsoft.SqlServer.Management.SMO.Server($serverConn)
    $msmDatabase = New-Object Microsoft.SqlServer.Management.SMO.Database($msmServer, $databaseConnectionObject.Name)
    $asqlCon = New-Object Microsoft.Data.SqlClient.SqlConnection($auditConnectionString)
    $aserverConn = New-Object Microsoft.SqlServer.Management.Common.ServerConnection($asqlCon)
    $auditServer = New-Object Microsoft.SqlServer.Management.SMO.Server($aserverConn)
    $auditDatabase = New-Object Microsoft.SqlServer.Management.SMO.Database($auditServer, $auditConnectionObject.Name)

    while ($True) {
        try
        {
            $results = $msmDatabase.ExecuteWithResults("SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'databaseVersion'")
            $hasDatabaseVersionTable = $results.Tables[0].Rows.Count -gt 0;
            if (!$hasDatabaseVersionTable)
            {
                Write-Warning "MSM database has not been initialized."
                Start-Sleep -Seconds 5
                continue
            }

            $results = $msmDatabase.ExecuteWithResults("SELECT CONCAT(major, '.', minor, '.',  build, '.',  revision) AS version FROM databaseVersion")
            $msmDatabaseVersionMatch = $results.Tables[0].Rows[0].version -eq $msmVersion
            
            if (!$msmDatabaseVersionMatch) {
                Write-Warning "MSM database version does not match! Expected: $msmVersion. Was: $($results.Tables[0].Rows[0].version)"
                Start-Sleep -Seconds 5
                continue
            }
            
            $results = $msmDatabase.ExecuteWithResults("SELECT licence FROM systemConfiguration")
            $licenceMatch = $results.Tables[0].Rows[0].licence -eq $licenceData

            if (!$licenceMatch) {
                Write-Warning "Licence does not match!"
                Start-Sleep -Seconds 5
                continue
            }
            
            $results = $msmDatabase.ExecuteWithResults($needsUpgradeSqlCommand)
            $needsPasswordUpgrade = $results.Tables[0].Rows[0].upgradedEncryptionScheme -eq 0
            
            if ($needsPasswordUpgrade) {
                Write-Warning "Password encryption needs updating!"
                Start-Sleep -Seconds 5
                continue
            }

            $results = $auditDatabase.ExecuteWithResults("SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'databaseVersion'")
            $hasDatabaseVersionTable = $results.Tables[0].Rows.Count -gt 0;
            if (!$hasDatabaseVersionTable)
            {
                Write-Warning "MSM Audit database has not been initialized."
                Start-Sleep -Seconds 5
                continue
            }

            $results = $auditDatabase.ExecuteWithResults("SELECT CONCAT(major, '.', minor, '.',  build, '.',  revision) AS version FROM databaseVersion")
            $auditDatabaseVersionMatch = $results.Tables[0].Rows[0].version -eq $auditVersion

            if (!$auditDatabaseVersionMatch) {
                Write-Warning "Audit database version does not match! Expected: $auditVersion. Was: $($results.Tables[0].Rows[0].version)"
                Start-Sleep -Seconds 5
                continue
            }
        } 
        catch 
        {
            $_.Exception | Format-List -Force        
            Start-Sleep -Seconds 5
            continue
        }
        
        break;
    }
}
catch {
    $_.Exception | Format-List -Force
    throw $_.Exception
}  