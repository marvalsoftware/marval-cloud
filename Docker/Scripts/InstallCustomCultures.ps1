﻿
Add-Type -AssemblyName "sysglobl"

try
{    
    $newCulture = "aa-DJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Djibouti"
    $cib.RegionNativeName = "Yabuuti"
    $cib.ThreeLetterISORegionName = "DJI"
    $cib.TwoLetterISORegionName = "DJ"
    $cib.ThreeLetterWindowsRegionName = "DJI"
    $cib.CurrencyEnglishName = "Djiboutian Franc"
    $cib.CurrencyNativeName = "Djiboutian Franc"
    $cib.CultureEnglishName = "Afar (Djibouti)"
    $cib.CultureNativeName = "Qafar (Yabuuti)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "aar"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "aa"

    $cib.GregorianDateTimeFormat.AMDesignator = "saaku"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Fdj"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "aa-ER"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Eritrea"
    $cib.RegionNativeName = "Eretria"
    $cib.ThreeLetterISORegionName = "ERI"
    $cib.TwoLetterISORegionName = "ER"
    $cib.ThreeLetterWindowsRegionName = "ERI"
    $cib.CurrencyEnglishName = "Eritrean Nakfa"
    $cib.CurrencyNativeName = "Eritrean Nakfa"
    $cib.CultureEnglishName = "Afar (Eritrea)"
    $cib.CultureNativeName = "Qafar (Eretria)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "aar"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "aa"

    $cib.GregorianDateTimeFormat.AMDesignator = "saaku"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Nfk"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "aa-ET"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ethiopia"
    $cib.RegionNativeName = "Otobbia"
    $cib.ThreeLetterISORegionName = "ETH"
    $cib.TwoLetterISORegionName = "ET"
    $cib.ThreeLetterWindowsRegionName = "ETH"
    $cib.CurrencyEnglishName = "Ethiopian Birr"
    $cib.CurrencyNativeName = "Ethiopian Birr"
    $cib.CultureEnglishName = "Afar (Ethiopia)"
    $cib.CultureNativeName = "Qafar (Otobbia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "aar"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "aa"

    $cib.GregorianDateTimeFormat.AMDesignator = "saaku"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Br"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "af-NA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Namibia"
    $cib.RegionNativeName = "Namibië"
    $cib.ThreeLetterISORegionName = "NAM"
    $cib.TwoLetterISORegionName = "NA"
    $cib.ThreeLetterWindowsRegionName = "NAM"
    $cib.CurrencyEnglishName = "Namibian Dollar"
    $cib.CurrencyNativeName = "Namibiese dollar"
    $cib.CultureEnglishName = "Afrikaans (Namibia)"
    $cib.CultureNativeName = "Afrikaans (Namibië)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "afr"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "af"

    $cib.GregorianDateTimeFormat.AMDesignator = "vm."
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "agq-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kàmàlûŋ"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "CFA Fàlâŋ BEAC"
    $cib.CultureEnglishName = "Aghem (Cameroon)"
    $cib.CultureNativeName = "Aghem (Kàmàlûŋ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "agq"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "agq"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.g"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ak-GH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ghana"
    $cib.RegionNativeName = "Gaana"
    $cib.ThreeLetterISORegionName = "GHA"
    $cib.TwoLetterISORegionName = "GH"
    $cib.ThreeLetterWindowsRegionName = "GHA"
    $cib.CurrencyEnglishName = "Ghanaian Cedi"
    $cib.CurrencyNativeName = "Ghana Sidi"
    $cib.CultureEnglishName = "Akan (Ghana)"
    $cib.CultureNativeName = "Akan (Gaana)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "aka"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ak"

    $cib.GregorianDateTimeFormat.AMDesignator = "AN"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, yyyy MMMM dd h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, yyyy MMMM dd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy/MM/dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "GH₵"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-001"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "World"
    $cib.RegionNativeName = "العالم"
    $cib.ThreeLetterISORegionName = "001"
    $cib.TwoLetterISORegionName = "001"
    $cib.ThreeLetterWindowsRegionName = "001"
    $cib.CurrencyEnglishName = "Special Drawing Rights"
    $cib.CurrencyNativeName = "حقوق السحب الخاصة"
    $cib.CultureEnglishName = "Arabic (World)"
    $cib.CultureNativeName = "العربية (العالم)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "XDR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-DJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Djibouti"
    $cib.RegionNativeName = "جيبوتي"
    $cib.ThreeLetterISORegionName = "DJI"
    $cib.TwoLetterISORegionName = "DJ"
    $cib.ThreeLetterWindowsRegionName = "DJI"
    $cib.CurrencyEnglishName = "Djiboutian Franc"
    $cib.CurrencyNativeName = "فرنك جيبوتي"
    $cib.CultureEnglishName = "Arabic (Djibouti)"
    $cib.CultureNativeName = "العربية (جيبوتي)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "Fdj"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-ER"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Eritrea"
    $cib.RegionNativeName = "إريتريا"
    $cib.ThreeLetterISORegionName = "ERI"
    $cib.TwoLetterISORegionName = "ER"
    $cib.ThreeLetterWindowsRegionName = "ERI"
    $cib.CurrencyEnglishName = "Eritrean Nakfa"
    $cib.CurrencyNativeName = "ناكفا أريتري"
    $cib.CultureEnglishName = "Arabic (Eritrea)"
    $cib.CultureNativeName = "العربية (إريتريا)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "Nfk"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-IL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Israel"
    $cib.RegionNativeName = "إسرائيل"
    $cib.ThreeLetterISORegionName = "ISR"
    $cib.TwoLetterISORegionName = "IL"
    $cib.ThreeLetterWindowsRegionName = "ISR"
    $cib.CurrencyEnglishName = "Israeli New Shekel"
    $cib.CurrencyNativeName = "شيكل إسرائيلي جديد"
    $cib.CultureEnglishName = "Arabic (Israel)"
    $cib.CultureNativeName = "العربية (إسرائيل)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "₪"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-KM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Comoros"
    $cib.RegionNativeName = "جزر القمر"
    $cib.ThreeLetterISORegionName = "COM"
    $cib.TwoLetterISORegionName = "KM"
    $cib.ThreeLetterWindowsRegionName = "COM"
    $cib.CurrencyEnglishName = "Comorian Franc"
    $cib.CurrencyNativeName = "فرنك جزر القمر"
    $cib.CultureEnglishName = "Arabic (Comoros)"
    $cib.CultureNativeName = "العربية (جزر القمر)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "CF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-MR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mauritania"
    $cib.RegionNativeName = "موريتانيا"
    $cib.ThreeLetterISORegionName = "MRT"
    $cib.TwoLetterISORegionName = "MR"
    $cib.ThreeLetterWindowsRegionName = "MRT"
    $cib.CurrencyEnglishName = "Mauritanian Ouguiya"
    $cib.CurrencyNativeName = "أوقية موريتانية"
    $cib.CultureEnglishName = "Arabic (Mauritania)"
    $cib.CultureNativeName = "العربية (موريتانيا)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "أ.م.‏"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-PS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Palestinian Authority"
    $cib.RegionNativeName = "السلطة الفلسطينية"
    $cib.ThreeLetterISORegionName = "PSE"
    $cib.TwoLetterISORegionName = "PS"
    $cib.ThreeLetterWindowsRegionName = "PSE"
    $cib.CurrencyEnglishName = "Israeli New Shekel"
    $cib.CurrencyNativeName = "شيكل إسرائيلي جديد"
    $cib.CultureEnglishName = "Arabic (Palestinian Authority)"
    $cib.CultureNativeName = "العربية (السلطة الفلسطينية)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "₪"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-SD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Sudan"
    $cib.RegionNativeName = "السودان"
    $cib.ThreeLetterISORegionName = "SDN"
    $cib.TwoLetterISORegionName = "SD"
    $cib.ThreeLetterWindowsRegionName = "SDN"
    $cib.CurrencyEnglishName = "Sudanese Pound"
    $cib.CurrencyNativeName = "جنيه سوداني"
    $cib.CultureEnglishName = "Arabic (Sudan)"
    $cib.CultureNativeName = "العربية (السودان)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "ج.س."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-SO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Somalia"
    $cib.RegionNativeName = "الصومال"
    $cib.ThreeLetterISORegionName = "SOM"
    $cib.TwoLetterISORegionName = "SO"
    $cib.ThreeLetterWindowsRegionName = "SOM"
    $cib.CurrencyEnglishName = "Somali Shilling"
    $cib.CurrencyNativeName = "شلن صومالي"
    $cib.CultureEnglishName = "Arabic (Somalia)"
    $cib.CultureNativeName = "العربية (الصومال)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "S"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "٪"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-SS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "South Sudan"
    $cib.RegionNativeName = "جنوب السودان"
    $cib.ThreeLetterISORegionName = "SSD"
    $cib.TwoLetterISORegionName = "SS"
    $cib.ThreeLetterWindowsRegionName = "SSD"
    $cib.CurrencyEnglishName = "South Sudanese Pound"
    $cib.CurrencyNativeName = "جنيه جنوب السودان"
    $cib.CultureEnglishName = "Arabic (South Sudan)"
    $cib.CultureNativeName = "العربية (جنوب السودان)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ar-TD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Chad"
    $cib.RegionNativeName = "تشاد"
    $cib.ThreeLetterISORegionName = "TCD"
    $cib.TwoLetterISORegionName = "TD"
    $cib.ThreeLetterWindowsRegionName = "TCD"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "فرنك وسط أفريقي"
    $cib.CultureEnglishName = "Arabic (Chad)"
    $cib.CultureNativeName = "العربية (تشاد)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ara"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ar"

    $cib.GregorianDateTimeFormat.AMDesignator = "ص"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd، d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd، d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "asa-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tadhania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "shilingi ya Tandhania"
    $cib.CultureEnglishName = "Asu (Tanzania)"
    $cib.CultureNativeName = "Kipare (Tadhania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "asa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "asa"

    $cib.GregorianDateTimeFormat.AMDesignator = "icheheavo"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ast-ES"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Spain"
    $cib.RegionNativeName = "España"
    $cib.ThreeLetterISORegionName = "ESP"
    $cib.TwoLetterISORegionName = "ES"
    $cib.ThreeLetterWindowsRegionName = "ESP"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Asturian (Spain)"
    $cib.CultureNativeName = "asturianu (España)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ast"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ast"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "bas-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kàmɛ̀rûn"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Frǎŋ CFA (BEAC)"
    $cib.CultureEnglishName = "Basaa (Cameroon)"
    $cib.CultureNativeName = "Ɓàsàa (Kàmɛ̀rûn)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "bas"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "bas"

    $cib.GregorianDateTimeFormat.AMDesignator = "I bikɛ̂glà"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "bem-ZM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Zambia"
    $cib.RegionNativeName = "Zambia"
    $cib.ThreeLetterISORegionName = "ZMB"
    $cib.TwoLetterISORegionName = "ZM"
    $cib.ThreeLetterWindowsRegionName = "ZMB"
    $cib.CurrencyEnglishName = "Zambian Kwacha"
    $cib.CurrencyNativeName = "Zambian Kwacha"
    $cib.CultureEnglishName = "Bemba (Zambia)"
    $cib.CultureNativeName = "Ichibemba (Zambia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "bem"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "bem"

    $cib.GregorianDateTimeFormat.AMDesignator = "uluchelo"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "K"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "bez-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Hutanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Hutanzania"
    $cib.CultureEnglishName = "Bena (Tanzania)"
    $cib.CultureNativeName = "Hibena (Hutanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "bez"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "bez"

    $cib.GregorianDateTimeFormat.AMDesignator = "pamilau"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "bm"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Bermuda"
    $cib.RegionNativeName = "Bermuda"
    $cib.ThreeLetterISORegionName = "BMU"
    $cib.TwoLetterISORegionName = "BM"
    $cib.ThreeLetterWindowsRegionName = "BMU"
    $cib.CurrencyEnglishName = "Bermudan Dollar"
    $cib.CurrencyNativeName = "Bermudan Dollar"
    $cib.CultureEnglishName = "Bamanankan"
    $cib.CultureNativeName = "bamanakan"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "bam"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "bm"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "bm-Latn-ML"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mali"
    $cib.RegionNativeName = "Mali"
    $cib.ThreeLetterISORegionName = "MLI"
    $cib.TwoLetterISORegionName = "ML"
    $cib.ThreeLetterWindowsRegionName = "MLI"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "sefa Fraŋ (BCEAO)"
    $cib.CultureEnglishName = "Bamanankan (Latin, Mali)"
    $cib.CultureNativeName = "bamanakan (Mali)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "bam"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "bm"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "bo-IN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "India"
    $cib.RegionNativeName = "རྒྱ་གར་"
    $cib.ThreeLetterISORegionName = "IND"
    $cib.TwoLetterISORegionName = "IN"
    $cib.ThreeLetterWindowsRegionName = "IND"
    $cib.CurrencyEnglishName = "Indian Rupee"
    $cib.CurrencyNativeName = "རྒྱ་གར་སྒོར་"
    $cib.CultureEnglishName = "Tibetan (India)"
    $cib.CultureNativeName = "བོད་སྐད་ (རྒྱ་གར་)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "bod"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "bo"

    $cib.GregorianDateTimeFormat.AMDesignator = "སྔ་དྲོ་"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMMའི་ཚེས་d, dddd h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMMའི་ཚེས་d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMMའི་ཚེས་d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "₹"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "brx-IN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "India"
    $cib.RegionNativeName = "भारत"
    $cib.ThreeLetterISORegionName = "IND"
    $cib.TwoLetterISORegionName = "IN"
    $cib.ThreeLetterWindowsRegionName = "IND"
    $cib.CurrencyEnglishName = "Indian Rupee"
    $cib.CurrencyNativeName = "रां"
    $cib.CultureEnglishName = "Bodo (India)"
    $cib.CultureNativeName = "बड़ो (भारत)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "brx"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "brx"

    $cib.GregorianDateTimeFormat.AMDesignator = "फुं"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "₹"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "byn-ER"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Eritrea"
    $cib.RegionNativeName = "ኤርትራ"
    $cib.ThreeLetterISORegionName = "ERI"
    $cib.TwoLetterISORegionName = "ER"
    $cib.ThreeLetterWindowsRegionName = "ERI"
    $cib.CurrencyEnglishName = "Eritrean Nakfa"
    $cib.CurrencyNativeName = "Eritrean Nakfa"
    $cib.CultureEnglishName = "Blin (Eritrea)"
    $cib.CultureNativeName = "ብሊን (ኤርትራ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "byn"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "byn"

    $cib.GregorianDateTimeFormat.AMDesignator = "ፋዱስ ጃብ"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd፡ dd MMMM ግርጋ yyyy gg h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd፡ dd MMMM ግርጋ yyyy gg"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Nfk"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ca-AD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Andorra"
    $cib.RegionNativeName = "Andorra"
    $cib.ThreeLetterISORegionName = "AND"
    $cib.TwoLetterISORegionName = "AD"
    $cib.ThreeLetterWindowsRegionName = "AND"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Catalan (Andorra)"
    $cib.CultureNativeName = "català (Andorra)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "cat"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ca"

    $cib.GregorianDateTimeFormat.AMDesignator = "a. m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM 'de' yyyy H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ca-FR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "France"
    $cib.RegionNativeName = "França"
    $cib.ThreeLetterISORegionName = "FRA"
    $cib.TwoLetterISORegionName = "FR"
    $cib.ThreeLetterWindowsRegionName = "FRA"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Catalan (France)"
    $cib.CultureNativeName = "català (França)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "cat"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ca"

    $cib.GregorianDateTimeFormat.AMDesignator = "a. m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM 'de' yyyy H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ca-IT"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Italy"
    $cib.RegionNativeName = "Itàlia"
    $cib.ThreeLetterISORegionName = "ITA"
    $cib.TwoLetterISORegionName = "IT"
    $cib.ThreeLetterWindowsRegionName = "ITA"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Catalan (Italy)"
    $cib.CultureNativeName = "català (Itàlia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "cat"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ca"

    $cib.GregorianDateTimeFormat.AMDesignator = "a. m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM 'de' yyyy H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ce-RU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Russia"
    $cib.RegionNativeName = "Росси"
    $cib.ThreeLetterISORegionName = "RUS"
    $cib.TwoLetterISORegionName = "RU"
    $cib.ThreeLetterWindowsRegionName = "RUS"
    $cib.CurrencyEnglishName = "Russian Ruble"
    $cib.CurrencyNativeName = "Российн сом"
    $cib.CultureEnglishName = "Chechen (Russia)"
    $cib.CultureNativeName = "нохчийн (Росси)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "che"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ce"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "₽"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "cgg-UG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Uganda"
    $cib.RegionNativeName = "Uganda"
    $cib.ThreeLetterISORegionName = "UGA"
    $cib.TwoLetterISORegionName = "UG"
    $cib.ThreeLetterWindowsRegionName = "UGA"
    $cib.CurrencyEnglishName = "Ugandan Shilling"
    $cib.CurrencyNativeName = "Eshiringi ya Uganda"
    $cib.CultureEnglishName = "Chiga (Uganda)"
    $cib.CultureNativeName = "Rukiga (Uganda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "cgg"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "cgg"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "USh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "cu"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cuba"
    $cib.RegionNativeName = "Cuba"
    $cib.ThreeLetterISORegionName = "CUB"
    $cib.TwoLetterISORegionName = "CU"
    $cib.ThreeLetterWindowsRegionName = "CUB"
    $cib.CurrencyEnglishName = "Cuban Peso"
    $cib.CurrencyNativeName = "peso cubano"
    $cib.CultureEnglishName = "Church Slavic"
    $cib.CultureNativeName = "церковнослове́нскїй"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "chu"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "cu"

    $cib.GregorianDateTimeFormat.AMDesignator = "ДП"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM 'л'. yyyy. HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM 'л'. yyyy."
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy.MM.dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "₽"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "cu-RU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Russia"
    $cib.RegionNativeName = "рѡссі́а"
    $cib.ThreeLetterISORegionName = "RUS"
    $cib.TwoLetterISORegionName = "RU"
    $cib.ThreeLetterWindowsRegionName = "RUS"
    $cib.CurrencyEnglishName = "Russian Ruble"
    $cib.CurrencyNativeName = "рѡссі́йскїй рꙋ́бль"
    $cib.CultureEnglishName = "Church Slavic (Russia)"
    $cib.CultureNativeName = "церковнослове́нскїй (рѡссі́а)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "chu"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "cu"

    $cib.GregorianDateTimeFormat.AMDesignator = "ДП"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM 'л'. yyyy. HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM 'л'. yyyy."
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy.MM.dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "₽"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "da-GL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Greenland"
    $cib.RegionNativeName = "Grønland"
    $cib.ThreeLetterISORegionName = "GRL"
    $cib.TwoLetterISORegionName = "GL"
    $cib.ThreeLetterWindowsRegionName = "GRL"
    $cib.CurrencyEnglishName = "Danish Krone"
    $cib.CurrencyNativeName = "dansk krone"
    $cib.CultureEnglishName = "Danish (Greenland)"
    $cib.CultureNativeName = "dansk (Grønland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "dan"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "da"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd 'den' d. MMMM yyyy HH.mm.ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd 'den' d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH.mm.ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH.mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = "."
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "kr."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "dav-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Kenya"
    $cib.CultureEnglishName = "Taita (Kenya)"
    $cib.CultureNativeName = "Kitaita (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "dav"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "dav"

    $cib.GregorianDateTimeFormat.AMDesignator = "Luma lwa K"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "de-BE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Belgium"
    $cib.RegionNativeName = "Belgien"
    $cib.ThreeLetterISORegionName = "BEL"
    $cib.TwoLetterISORegionName = "BE"
    $cib.ThreeLetterWindowsRegionName = "BEL"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "German (Belgium)"
    $cib.CultureNativeName = "Deutsch (Belgien)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "deu"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "de"

    $cib.GregorianDateTimeFormat.AMDesignator = "vorm."
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "de-IT"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Italy"
    $cib.RegionNativeName = "Italien"
    $cib.ThreeLetterISORegionName = "ITA"
    $cib.TwoLetterISORegionName = "IT"
    $cib.ThreeLetterWindowsRegionName = "ITA"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "German (Italy)"
    $cib.CultureNativeName = "Deutsch (Italien)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "deu"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "de"

    $cib.GregorianDateTimeFormat.AMDesignator = "vorm."
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "dje-NE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Niger"
    $cib.RegionNativeName = "Nižer"
    $cib.ThreeLetterISORegionName = "NER"
    $cib.TwoLetterISORegionName = "NE"
    $cib.ThreeLetterWindowsRegionName = "NER"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "CFA Fraŋ (BCEAO)"
    $cib.CultureEnglishName = "Zarma (Niger)"
    $cib.CultureNativeName = "Zarmaciine (Nižer)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "dje"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "dje"

    $cib.GregorianDateTimeFormat.AMDesignator = "Subbaahi"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "dua-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Cameroun"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Central African CFA Franc"
    $cib.CultureEnglishName = "Duala (Cameroon)"
    $cib.CultureNativeName = "duálá (Cameroun)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "dua"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "dua"

    $cib.GregorianDateTimeFormat.AMDesignator = "idiɓa"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "dyo-SN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Senegal"
    $cib.RegionNativeName = "Senegal"
    $cib.ThreeLetterISORegionName = "SEN"
    $cib.TwoLetterISORegionName = "SN"
    $cib.ThreeLetterWindowsRegionName = "SEN"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "seefa yati BCEAO"
    $cib.CultureEnglishName = "Jola-Fonyi (Senegal)"
    $cib.CultureNativeName = "joola (Senegal)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "dyo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "dyo"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "dz"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Algeria"
    $cib.RegionNativeName = "الجزائر"
    $cib.ThreeLetterISORegionName = "DZA"
    $cib.TwoLetterISORegionName = "DZ"
    $cib.ThreeLetterWindowsRegionName = "DZA"
    $cib.CurrencyEnglishName = "Algerian Dinar"
    $cib.CurrencyNativeName = "دينار جزائري"
    $cib.CultureEnglishName = "Dzongkha"
    $cib.CultureNativeName = "རྫོང་ཁ"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "dzo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "dz"

    $cib.GregorianDateTimeFormat.AMDesignator = "སྔ་ཆ་"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, སྤྱི་ལོ་yyyy MMMM ཚེས་dd ཆུ་ཚོད་h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, སྤྱི་ལོ་yyyy MMMM ཚེས་dd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "ཆུ་ཚོད་h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "ཆུ་ཚོད་ h སྐར་མ་ mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Nu."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ebu-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Kenya"
    $cib.CultureEnglishName = "Embu (Kenya)"
    $cib.CultureNativeName = "Kĩembu (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ebu"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ebu"

    $cib.GregorianDateTimeFormat.AMDesignator = "KI"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ee"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Estonia"
    $cib.RegionNativeName = "Eesti"
    $cib.ThreeLetterISORegionName = "EST"
    $cib.TwoLetterISORegionName = "EE"
    $cib.ThreeLetterWindowsRegionName = "EST"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Ewe"
    $cib.CultureNativeName = "Eʋegbe"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ewe"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ee"

    $cib.GregorianDateTimeFormat.AMDesignator = "ŋdi"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d 'lia' yyyy tt 'ga' h:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d 'lia' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tt 'ga' h:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d 'lia'"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tt 'ga' h:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "GH₵"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ee-GH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ghana"
    $cib.RegionNativeName = "Ghana nutome"
    $cib.ThreeLetterISORegionName = "GHA"
    $cib.TwoLetterISORegionName = "GH"
    $cib.ThreeLetterWindowsRegionName = "GHA"
    $cib.CurrencyEnglishName = "Ghanaian Cedi"
    $cib.CurrencyNativeName = "ghana siɖi"
    $cib.CultureEnglishName = "Ewe (Ghana)"
    $cib.CultureNativeName = "Eʋegbe (Ghana nutome)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ewe"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ee"

    $cib.GregorianDateTimeFormat.AMDesignator = "ŋdi"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d 'lia' yyyy tt 'ga' h:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d 'lia' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tt 'ga' h:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d 'lia'"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tt 'ga' h:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "GH₵"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ee-TG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Togo"
    $cib.RegionNativeName = "Togo nutome"
    $cib.ThreeLetterISORegionName = "TGO"
    $cib.TwoLetterISORegionName = "TG"
    $cib.ThreeLetterWindowsRegionName = "TGO"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "ɣetoɖofe afrikaga CFA franc BCEAO"
    $cib.CultureEnglishName = "Ewe (Togo)"
    $cib.CultureNativeName = "Eʋegbe (Togo nutome)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ewe"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ee"

    $cib.GregorianDateTimeFormat.AMDesignator = "ŋdi"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d 'lia' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d 'lia' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d 'lia'"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "el-CY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cyprus"
    $cib.RegionNativeName = "Κύπρος"
    $cib.ThreeLetterISORegionName = "CYP"
    $cib.TwoLetterISORegionName = "CY"
    $cib.ThreeLetterWindowsRegionName = "CYP"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Ευρώ"
    $cib.CultureEnglishName = "Greek (Cyprus)"
    $cib.CultureNativeName = "Ελληνικά (Κύπρος)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ell"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "el"

    $cib.GregorianDateTimeFormat.AMDesignator = "π.μ."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-001"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "World"
    $cib.RegionNativeName = "World"
    $cib.ThreeLetterISORegionName = "001"
    $cib.TwoLetterISORegionName = "001"
    $cib.ThreeLetterWindowsRegionName = "001"
    $cib.CurrencyEnglishName = "Special Drawing Rights"
    $cib.CurrencyNativeName = "Special Drawing Rights"
    $cib.CultureEnglishName = "English (World)"
    $cib.CultureNativeName = "English (World)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "XDR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-150"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Europe"
    $cib.RegionNativeName = "Europe"
    $cib.ThreeLetterISORegionName = "150"
    $cib.TwoLetterISORegionName = "150"
    $cib.ThreeLetterWindowsRegionName = "150"
    $cib.CurrencyEnglishName = "Special Drawing Rights"
    $cib.CurrencyNativeName = "Special Drawing Rights"
    $cib.CultureEnglishName = "English (Europe)"
    $cib.CultureNativeName = "English (Europe)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "XDR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-AG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Antigua and Barbuda"
    $cib.RegionNativeName = "Antigua and Barbuda"
    $cib.ThreeLetterISORegionName = "ATG"
    $cib.TwoLetterISORegionName = "AG"
    $cib.ThreeLetterWindowsRegionName = "ATG"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Antigua and Barbuda)"
    $cib.CultureNativeName = "English (Antigua and Barbuda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-AI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Anguilla"
    $cib.RegionNativeName = "Anguilla"
    $cib.ThreeLetterISORegionName = "AIA"
    $cib.TwoLetterISORegionName = "AI"
    $cib.ThreeLetterWindowsRegionName = "AIA"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Anguilla)"
    $cib.CultureNativeName = "English (Anguilla)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-AS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "American Samoa"
    $cib.RegionNativeName = "American Samoa"
    $cib.ThreeLetterISORegionName = "ASM"
    $cib.TwoLetterISORegionName = "AS"
    $cib.ThreeLetterWindowsRegionName = "ASM"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (American Samoa)"
    $cib.CultureNativeName = "English (American Samoa)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-AT"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Austria"
    $cib.RegionNativeName = "Austria"
    $cib.ThreeLetterISORegionName = "AUT"
    $cib.TwoLetterISORegionName = "AT"
    $cib.ThreeLetterWindowsRegionName = "AUT"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Austria)"
    $cib.CultureNativeName = "English (Austria)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-BB"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Barbados"
    $cib.RegionNativeName = "Barbados"
    $cib.ThreeLetterISORegionName = "BRB"
    $cib.TwoLetterISORegionName = "BB"
    $cib.ThreeLetterWindowsRegionName = "BRB"
    $cib.CurrencyEnglishName = "Barbadian Dollar"
    $cib.CurrencyNativeName = "Barbadian Dollar"
    $cib.CultureEnglishName = "English (Barbados)"
    $cib.CultureNativeName = "English (Barbados)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-BE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Belgium"
    $cib.RegionNativeName = "Belgium"
    $cib.ThreeLetterISORegionName = "BEL"
    $cib.TwoLetterISORegionName = "BE"
    $cib.ThreeLetterWindowsRegionName = "BEL"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Belgium)"
    $cib.CultureNativeName = "English (Belgium)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-BI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Burundi"
    $cib.RegionNativeName = "Burundi"
    $cib.ThreeLetterISORegionName = "BDI"
    $cib.TwoLetterISORegionName = "BI"
    $cib.ThreeLetterWindowsRegionName = "BDI"
    $cib.CurrencyEnglishName = "Burundian Franc"
    $cib.CurrencyNativeName = "Burundian Franc"
    $cib.CultureEnglishName = "English (Burundi)"
    $cib.CultureNativeName = "English (Burundi)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "FBu"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-BM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Bermuda"
    $cib.RegionNativeName = "Bermuda"
    $cib.ThreeLetterISORegionName = "BMU"
    $cib.TwoLetterISORegionName = "BM"
    $cib.ThreeLetterWindowsRegionName = "BMU"
    $cib.CurrencyEnglishName = "Bermudan Dollar"
    $cib.CurrencyNativeName = "Bermudan Dollar"
    $cib.CultureEnglishName = "English (Bermuda)"
    $cib.CultureNativeName = "English (Bermuda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-BS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Bahamas"
    $cib.RegionNativeName = "Bahamas"
    $cib.ThreeLetterISORegionName = "BHS"
    $cib.TwoLetterISORegionName = "BS"
    $cib.ThreeLetterWindowsRegionName = "BHS"
    $cib.CurrencyEnglishName = "Bahamian Dollar"
    $cib.CurrencyNativeName = "Bahamian Dollar"
    $cib.CultureEnglishName = "English (Bahamas)"
    $cib.CultureNativeName = "English (Bahamas)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-BW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Botswana"
    $cib.RegionNativeName = "Botswana"
    $cib.ThreeLetterISORegionName = "BWA"
    $cib.TwoLetterISORegionName = "BW"
    $cib.ThreeLetterWindowsRegionName = "BWA"
    $cib.CurrencyEnglishName = "Botswanan Pula"
    $cib.CurrencyNativeName = "Botswanan Pula"
    $cib.CultureEnglishName = "English (Botswana)"
    $cib.CultureNativeName = "English (Botswana)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, dd MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, dd MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "P"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-CC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cocos (Keeling) Islands"
    $cib.RegionNativeName = "Cocos (Keeling) Islands"
    $cib.ThreeLetterISORegionName = "CCK"
    $cib.TwoLetterISORegionName = "CC"
    $cib.ThreeLetterWindowsRegionName = "CCK"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "English (Cocos (Keeling) Islands)"
    $cib.CultureNativeName = "English (Cocos (Keeling) Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-CH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Switzerland"
    $cib.RegionNativeName = "Switzerland"
    $cib.ThreeLetterISORegionName = "CHE"
    $cib.TwoLetterISORegionName = "CH"
    $cib.ThreeLetterWindowsRegionName = "CHE"
    $cib.CurrencyEnglishName = "Swiss Franc"
    $cib.CurrencyNativeName = "Swiss Franc"
    $cib.CultureEnglishName = "English (Switzerland)"
    $cib.CultureNativeName = "English (Switzerland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 2
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "CHF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-CK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cook Islands"
    $cib.RegionNativeName = "Cook Islands"
    $cib.ThreeLetterISORegionName = "COK"
    $cib.TwoLetterISORegionName = "CK"
    $cib.ThreeLetterWindowsRegionName = "COK"
    $cib.CurrencyEnglishName = "New Zealand Dollar"
    $cib.CurrencyNativeName = "New Zealand Dollar"
    $cib.CultureEnglishName = "English (Cook Islands)"
    $cib.CultureNativeName = "English (Cook Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Cameroon"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Central African CFA Franc"
    $cib.CultureEnglishName = "English (Cameroon)"
    $cib.CultureNativeName = "English (Cameroon)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-CX"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Christmas Island"
    $cib.RegionNativeName = "Christmas Island"
    $cib.ThreeLetterISORegionName = "CXR"
    $cib.TwoLetterISORegionName = "CX"
    $cib.ThreeLetterWindowsRegionName = "CXR"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "English (Christmas Island)"
    $cib.CultureNativeName = "English (Christmas Island)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-CY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cyprus"
    $cib.RegionNativeName = "Cyprus"
    $cib.ThreeLetterISORegionName = "CYP"
    $cib.TwoLetterISORegionName = "CY"
    $cib.ThreeLetterWindowsRegionName = "CYP"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Cyprus)"
    $cib.CultureNativeName = "English (Cyprus)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-DE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Germany"
    $cib.RegionNativeName = "Germany"
    $cib.ThreeLetterISORegionName = "DEU"
    $cib.TwoLetterISORegionName = "DE"
    $cib.ThreeLetterWindowsRegionName = "DEU"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Germany)"
    $cib.CultureNativeName = "English (Germany)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-DK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Denmark"
    $cib.RegionNativeName = "Denmark"
    $cib.ThreeLetterISORegionName = "DNK"
    $cib.TwoLetterISORegionName = "DK"
    $cib.ThreeLetterWindowsRegionName = "DNK"
    $cib.CurrencyEnglishName = "Danish Krone"
    $cib.CurrencyNativeName = "Danish Krone"
    $cib.CultureEnglishName = "English (Denmark)"
    $cib.CultureNativeName = "English (Denmark)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH.mm.ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH.mm.ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH.mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = "."
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "kr."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-DM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Dominica"
    $cib.RegionNativeName = "Dominica"
    $cib.ThreeLetterISORegionName = "DMA"
    $cib.TwoLetterISORegionName = "DM"
    $cib.ThreeLetterWindowsRegionName = "DMA"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Dominica)"
    $cib.CultureNativeName = "English (Dominica)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-ER"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Eritrea"
    $cib.RegionNativeName = "Eritrea"
    $cib.ThreeLetterISORegionName = "ERI"
    $cib.TwoLetterISORegionName = "ER"
    $cib.ThreeLetterWindowsRegionName = "ERI"
    $cib.CurrencyEnglishName = "Eritrean Nakfa"
    $cib.CurrencyNativeName = "Eritrean Nakfa"
    $cib.CultureEnglishName = "English (Eritrea)"
    $cib.CultureNativeName = "English (Eritrea)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Nfk"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-FI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Finland"
    $cib.RegionNativeName = "Finland"
    $cib.ThreeLetterISORegionName = "FIN"
    $cib.TwoLetterISORegionName = "FI"
    $cib.ThreeLetterWindowsRegionName = "FIN"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Finland)"
    $cib.CultureNativeName = "English (Finland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy H.mm.ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H.mm.ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H.mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = "."
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-FJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Fiji"
    $cib.RegionNativeName = "Fiji"
    $cib.ThreeLetterISORegionName = "FJI"
    $cib.TwoLetterISORegionName = "FJ"
    $cib.ThreeLetterWindowsRegionName = "FJI"
    $cib.CurrencyEnglishName = "Fijian Dollar"
    $cib.CurrencyNativeName = "Fijian Dollar"
    $cib.CultureEnglishName = "English (Fiji)"
    $cib.CultureNativeName = "English (Fiji)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-FK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Falkland Islands"
    $cib.RegionNativeName = "Falkland Islands"
    $cib.ThreeLetterISORegionName = "FLK"
    $cib.TwoLetterISORegionName = "FK"
    $cib.ThreeLetterWindowsRegionName = "FLK"
    $cib.CurrencyEnglishName = "Falkland Islands Pound"
    $cib.CurrencyNativeName = "Falkland Islands Pound"
    $cib.CultureEnglishName = "English (Falkland Islands)"
    $cib.CultureNativeName = "English (Falkland Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-FM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Micronesia"
    $cib.RegionNativeName = "Micronesia"
    $cib.ThreeLetterISORegionName = "FSM"
    $cib.TwoLetterISORegionName = "FM"
    $cib.ThreeLetterWindowsRegionName = "FSM"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (Micronesia)"
    $cib.CultureNativeName = "English (Micronesia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "US$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-GD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Grenada"
    $cib.RegionNativeName = "Grenada"
    $cib.ThreeLetterISORegionName = "GRD"
    $cib.TwoLetterISORegionName = "GD"
    $cib.ThreeLetterWindowsRegionName = "GRD"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Grenada)"
    $cib.CultureNativeName = "English (Grenada)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-GG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guernsey"
    $cib.RegionNativeName = "Guernsey"
    $cib.ThreeLetterISORegionName = "GGY"
    $cib.TwoLetterISORegionName = "GG"
    $cib.ThreeLetterWindowsRegionName = "GGY"
    $cib.CurrencyEnglishName = "British Pound"
    $cib.CurrencyNativeName = "UK Pound"
    $cib.CultureEnglishName = "English (Guernsey)"
    $cib.CultureNativeName = "English (Guernsey)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-GH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ghana"
    $cib.RegionNativeName = "Ghana"
    $cib.ThreeLetterISORegionName = "GHA"
    $cib.TwoLetterISORegionName = "GH"
    $cib.ThreeLetterWindowsRegionName = "GHA"
    $cib.CurrencyEnglishName = "Ghanaian Cedi"
    $cib.CurrencyNativeName = "Ghanaian Cedi"
    $cib.CultureEnglishName = "English (Ghana)"
    $cib.CultureNativeName = "English (Ghana)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "GH₵"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-GI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Gibraltar"
    $cib.RegionNativeName = "Gibraltar"
    $cib.ThreeLetterISORegionName = "GIB"
    $cib.TwoLetterISORegionName = "GI"
    $cib.ThreeLetterWindowsRegionName = "GIB"
    $cib.CurrencyEnglishName = "Gibraltar Pound"
    $cib.CurrencyNativeName = "Gibraltar Pound"
    $cib.CultureEnglishName = "English (Gibraltar)"
    $cib.CultureNativeName = "English (Gibraltar)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-GM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Gambia"
    $cib.RegionNativeName = "Gambia"
    $cib.ThreeLetterISORegionName = "GMB"
    $cib.TwoLetterISORegionName = "GM"
    $cib.ThreeLetterWindowsRegionName = "GMB"
    $cib.CurrencyEnglishName = "Gambian Dalasi"
    $cib.CurrencyNativeName = "Gambian Dalasi"
    $cib.CultureEnglishName = "English (Gambia)"
    $cib.CultureNativeName = "English (Gambia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "D"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-GU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guam"
    $cib.RegionNativeName = "Guam"
    $cib.ThreeLetterISORegionName = "GUM"
    $cib.TwoLetterISORegionName = "GU"
    $cib.ThreeLetterWindowsRegionName = "GUM"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (Guam)"
    $cib.CultureNativeName = "English (Guam)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-GY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guyana"
    $cib.RegionNativeName = "Guyana"
    $cib.ThreeLetterISORegionName = "GUY"
    $cib.TwoLetterISORegionName = "GY"
    $cib.ThreeLetterWindowsRegionName = "GUY"
    $cib.CurrencyEnglishName = "Guyanaese Dollar"
    $cib.CurrencyNativeName = "Guyanaese Dollar"
    $cib.CultureEnglishName = "English (Guyana)"
    $cib.CultureNativeName = "English (Guyana)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-IL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Israel"
    $cib.RegionNativeName = "Israel"
    $cib.ThreeLetterISORegionName = "ISR"
    $cib.TwoLetterISORegionName = "IL"
    $cib.ThreeLetterWindowsRegionName = "ISR"
    $cib.CurrencyEnglishName = "Israeli New Shekel"
    $cib.CurrencyNativeName = "Israeli New Shekel"
    $cib.CultureEnglishName = "English (Israel)"
    $cib.CultureNativeName = "English (Israel)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "₪"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-IM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Isle of Man"
    $cib.RegionNativeName = "Isle of Man"
    $cib.ThreeLetterISORegionName = "IMN"
    $cib.TwoLetterISORegionName = "IM"
    $cib.ThreeLetterWindowsRegionName = "IMN"
    $cib.CurrencyEnglishName = "British Pound"
    $cib.CurrencyNativeName = "UK Pound"
    $cib.CultureEnglishName = "English (Isle of Man)"
    $cib.CultureNativeName = "English (Isle of Man)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-IO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "British Indian Ocean Territory"
    $cib.RegionNativeName = "British Indian Ocean Territory"
    $cib.ThreeLetterISORegionName = "IOT"
    $cib.TwoLetterISORegionName = "IO"
    $cib.ThreeLetterWindowsRegionName = "IOT"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (British Indian Ocean Territory)"
    $cib.CultureNativeName = "English (British Indian Ocean Territory)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "US$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-JE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Jersey"
    $cib.RegionNativeName = "Jersey"
    $cib.ThreeLetterISORegionName = "JEY"
    $cib.TwoLetterISORegionName = "JE"
    $cib.ThreeLetterWindowsRegionName = "JEY"
    $cib.CurrencyEnglishName = "British Pound"
    $cib.CurrencyNativeName = "UK Pound"
    $cib.CultureEnglishName = "English (Jersey)"
    $cib.CultureNativeName = "English (Jersey)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Kenyan Shilling"
    $cib.CultureEnglishName = "English (Kenya)"
    $cib.CultureNativeName = "English (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-KI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kiribati"
    $cib.RegionNativeName = "Kiribati"
    $cib.ThreeLetterISORegionName = "KIR"
    $cib.TwoLetterISORegionName = "KI"
    $cib.ThreeLetterWindowsRegionName = "KIR"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "English (Kiribati)"
    $cib.CultureNativeName = "English (Kiribati)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-KN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Saint Kitts and Nevis"
    $cib.RegionNativeName = "Saint Kitts and Nevis"
    $cib.ThreeLetterISORegionName = "KNA"
    $cib.TwoLetterISORegionName = "KN"
    $cib.ThreeLetterWindowsRegionName = "KNA"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Saint Kitts and Nevis)"
    $cib.CultureNativeName = "English (Saint Kitts and Nevis)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-KY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cayman Islands"
    $cib.RegionNativeName = "Cayman Islands"
    $cib.ThreeLetterISORegionName = "CYM"
    $cib.TwoLetterISORegionName = "KY"
    $cib.ThreeLetterWindowsRegionName = "CYM"
    $cib.CurrencyEnglishName = "Cayman Islands Dollar"
    $cib.CurrencyNativeName = "Cayman Islands Dollar"
    $cib.CultureEnglishName = "English (Cayman Islands)"
    $cib.CultureNativeName = "English (Cayman Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-LC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Saint Lucia"
    $cib.RegionNativeName = "Saint Lucia"
    $cib.ThreeLetterISORegionName = "LCA"
    $cib.TwoLetterISORegionName = "LC"
    $cib.ThreeLetterWindowsRegionName = "LCA"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Saint Lucia)"
    $cib.CultureNativeName = "English (Saint Lucia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-LR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Liberia"
    $cib.RegionNativeName = "Liberia"
    $cib.ThreeLetterISORegionName = "LBR"
    $cib.TwoLetterISORegionName = "LR"
    $cib.ThreeLetterWindowsRegionName = "LBR"
    $cib.CurrencyEnglishName = "Liberian Dollar"
    $cib.CurrencyNativeName = "Liberian Dollar"
    $cib.CultureEnglishName = "English (Liberia)"
    $cib.CultureNativeName = "English (Liberia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-LS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Lesotho"
    $cib.RegionNativeName = "Lesotho"
    $cib.ThreeLetterISORegionName = "LSO"
    $cib.TwoLetterISORegionName = "LS"
    $cib.ThreeLetterWindowsRegionName = "LSO"
    $cib.CurrencyEnglishName = "South African Rand"
    $cib.CurrencyNativeName = "South African Rand"
    $cib.CultureEnglishName = "English (Lesotho)"
    $cib.CultureNativeName = "English (Lesotho)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "R"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Madagascar"
    $cib.RegionNativeName = "Madagascar"
    $cib.ThreeLetterISORegionName = "MDG"
    $cib.TwoLetterISORegionName = "MG"
    $cib.ThreeLetterWindowsRegionName = "MDG"
    $cib.CurrencyEnglishName = "Malagasy Ariary"
    $cib.CurrencyNativeName = "Malagasy Ariary"
    $cib.CultureEnglishName = "English (Madagascar)"
    $cib.CultureNativeName = "English (Madagascar)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ar"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Marshall Islands"
    $cib.RegionNativeName = "Marshall Islands"
    $cib.ThreeLetterISORegionName = "MHL"
    $cib.TwoLetterISORegionName = "MH"
    $cib.ThreeLetterWindowsRegionName = "MHL"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (Marshall Islands)"
    $cib.CultureNativeName = "English (Marshall Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Macao SAR"
    $cib.RegionNativeName = "Macao SAR"
    $cib.ThreeLetterISORegionName = "MAC"
    $cib.TwoLetterISORegionName = "MO"
    $cib.ThreeLetterWindowsRegionName = "MAC"
    $cib.CurrencyEnglishName = "Macanese Pataca"
    $cib.CurrencyNativeName = "Macanese Pataca"
    $cib.CultureEnglishName = "English (Macao SAR)"
    $cib.CultureNativeName = "English (Macao SAR)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "MOP$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MP"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Northern Mariana Islands"
    $cib.RegionNativeName = "Northern Mariana Islands"
    $cib.ThreeLetterISORegionName = "MNP"
    $cib.TwoLetterISORegionName = "MP"
    $cib.ThreeLetterWindowsRegionName = "MNP"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (Northern Mariana Islands)"
    $cib.CultureNativeName = "English (Northern Mariana Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Montserrat"
    $cib.RegionNativeName = "Montserrat"
    $cib.ThreeLetterISORegionName = "MSR"
    $cib.TwoLetterISORegionName = "MS"
    $cib.ThreeLetterWindowsRegionName = "MSR"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Montserrat)"
    $cib.CultureNativeName = "English (Montserrat)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MT"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Malta"
    $cib.RegionNativeName = "Malta"
    $cib.ThreeLetterISORegionName = "MLT"
    $cib.TwoLetterISORegionName = "MT"
    $cib.ThreeLetterWindowsRegionName = "MLT"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Malta)"
    $cib.CultureNativeName = "English (Malta)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mauritius"
    $cib.RegionNativeName = "Mauritius"
    $cib.ThreeLetterISORegionName = "MUS"
    $cib.TwoLetterISORegionName = "MU"
    $cib.ThreeLetterWindowsRegionName = "MUS"
    $cib.CurrencyEnglishName = "Mauritian Rupee"
    $cib.CurrencyNativeName = "Mauritian Rupee"
    $cib.CultureEnglishName = "English (Mauritius)"
    $cib.CultureNativeName = "English (Mauritius)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Rs"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-MW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Malawi"
    $cib.RegionNativeName = "Malawi"
    $cib.ThreeLetterISORegionName = "MWI"
    $cib.TwoLetterISORegionName = "MW"
    $cib.ThreeLetterWindowsRegionName = "MWI"
    $cib.CurrencyEnglishName = "Malawian Kwacha"
    $cib.CurrencyNativeName = "Malawian Kwacha"
    $cib.CultureEnglishName = "English (Malawi)"
    $cib.CultureNativeName = "English (Malawi)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "MK"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-NA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Namibia"
    $cib.RegionNativeName = "Namibia"
    $cib.ThreeLetterISORegionName = "NAM"
    $cib.TwoLetterISORegionName = "NA"
    $cib.ThreeLetterWindowsRegionName = "NAM"
    $cib.CurrencyEnglishName = "Namibian Dollar"
    $cib.CurrencyNativeName = "Namibian Dollar"
    $cib.CultureEnglishName = "English (Namibia)"
    $cib.CultureNativeName = "English (Namibia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-NF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Norfolk Island"
    $cib.RegionNativeName = "Norfolk Island"
    $cib.ThreeLetterISORegionName = "NFK"
    $cib.TwoLetterISORegionName = "NF"
    $cib.ThreeLetterWindowsRegionName = "NFK"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "English (Norfolk Island)"
    $cib.CultureNativeName = "English (Norfolk Island)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-NG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Nigeria"
    $cib.RegionNativeName = "Nigeria"
    $cib.ThreeLetterISORegionName = "NGA"
    $cib.TwoLetterISORegionName = "NG"
    $cib.ThreeLetterWindowsRegionName = "NGA"
    $cib.CurrencyEnglishName = "Nigerian Naira"
    $cib.CurrencyNativeName = "Nigerian Naira"
    $cib.CultureEnglishName = "English (Nigeria)"
    $cib.CultureNativeName = "English (Nigeria)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "₦"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-NL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Netherlands"
    $cib.RegionNativeName = "Netherlands"
    $cib.ThreeLetterISORegionName = "NLD"
    $cib.TwoLetterISORegionName = "NL"
    $cib.ThreeLetterWindowsRegionName = "NLD"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Netherlands)"
    $cib.CultureNativeName = "English (Netherlands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 12
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-NR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Nauru"
    $cib.RegionNativeName = "Nauru"
    $cib.ThreeLetterISORegionName = "NRU"
    $cib.TwoLetterISORegionName = "NR"
    $cib.ThreeLetterWindowsRegionName = "NRU"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "English (Nauru)"
    $cib.CultureNativeName = "English (Nauru)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-NU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Niue"
    $cib.RegionNativeName = "Niue"
    $cib.ThreeLetterISORegionName = "NIU"
    $cib.TwoLetterISORegionName = "NU"
    $cib.ThreeLetterWindowsRegionName = "NIU"
    $cib.CurrencyEnglishName = "New Zealand Dollar"
    $cib.CurrencyNativeName = "New Zealand Dollar"
    $cib.CultureEnglishName = "English (Niue)"
    $cib.CultureNativeName = "English (Niue)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-PG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Papua New Guinea"
    $cib.RegionNativeName = "Papua New Guinea"
    $cib.ThreeLetterISORegionName = "PNG"
    $cib.TwoLetterISORegionName = "PG"
    $cib.ThreeLetterWindowsRegionName = "PNG"
    $cib.CurrencyEnglishName = "Papua New Guinean Kina"
    $cib.CurrencyNativeName = "Papua New Guinean Kina"
    $cib.CultureEnglishName = "English (Papua New Guinea)"
    $cib.CultureNativeName = "English (Papua New Guinea)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "K"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-PK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Pakistan"
    $cib.RegionNativeName = "Pakistan"
    $cib.ThreeLetterISORegionName = "PAK"
    $cib.TwoLetterISORegionName = "PK"
    $cib.ThreeLetterWindowsRegionName = "PAK"
    $cib.CurrencyEnglishName = "Pakistani Rupee"
    $cib.CurrencyNativeName = "Pakistani Rupee"
    $cib.CultureEnglishName = "English (Pakistan)"
    $cib.CultureNativeName = "English (Pakistan)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Rs"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-PN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Pitcairn Islands"
    $cib.RegionNativeName = "Pitcairn Islands"
    $cib.ThreeLetterISORegionName = "PCN"
    $cib.TwoLetterISORegionName = "PN"
    $cib.ThreeLetterWindowsRegionName = "PCN"
    $cib.CurrencyEnglishName = "New Zealand Dollar"
    $cib.CurrencyNativeName = "New Zealand Dollar"
    $cib.CultureEnglishName = "English (Pitcairn Islands)"
    $cib.CultureNativeName = "English (Pitcairn Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-PR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Puerto Rico"
    $cib.RegionNativeName = "Puerto Rico"
    $cib.ThreeLetterISORegionName = "PRI"
    $cib.TwoLetterISORegionName = "PR"
    $cib.ThreeLetterWindowsRegionName = "PRI"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (Puerto Rico)"
    $cib.CultureNativeName = "English (Puerto Rico)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-PW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Palau"
    $cib.RegionNativeName = "Palau"
    $cib.ThreeLetterISORegionName = "PLW"
    $cib.TwoLetterISORegionName = "PW"
    $cib.ThreeLetterWindowsRegionName = "PLW"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (Palau)"
    $cib.CultureNativeName = "English (Palau)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "US$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-RW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Rwanda"
    $cib.RegionNativeName = "Rwanda"
    $cib.ThreeLetterISORegionName = "RWA"
    $cib.TwoLetterISORegionName = "RW"
    $cib.ThreeLetterWindowsRegionName = "RWA"
    $cib.CurrencyEnglishName = "Rwandan Franc"
    $cib.CurrencyNativeName = "Rwandan Franc"
    $cib.CultureEnglishName = "English (Rwanda)"
    $cib.CultureNativeName = "English (Rwanda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "RF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SB"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Solomon Islands"
    $cib.RegionNativeName = "Solomon Islands"
    $cib.ThreeLetterISORegionName = "SLB"
    $cib.TwoLetterISORegionName = "SB"
    $cib.ThreeLetterWindowsRegionName = "SLB"
    $cib.CurrencyEnglishName = "Solomon Islands Dollar"
    $cib.CurrencyNativeName = "Solomon Islands Dollar"
    $cib.CultureEnglishName = "English (Solomon Islands)"
    $cib.CultureNativeName = "English (Solomon Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Seychelles"
    $cib.RegionNativeName = "Seychelles"
    $cib.ThreeLetterISORegionName = "SYC"
    $cib.TwoLetterISORegionName = "SC"
    $cib.ThreeLetterWindowsRegionName = "SYC"
    $cib.CurrencyEnglishName = "Seychellois Rupee"
    $cib.CurrencyNativeName = "Seychellois Rupee"
    $cib.CultureEnglishName = "English (Seychelles)"
    $cib.CultureNativeName = "English (Seychelles)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "SR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Sudan"
    $cib.RegionNativeName = "Sudan"
    $cib.ThreeLetterISORegionName = "SDN"
    $cib.TwoLetterISORegionName = "SD"
    $cib.ThreeLetterWindowsRegionName = "SDN"
    $cib.CurrencyEnglishName = "Sudanese Pound"
    $cib.CurrencyNativeName = "Sudanese Pound"
    $cib.CultureEnglishName = "English (Sudan)"
    $cib.CultureNativeName = "English (Sudan)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "SDG"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Sweden"
    $cib.RegionNativeName = "Sweden"
    $cib.ThreeLetterISORegionName = "SWE"
    $cib.TwoLetterISORegionName = "SE"
    $cib.ThreeLetterWindowsRegionName = "SWE"
    $cib.CurrencyEnglishName = "Swedish Krona"
    $cib.CurrencyNativeName = "Swedish Krona"
    $cib.CultureEnglishName = "English (Sweden)"
    $cib.CultureNativeName = "English (Sweden)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "kr"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "St Helena, Ascension, Tristan da Cunha"
    $cib.RegionNativeName = "St Helena, Ascension, Tristan da Cunha"
    $cib.ThreeLetterISORegionName = "SHN"
    $cib.TwoLetterISORegionName = "SH"
    $cib.ThreeLetterWindowsRegionName = "SHN"
    $cib.CurrencyEnglishName = "Saint Helena Pound"
    $cib.CurrencyNativeName = "Saint Helena Pound"
    $cib.CultureEnglishName = "English (St Helena, Ascension, Tristan da Cunha)"
    $cib.CultureNativeName = "English (St Helena, Ascension, Tristan da Cunha)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Slovenia"
    $cib.RegionNativeName = "Slovenia"
    $cib.ThreeLetterISORegionName = "SVN"
    $cib.TwoLetterISORegionName = "SI"
    $cib.ThreeLetterWindowsRegionName = "SVN"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "English (Slovenia)"
    $cib.CultureNativeName = "English (Slovenia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 15
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Sierra Leone"
    $cib.RegionNativeName = "Sierra Leone"
    $cib.ThreeLetterISORegionName = "SLE"
    $cib.TwoLetterISORegionName = "SL"
    $cib.ThreeLetterWindowsRegionName = "SLE"
    $cib.CurrencyEnglishName = "Sierra Leonean Leone"
    $cib.CurrencyNativeName = "Sierra Leonean Leone"
    $cib.CultureEnglishName = "English (Sierra Leone)"
    $cib.CultureNativeName = "English (Sierra Leone)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Le"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "South Sudan"
    $cib.RegionNativeName = "South Sudan"
    $cib.ThreeLetterISORegionName = "SSD"
    $cib.TwoLetterISORegionName = "SS"
    $cib.ThreeLetterWindowsRegionName = "SSD"
    $cib.CurrencyEnglishName = "South Sudanese Pound"
    $cib.CurrencyNativeName = "South Sudanese Pound"
    $cib.CultureEnglishName = "English (South Sudan)"
    $cib.CultureNativeName = "English (South Sudan)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SX"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Sint Maarten"
    $cib.RegionNativeName = "Sint Maarten"
    $cib.ThreeLetterISORegionName = "SXM"
    $cib.TwoLetterISORegionName = "SX"
    $cib.ThreeLetterWindowsRegionName = "SXM"
    $cib.CurrencyEnglishName = "Netherlands Antillean Guilder"
    $cib.CurrencyNativeName = "Netherlands Antillean Guilder"
    $cib.CultureEnglishName = "English (Sint Maarten)"
    $cib.CultureNativeName = "English (Sint Maarten)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "NAf."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-SZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Swaziland"
    $cib.RegionNativeName = "Swaziland"
    $cib.ThreeLetterISORegionName = "SWZ"
    $cib.TwoLetterISORegionName = "SZ"
    $cib.ThreeLetterWindowsRegionName = "SWZ"
    $cib.CurrencyEnglishName = "Swazi Lilangeni"
    $cib.CurrencyNativeName = "Swazi Lilangeni"
    $cib.CultureEnglishName = "English (Swaziland)"
    $cib.CultureNativeName = "English (Swaziland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "E"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-TC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Turks and Caicos Islands"
    $cib.RegionNativeName = "Turks and Caicos Islands"
    $cib.ThreeLetterISORegionName = "TCA"
    $cib.TwoLetterISORegionName = "TC"
    $cib.ThreeLetterWindowsRegionName = "TCA"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (Turks and Caicos Islands)"
    $cib.CultureNativeName = "English (Turks and Caicos Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "US$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-TK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tokelau"
    $cib.RegionNativeName = "Tokelau"
    $cib.ThreeLetterISORegionName = "TKL"
    $cib.TwoLetterISORegionName = "TK"
    $cib.ThreeLetterWindowsRegionName = "TKL"
    $cib.CurrencyEnglishName = "New Zealand Dollar"
    $cib.CurrencyNativeName = "New Zealand Dollar"
    $cib.CultureEnglishName = "English (Tokelau)"
    $cib.CultureNativeName = "English (Tokelau)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-TO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tonga"
    $cib.RegionNativeName = "Tonga"
    $cib.ThreeLetterISORegionName = "TON"
    $cib.TwoLetterISORegionName = "TO"
    $cib.ThreeLetterWindowsRegionName = "TON"
    $cib.CurrencyEnglishName = "Tongan Paʻanga"
    $cib.CurrencyNativeName = "Tongan Paʻanga"
    $cib.CultureEnglishName = "English (Tonga)"
    $cib.CultureNativeName = "English (Tonga)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "T$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-TV"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tuvalu"
    $cib.RegionNativeName = "Tuvalu"
    $cib.ThreeLetterISORegionName = "TUV"
    $cib.TwoLetterISORegionName = "TV"
    $cib.ThreeLetterWindowsRegionName = "TUV"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "English (Tuvalu)"
    $cib.CultureNativeName = "English (Tuvalu)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Tanzanian Shilling"
    $cib.CultureEnglishName = "English (Tanzania)"
    $cib.CultureNativeName = "English (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-UG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Uganda"
    $cib.RegionNativeName = "Uganda"
    $cib.ThreeLetterISORegionName = "UGA"
    $cib.TwoLetterISORegionName = "UG"
    $cib.ThreeLetterWindowsRegionName = "UGA"
    $cib.CurrencyEnglishName = "Ugandan Shilling"
    $cib.CurrencyNativeName = "Ugandan Shilling"
    $cib.CultureEnglishName = "English (Uganda)"
    $cib.CultureNativeName = "English (Uganda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "USh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-UM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "U.S. Outlying Islands"
    $cib.RegionNativeName = "U.S. Outlying Islands"
    $cib.ThreeLetterISORegionName = "UMI"
    $cib.TwoLetterISORegionName = "UM"
    $cib.ThreeLetterWindowsRegionName = "UMI"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (U.S. Outlying Islands)"
    $cib.CultureNativeName = "English (U.S. Outlying Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-VC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Saint Vincent and the Grenadines"
    $cib.RegionNativeName = "Saint Vincent and the Grenadines"
    $cib.ThreeLetterISORegionName = "VCT"
    $cib.TwoLetterISORegionName = "VC"
    $cib.ThreeLetterWindowsRegionName = "VCT"
    $cib.CurrencyEnglishName = "East Caribbean Dollar"
    $cib.CurrencyNativeName = "East Caribbean Dollar"
    $cib.CultureEnglishName = "English (Saint Vincent and the Grenadines)"
    $cib.CultureNativeName = "English (Saint Vincent and the Grenadines)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-VG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "British Virgin Islands"
    $cib.RegionNativeName = "British Virgin Islands"
    $cib.ThreeLetterISORegionName = "VGB"
    $cib.TwoLetterISORegionName = "VG"
    $cib.ThreeLetterWindowsRegionName = "VGB"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (British Virgin Islands)"
    $cib.CultureNativeName = "English (British Virgin Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "US$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-VI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "U.S. Virgin Islands"
    $cib.RegionNativeName = "U.S. Virgin Islands"
    $cib.ThreeLetterISORegionName = "VIR"
    $cib.TwoLetterISORegionName = "VI"
    $cib.ThreeLetterWindowsRegionName = "VIR"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "English (U.S. Virgin Islands)"
    $cib.CultureNativeName = "English (U.S. Virgin Islands)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-VU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Vanuatu"
    $cib.RegionNativeName = "Vanuatu"
    $cib.ThreeLetterISORegionName = "VUT"
    $cib.TwoLetterISORegionName = "VU"
    $cib.ThreeLetterWindowsRegionName = "VUT"
    $cib.CurrencyEnglishName = "Vanuatu Vatu"
    $cib.CurrencyNativeName = "Vanuatu Vatu"
    $cib.CultureEnglishName = "English (Vanuatu)"
    $cib.CultureNativeName = "English (Vanuatu)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "VT"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-WS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Samoa"
    $cib.RegionNativeName = "Samoa"
    $cib.ThreeLetterISORegionName = "WSM"
    $cib.TwoLetterISORegionName = "WS"
    $cib.ThreeLetterWindowsRegionName = "WSM"
    $cib.CurrencyEnglishName = "Samoan Tala"
    $cib.CurrencyNativeName = "Samoan Tala"
    $cib.CultureEnglishName = "English (Samoa)"
    $cib.CultureNativeName = "English (Samoa)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "WS$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "en-ZM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Zambia"
    $cib.RegionNativeName = "Zambia"
    $cib.ThreeLetterISORegionName = "ZMB"
    $cib.TwoLetterISORegionName = "ZM"
    $cib.ThreeLetterWindowsRegionName = "ZMB"
    $cib.CurrencyEnglishName = "Zambian Kwacha"
    $cib.CurrencyNativeName = "Zambian Kwacha"
    $cib.CultureEnglishName = "English (Zambia)"
    $cib.CultureNativeName = "English (Zambia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "eng"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "en"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "K"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "eo-001"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "World"
    $cib.RegionNativeName = "World"
    $cib.ThreeLetterISORegionName = "001"
    $cib.TwoLetterISORegionName = "001"
    $cib.ThreeLetterWindowsRegionName = "001"
    $cib.CurrencyEnglishName = "Special Drawing Rights"
    $cib.CurrencyNativeName = "Special Drawing Rights"
    $cib.CultureEnglishName = "Esperanto (World)"
    $cib.CultureNativeName = "esperanto (World)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "epo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "eo"

    $cib.GregorianDateTimeFormat.AMDesignator = "atm"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d-'a' 'de' MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d-'a' 'de' MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "XDR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "es-BR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Brazil"
    $cib.RegionNativeName = "Brasil"
    $cib.ThreeLetterISORegionName = "BRA"
    $cib.TwoLetterISORegionName = "BR"
    $cib.ThreeLetterWindowsRegionName = "BRA"
    $cib.CurrencyEnglishName = "Brazilian Real"
    $cib.CurrencyNativeName = "real brasileño"
    $cib.CultureEnglishName = "Spanish (Brazil)"
    $cib.CultureNativeName = "español (Brasil)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "spa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "es"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "R$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "es-BZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Belize"
    $cib.RegionNativeName = "Belice"
    $cib.ThreeLetterISORegionName = "BLZ"
    $cib.TwoLetterISORegionName = "BZ"
    $cib.ThreeLetterWindowsRegionName = "BLZ"
    $cib.CurrencyEnglishName = "Belize Dollar"
    $cib.CurrencyNativeName = "dólar beliceño"
    $cib.CultureEnglishName = "Spanish (Belize)"
    $cib.CultureNativeName = "español (Belice)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "spa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "es"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "es-GQ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Equatorial Guinea"
    $cib.RegionNativeName = "Guinea Ecuatorial"
    $cib.ThreeLetterISORegionName = "GNQ"
    $cib.TwoLetterISORegionName = "GQ"
    $cib.ThreeLetterWindowsRegionName = "GNQ"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "franco CFA de África Central"
    $cib.CultureEnglishName = "Spanish (Equatorial Guinea)"
    $cib.CultureNativeName = "español (Guinea Ecuatorial)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "spa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "es"

    $cib.GregorianDateTimeFormat.AMDesignator = "a. m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "es-PH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Philippines"
    $cib.RegionNativeName = "Filipinas"
    $cib.ThreeLetterISORegionName = "PHL"
    $cib.TwoLetterISORegionName = "PH"
    $cib.ThreeLetterWindowsRegionName = "PHL"
    $cib.CurrencyEnglishName = "Philippine Piso"
    $cib.CurrencyNativeName = "peso filipino"
    $cib.CultureEnglishName = "Spanish (Philippines)"
    $cib.CultureNativeName = "español (Filipinas)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "spa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "es"

    $cib.GregorianDateTimeFormat.AMDesignator = "a. m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "₱"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ewo-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kamərún"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Fəláŋ CFA (BEAC)"
    $cib.CultureEnglishName = "Ewondo (Cameroon)"
    $cib.CultureNativeName = "ewondo (Kamərún)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ewo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ewo"

    $cib.GregorianDateTimeFormat.AMDesignator = "kíkíríg"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ff-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kameruun"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Mbuuɗi Seefaa BEAC"
    $cib.CultureEnglishName = "Fulah (Cameroon)"
    $cib.CultureNativeName = "Pulaar (Kameruun)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ful"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ff"

    $cib.GregorianDateTimeFormat.AMDesignator = "subaka"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ff-GN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guinea"
    $cib.RegionNativeName = "Gine"
    $cib.ThreeLetterISORegionName = "GIN"
    $cib.TwoLetterISORegionName = "GN"
    $cib.ThreeLetterWindowsRegionName = "GIN"
    $cib.CurrencyEnglishName = "Guinean Franc"
    $cib.CurrencyNativeName = "Guinean Franc"
    $cib.CultureEnglishName = "Fulah (Guinea)"
    $cib.CultureNativeName = "Pulaar (Gine)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ful"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ff"

    $cib.GregorianDateTimeFormat.AMDesignator = "subaka"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FG"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ff-MR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mauritania"
    $cib.RegionNativeName = "Muritani"
    $cib.ThreeLetterISORegionName = "MRT"
    $cib.TwoLetterISORegionName = "MR"
    $cib.ThreeLetterWindowsRegionName = "MRT"
    $cib.CurrencyEnglishName = "Mauritanian Ouguiya"
    $cib.CurrencyNativeName = "Ugiyya Muritani"
    $cib.CultureEnglishName = "Fulah (Mauritania)"
    $cib.CultureNativeName = "Pulaar (Muritani)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ful"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ff"

    $cib.GregorianDateTimeFormat.AMDesignator = "subaka"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "UM"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fo-DK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Denmark"
    $cib.RegionNativeName = "Danmark"
    $cib.ThreeLetterISORegionName = "DNK"
    $cib.TwoLetterISORegionName = "DK"
    $cib.ThreeLetterWindowsRegionName = "DNK"
    $cib.CurrencyEnglishName = "Danish Krone"
    $cib.CurrencyNativeName = "donsk króna"
    $cib.CultureEnglishName = "Faroese (Denmark)"
    $cib.CultureNativeName = "føroyskt (Danmark)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fao"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fo"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "kr."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-BF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Burkina Faso"
    $cib.RegionNativeName = "Burkina Faso"
    $cib.ThreeLetterISORegionName = "BFA"
    $cib.TwoLetterISORegionName = "BF"
    $cib.ThreeLetterWindowsRegionName = "BFA"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BCEAO)"
    $cib.CultureEnglishName = "French (Burkina Faso)"
    $cib.CultureNativeName = "français (Burkina Faso)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-BI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Burundi"
    $cib.RegionNativeName = "Burundi"
    $cib.ThreeLetterISORegionName = "BDI"
    $cib.TwoLetterISORegionName = "BI"
    $cib.ThreeLetterWindowsRegionName = "BDI"
    $cib.CurrencyEnglishName = "Burundian Franc"
    $cib.CurrencyNativeName = "franc burundais"
    $cib.CultureEnglishName = "French (Burundi)"
    $cib.CultureNativeName = "français (Burundi)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FBu"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-BJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Benin"
    $cib.RegionNativeName = "Bénin"
    $cib.ThreeLetterISORegionName = "BEN"
    $cib.TwoLetterISORegionName = "BJ"
    $cib.ThreeLetterWindowsRegionName = "BEN"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BCEAO)"
    $cib.CultureEnglishName = "French (Benin)"
    $cib.CultureNativeName = "français (Bénin)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-BL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Saint Barthélemy"
    $cib.RegionNativeName = "Saint-Barthélemy"
    $cib.ThreeLetterISORegionName = "BLM"
    $cib.TwoLetterISORegionName = "BL"
    $cib.ThreeLetterWindowsRegionName = "BLM"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (Saint Barthélemy)"
    $cib.CultureNativeName = "français (Saint-Barthélemy)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-CF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Central African Republic"
    $cib.RegionNativeName = "République centrafricaine"
    $cib.ThreeLetterISORegionName = "CAF"
    $cib.TwoLetterISORegionName = "CF"
    $cib.ThreeLetterWindowsRegionName = "CAF"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BEAC)"
    $cib.CultureEnglishName = "French (Central African Republic)"
    $cib.CultureNativeName = "français (République centrafricaine)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-CG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Congo"
    $cib.RegionNativeName = "Congo"
    $cib.ThreeLetterISORegionName = "COG"
    $cib.TwoLetterISORegionName = "CG"
    $cib.ThreeLetterWindowsRegionName = "COG"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BEAC)"
    $cib.CultureEnglishName = "French (Congo)"
    $cib.CultureNativeName = "français (Congo)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-DJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Djibouti"
    $cib.RegionNativeName = "Djibouti"
    $cib.ThreeLetterISORegionName = "DJI"
    $cib.TwoLetterISORegionName = "DJ"
    $cib.ThreeLetterWindowsRegionName = "DJI"
    $cib.CurrencyEnglishName = "Djiboutian Franc"
    $cib.CurrencyNativeName = "franc djiboutien"
    $cib.CultureEnglishName = "French (Djibouti)"
    $cib.CultureNativeName = "français (Djibouti)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "Fdj"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-DZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Algeria"
    $cib.RegionNativeName = "Algérie"
    $cib.ThreeLetterISORegionName = "DZA"
    $cib.TwoLetterISORegionName = "DZ"
    $cib.ThreeLetterWindowsRegionName = "DZA"
    $cib.CurrencyEnglishName = "Algerian Dinar"
    $cib.CurrencyNativeName = "dinar algérien"
    $cib.CultureEnglishName = "French (Algeria)"
    $cib.CultureNativeName = "français (Algérie)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "DA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-GA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Gabon"
    $cib.RegionNativeName = "Gabon"
    $cib.ThreeLetterISORegionName = "GAB"
    $cib.TwoLetterISORegionName = "GA"
    $cib.ThreeLetterWindowsRegionName = "GAB"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BEAC)"
    $cib.CultureEnglishName = "French (Gabon)"
    $cib.CultureNativeName = "français (Gabon)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-GF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "French Guiana"
    $cib.RegionNativeName = "Guyane française"
    $cib.ThreeLetterISORegionName = "GUF"
    $cib.TwoLetterISORegionName = "GF"
    $cib.ThreeLetterWindowsRegionName = "GUF"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (French Guiana)"
    $cib.CultureNativeName = "français (Guyane française)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-GN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guinea"
    $cib.RegionNativeName = "Guinée"
    $cib.ThreeLetterISORegionName = "GIN"
    $cib.TwoLetterISORegionName = "GN"
    $cib.ThreeLetterWindowsRegionName = "GIN"
    $cib.CurrencyEnglishName = "Guinean Franc"
    $cib.CurrencyNativeName = "franc guinéen"
    $cib.CultureEnglishName = "French (Guinea)"
    $cib.CultureNativeName = "français (Guinée)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FG"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-GP"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guadeloupe"
    $cib.RegionNativeName = "Guadeloupe"
    $cib.ThreeLetterISORegionName = "GLP"
    $cib.TwoLetterISORegionName = "GP"
    $cib.ThreeLetterWindowsRegionName = "GLP"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (Guadeloupe)"
    $cib.CultureNativeName = "français (Guadeloupe)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-GQ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Equatorial Guinea"
    $cib.RegionNativeName = "Guinée équatoriale"
    $cib.ThreeLetterISORegionName = "GNQ"
    $cib.TwoLetterISORegionName = "GQ"
    $cib.ThreeLetterWindowsRegionName = "GNQ"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BEAC)"
    $cib.CultureEnglishName = "French (Equatorial Guinea)"
    $cib.CultureNativeName = "français (Guinée équatoriale)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-KM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Comoros"
    $cib.RegionNativeName = "Comores"
    $cib.ThreeLetterISORegionName = "COM"
    $cib.TwoLetterISORegionName = "KM"
    $cib.ThreeLetterWindowsRegionName = "COM"
    $cib.CurrencyEnglishName = "Comorian Franc"
    $cib.CurrencyNativeName = "franc comorien"
    $cib.CultureEnglishName = "French (Comoros)"
    $cib.CultureNativeName = "français (Comores)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-MC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Monaco"
    $cib.RegionNativeName = "Monaco"
    $cib.ThreeLetterISORegionName = "MCO"
    $cib.TwoLetterISORegionName = "MC"
    $cib.ThreeLetterWindowsRegionName = "MCO"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (Monaco)"
    $cib.CultureNativeName = "français (Monaco)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "FRM"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-MF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Saint Martin"
    $cib.RegionNativeName = "Saint-Martin"
    $cib.ThreeLetterISORegionName = "MAF"
    $cib.TwoLetterISORegionName = "MF"
    $cib.ThreeLetterWindowsRegionName = "MAF"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (Saint Martin)"
    $cib.CultureNativeName = "français (Saint-Martin)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-MG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Madagascar"
    $cib.RegionNativeName = "Madagascar"
    $cib.ThreeLetterISORegionName = "MDG"
    $cib.TwoLetterISORegionName = "MG"
    $cib.ThreeLetterWindowsRegionName = "MDG"
    $cib.CurrencyEnglishName = "Malagasy Ariary"
    $cib.CurrencyNativeName = "ariary malgache"
    $cib.CultureEnglishName = "French (Madagascar)"
    $cib.CultureNativeName = "français (Madagascar)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "Ar"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-MQ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Martinique"
    $cib.RegionNativeName = "Martinique"
    $cib.ThreeLetterISORegionName = "MTQ"
    $cib.TwoLetterISORegionName = "MQ"
    $cib.ThreeLetterWindowsRegionName = "MTQ"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (Martinique)"
    $cib.CultureNativeName = "français (Martinique)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-MR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mauritania"
    $cib.RegionNativeName = "Mauritanie"
    $cib.ThreeLetterISORegionName = "MRT"
    $cib.TwoLetterISORegionName = "MR"
    $cib.ThreeLetterWindowsRegionName = "MRT"
    $cib.CurrencyEnglishName = "Mauritanian Ouguiya"
    $cib.CurrencyNativeName = "ouguiya mauritanien"
    $cib.CultureEnglishName = "French (Mauritania)"
    $cib.CultureNativeName = "français (Mauritanie)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "UM"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-MU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mauritius"
    $cib.RegionNativeName = "Maurice"
    $cib.ThreeLetterISORegionName = "MUS"
    $cib.TwoLetterISORegionName = "MU"
    $cib.ThreeLetterWindowsRegionName = "MUS"
    $cib.CurrencyEnglishName = "Mauritian Rupee"
    $cib.CurrencyNativeName = "roupie mauricienne"
    $cib.CultureEnglishName = "French (Mauritius)"
    $cib.CultureNativeName = "français (Maurice)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "Rs"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-NC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "New Caledonia"
    $cib.RegionNativeName = "Nouvelle-Calédonie"
    $cib.ThreeLetterISORegionName = "NCL"
    $cib.TwoLetterISORegionName = "NC"
    $cib.ThreeLetterWindowsRegionName = "NCL"
    $cib.CurrencyEnglishName = "CFP Franc"
    $cib.CurrencyNativeName = "franc CFP"
    $cib.CultureEnglishName = "French (New Caledonia)"
    $cib.CultureNativeName = "français (Nouvelle-Calédonie)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFP"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-NE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Niger"
    $cib.RegionNativeName = "Niger"
    $cib.ThreeLetterISORegionName = "NER"
    $cib.TwoLetterISORegionName = "NE"
    $cib.ThreeLetterWindowsRegionName = "NER"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BCEAO)"
    $cib.CultureEnglishName = "French (Niger)"
    $cib.CultureNativeName = "français (Niger)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-PF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "French Polynesia"
    $cib.RegionNativeName = "Polynésie française"
    $cib.ThreeLetterISORegionName = "PYF"
    $cib.TwoLetterISORegionName = "PF"
    $cib.ThreeLetterWindowsRegionName = "PYF"
    $cib.CurrencyEnglishName = "CFP Franc"
    $cib.CurrencyNativeName = "franc CFP"
    $cib.CultureEnglishName = "French (French Polynesia)"
    $cib.CultureNativeName = "français (Polynésie française)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFP"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-PM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Saint Pierre and Miquelon"
    $cib.RegionNativeName = "Saint-Pierre-et-Miquelon"
    $cib.ThreeLetterISORegionName = "SPM"
    $cib.TwoLetterISORegionName = "PM"
    $cib.ThreeLetterWindowsRegionName = "SPM"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (Saint Pierre and Miquelon)"
    $cib.CultureNativeName = "français (Saint-Pierre-et-Miquelon)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-RW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Rwanda"
    $cib.RegionNativeName = "Rwanda"
    $cib.ThreeLetterISORegionName = "RWA"
    $cib.TwoLetterISORegionName = "RW"
    $cib.ThreeLetterWindowsRegionName = "RWA"
    $cib.CurrencyEnglishName = "Rwandan Franc"
    $cib.CurrencyNativeName = "franc rwandais"
    $cib.CultureEnglishName = "French (Rwanda)"
    $cib.CultureNativeName = "français (Rwanda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "RF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-SC"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Seychelles"
    $cib.RegionNativeName = "Seychelles"
    $cib.ThreeLetterISORegionName = "SYC"
    $cib.TwoLetterISORegionName = "SC"
    $cib.ThreeLetterWindowsRegionName = "SYC"
    $cib.CurrencyEnglishName = "Seychellois Rupee"
    $cib.CurrencyNativeName = "roupie des Seychelles"
    $cib.CultureEnglishName = "French (Seychelles)"
    $cib.CultureNativeName = "français (Seychelles)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "SR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-SY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Syria"
    $cib.RegionNativeName = "Syrie"
    $cib.ThreeLetterISORegionName = "SYR"
    $cib.TwoLetterISORegionName = "SY"
    $cib.ThreeLetterWindowsRegionName = "SYR"
    $cib.CurrencyEnglishName = "Syrian Pound"
    $cib.CurrencyNativeName = "livre syrienne"
    $cib.CultureEnglishName = "French (Syria)"
    $cib.CultureNativeName = "français (Syrie)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "LS"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-TD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Chad"
    $cib.RegionNativeName = "Tchad"
    $cib.ThreeLetterISORegionName = "TCD"
    $cib.TwoLetterISORegionName = "TD"
    $cib.ThreeLetterWindowsRegionName = "TCD"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BEAC)"
    $cib.CultureEnglishName = "French (Chad)"
    $cib.CultureNativeName = "français (Tchad)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-TG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Togo"
    $cib.RegionNativeName = "Togo"
    $cib.ThreeLetterISORegionName = "TGO"
    $cib.TwoLetterISORegionName = "TG"
    $cib.ThreeLetterWindowsRegionName = "TGO"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "franc CFA (BCEAO)"
    $cib.CultureEnglishName = "French (Togo)"
    $cib.CultureNativeName = "français (Togo)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-TN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tunisia"
    $cib.RegionNativeName = "Tunisie"
    $cib.ThreeLetterISORegionName = "TUN"
    $cib.TwoLetterISORegionName = "TN"
    $cib.ThreeLetterWindowsRegionName = "TUN"
    $cib.CurrencyEnglishName = "Tunisian Dinar"
    $cib.CurrencyNativeName = "dinar tunisien"
    $cib.CultureEnglishName = "French (Tunisia)"
    $cib.CultureNativeName = "français (Tunisie)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "DT"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-VU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Vanuatu"
    $cib.RegionNativeName = "Vanuatu"
    $cib.ThreeLetterISORegionName = "VUT"
    $cib.TwoLetterISORegionName = "VU"
    $cib.ThreeLetterWindowsRegionName = "VUT"
    $cib.CurrencyEnglishName = "Vanuatu Vatu"
    $cib.CurrencyNativeName = "vatu vanuatuan"
    $cib.CultureEnglishName = "French (Vanuatu)"
    $cib.CultureNativeName = "français (Vanuatu)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "VT"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-WF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Wallis and Futuna"
    $cib.RegionNativeName = "Wallis-et-Futuna"
    $cib.ThreeLetterISORegionName = "WLF"
    $cib.TwoLetterISORegionName = "WF"
    $cib.ThreeLetterWindowsRegionName = "WLF"
    $cib.CurrencyEnglishName = "CFP Franc"
    $cib.CurrencyNativeName = "franc CFP"
    $cib.CultureEnglishName = "French (Wallis and Futuna)"
    $cib.CultureNativeName = "français (Wallis-et-Futuna)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFP"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fr-YT"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mayotte"
    $cib.RegionNativeName = "Mayotte"
    $cib.ThreeLetterISORegionName = "MYT"
    $cib.TwoLetterISORegionName = "YT"
    $cib.ThreeLetterWindowsRegionName = "MYT"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "French (Mayotte)"
    $cib.CultureNativeName = "français (Mayotte)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fra"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "fur-IT"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Italy"
    $cib.RegionNativeName = "Italie"
    $cib.ThreeLetterISORegionName = "ITA"
    $cib.TwoLetterISORegionName = "IT"
    $cib.ThreeLetterWindowsRegionName = "ITA"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Friulian (Italy)"
    $cib.CultureNativeName = "furlan (Italie)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "fur"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "fur"

    $cib.GregorianDateTimeFormat.AMDesignator = "a."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d 'di' MMMM 'dal' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d 'di' MMMM 'dal' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'di' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'dal' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "gsw-CH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Switzerland"
    $cib.RegionNativeName = "Schwiiz"
    $cib.ThreeLetterISORegionName = "CHE"
    $cib.TwoLetterISORegionName = "CH"
    $cib.ThreeLetterWindowsRegionName = "CHE"
    $cib.CurrencyEnglishName = "Swiss Franc"
    $cib.CurrencyNativeName = "Schwiizer Franke"
    $cib.CultureEnglishName = "Swiss German (Switzerland)"
    $cib.CultureNativeName = "Schwiizertüütsch (Schwiiz)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "gsw"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "gsw"

    $cib.GregorianDateTimeFormat.AMDesignator = "vorm."
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = "’"
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CHF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = "’"
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = "’"
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "gsw-LI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Liechtenstein"
    $cib.RegionNativeName = "Liächteschtäi"
    $cib.ThreeLetterISORegionName = "LIE"
    $cib.TwoLetterISORegionName = "LI"
    $cib.ThreeLetterWindowsRegionName = "LIE"
    $cib.CurrencyEnglishName = "Swiss Franc"
    $cib.CurrencyNativeName = "Schwiizer Franke"
    $cib.CultureEnglishName = "Swiss German (Liechtenstein)"
    $cib.CultureNativeName = "Schwiizertüütsch (Liächteschtäi)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "gsw"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "gsw"

    $cib.GregorianDateTimeFormat.AMDesignator = "vorm."
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = "’"
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CHF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = "’"
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = "’"
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "guz-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Kenya"
    $cib.CultureEnglishName = "Gusii (Kenya)"
    $cib.CultureNativeName = "Ekegusii (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "guz"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "guz"

    $cib.GregorianDateTimeFormat.AMDesignator = "Ma"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "gv-IM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Isle of Man"
    $cib.RegionNativeName = "Ellan Vannin"
    $cib.ThreeLetterISORegionName = "IMN"
    $cib.TwoLetterISORegionName = "IM"
    $cib.ThreeLetterWindowsRegionName = "IMN"
    $cib.CurrencyEnglishName = "British Pound"
    $cib.CurrencyNativeName = "British Pound"
    $cib.CultureEnglishName = "Manx (Isle of Man)"
    $cib.CultureNativeName = "Gaelg (Ellan Vannin)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "glv"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "gv"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd dd MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd dd MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ha-Latn-GH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ghana"
    $cib.RegionNativeName = "Gana"
    $cib.ThreeLetterISORegionName = "GHA"
    $cib.TwoLetterISORegionName = "GH"
    $cib.ThreeLetterWindowsRegionName = "GHA"
    $cib.CurrencyEnglishName = "Ghanaian Cedi"
    $cib.CurrencyNativeName = "Ghanaian Cedi"
    $cib.CultureEnglishName = "Hausa (Latin, Ghana)"
    $cib.CultureNativeName = "Hausa (Gana)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "hau"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ha"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM, yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "GH₵"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ha-Latn-NE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Niger"
    $cib.RegionNativeName = "Nijar"
    $cib.ThreeLetterISORegionName = "NER"
    $cib.TwoLetterISORegionName = "NE"
    $cib.ThreeLetterWindowsRegionName = "NER"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "Kuɗin Sefa na Afirka Ta Yamma"
    $cib.CultureEnglishName = "Hausa (Latin, Niger)"
    $cib.CultureNativeName = "Hausa (Nijar)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "hau"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ha"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM, yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ia-001"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "World"
    $cib.RegionNativeName = "World"
    $cib.ThreeLetterISORegionName = "001"
    $cib.TwoLetterISORegionName = "001"
    $cib.ThreeLetterWindowsRegionName = "001"
    $cib.CurrencyEnglishName = "Special Drawing Rights"
    $cib.CurrencyNativeName = "Special Drawing Rights"
    $cib.CultureEnglishName = "Interlingua (World)"
    $cib.CultureNativeName = "interlingua (World)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ina"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ia"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, yyyy MMMM dd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, yyyy MMMM dd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy/MM/dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "XDR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ia-FR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "France"
    $cib.RegionNativeName = "Francia"
    $cib.ThreeLetterISORegionName = "FRA"
    $cib.TwoLetterISORegionName = "FR"
    $cib.ThreeLetterWindowsRegionName = "FRA"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euros"
    $cib.CultureEnglishName = "Interlingua (France)"
    $cib.CultureNativeName = "interlingua (Francia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ina"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ia"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, yyyy MMMM dd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, yyyy MMMM dd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy/MM/dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "it-SM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "San Marino"
    $cib.RegionNativeName = "San Marino"
    $cib.ThreeLetterISORegionName = "SMR"
    $cib.TwoLetterISORegionName = "SM"
    $cib.ThreeLetterWindowsRegionName = "SMR"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Italian (San Marino)"
    $cib.CultureNativeName = "italiano (San Marino)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ita"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "it"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "it-VA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Vatican City"
    $cib.RegionNativeName = "Città del Vaticano"
    $cib.ThreeLetterISORegionName = "VAT"
    $cib.TwoLetterISORegionName = "VA"
    $cib.ThreeLetterWindowsRegionName = "VAT"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Italian (Vatican City)"
    $cib.CultureNativeName = "italiano (Città del Vaticano)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ita"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "it"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "jgo-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kamɛlûn"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Fɛlâŋ"
    $cib.CultureEnglishName = "Ngomba (Cameroon)"
    $cib.CultureNativeName = "Ndaꞌa (Kamɛlûn)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "jgo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "jgo"

    $cib.GregorianDateTimeFormat.AMDesignator = "mbaꞌmbaꞌ"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, yyyy MMMM dd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, yyyy MMMM dd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "jmc-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Tanzania"
    $cib.CultureEnglishName = "Machame (Tanzania)"
    $cib.CultureNativeName = "Kimachame (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "jmc"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "jmc"

    $cib.GregorianDateTimeFormat.AMDesignator = "utuko"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "jv-Java-ID"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Indonesia"
    $cib.RegionNativeName = "Indonesia"
    $cib.ThreeLetterISORegionName = "IDN"
    $cib.TwoLetterISORegionName = "ID"
    $cib.ThreeLetterWindowsRegionName = "IDN"
    $cib.CurrencyEnglishName = "Indonesian Rupiah"
    $cib.CurrencyNativeName = "Rupiah"
    $cib.CultureEnglishName = "Javanese (Javanese, Indonesia)"
    $cib.CultureNativeName = "ꦧꦱꦗꦮ (Indonesia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "jav"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "jv"

    $cib.GregorianDateTimeFormat.AMDesignator = ""
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dd MMMM yyyy HH.mm.ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dd MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH.mm.ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "dd MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH.mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = "."
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Rp"
    $cib.NumberFormat.NegativeInfinitySymbol = "-Infinity"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "Infinity"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "jv-Latn-ID"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Indonesia"
    $cib.RegionNativeName = "Indonesia"
    $cib.ThreeLetterISORegionName = "IDN"
    $cib.TwoLetterISORegionName = "ID"
    $cib.ThreeLetterWindowsRegionName = "IDN"
    $cib.CurrencyEnglishName = "Indonesian Rupiah"
    $cib.CurrencyNativeName = "Rupiah"
    $cib.CultureEnglishName = "Javanese (Indonesia)"
    $cib.CultureNativeName = "Basa Jawa (Indonesia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "jav"
    $cib.ThreeLetterWindowsLanguageName = "JAV"
    $cib.TwoLetterISOLanguageName = "jv"

    $cib.GregorianDateTimeFormat.AMDesignator = ""
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dd MMMM yyyy HH.mm.ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dd MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH.mm.ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "dd MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH.mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = "."
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Rp"
    $cib.NumberFormat.NegativeInfinitySymbol = "-Infinity"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "Infinity"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kab-DZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Algeria"
    $cib.RegionNativeName = "Lezzayer"
    $cib.ThreeLetterISORegionName = "DZA"
    $cib.TwoLetterISORegionName = "DZ"
    $cib.ThreeLetterWindowsRegionName = "DZA"
    $cib.CurrencyEnglishName = "Algerian Dinar"
    $cib.CurrencyNativeName = "Adinar Azzayri"
    $cib.CultureEnglishName = "Kabyle (Algeria)"
    $cib.CultureNativeName = "Taqbaylit (Lezzayer)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kab"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kab"

    $cib.GregorianDateTimeFormat.AMDesignator = "n tufat"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "DA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kam-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Silingi ya Kenya"
    $cib.CultureEnglishName = "Kamba (Kenya)"
    $cib.CultureNativeName = "Kikamba (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kam"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kam"

    $cib.GregorianDateTimeFormat.AMDesignator = "Ĩyakwakya"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kde-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Tanzania"
    $cib.CultureEnglishName = "Makonde (Tanzania)"
    $cib.CultureNativeName = "Chimakonde (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kde"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kde"

    $cib.GregorianDateTimeFormat.AMDesignator = "Muhi"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kea-CV"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cabo Verde"
    $cib.RegionNativeName = "Kabu Verdi"
    $cib.ThreeLetterISORegionName = "CPV"
    $cib.TwoLetterISORegionName = "CV"
    $cib.ThreeLetterWindowsRegionName = "CPV"
    $cib.CurrencyEnglishName = "Cape Verdean Escudo"
    $cib.CurrencyNativeName = "Skudu Kabuverdianu"
    $cib.CultureEnglishName = "Kabuverdianu (Cabo Verde)"
    $cib.CultureNativeName = "kabuverdianu (Kabu Verdi)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kea"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kea"

    $cib.GregorianDateTimeFormat.AMDesignator = "am"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'di' MMMM 'di' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'di' MMMM 'di' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'di' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'di' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "$"
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "​"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "khq-ML"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mali"
    $cib.RegionNativeName = "Maali"
    $cib.ThreeLetterISORegionName = "MLI"
    $cib.TwoLetterISORegionName = "ML"
    $cib.ThreeLetterWindowsRegionName = "MLI"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "CFA Fraŋ (BCEAO)"
    $cib.CultureEnglishName = "Koyra Chiini (Mali)"
    $cib.CultureNativeName = "Koyra ciini (Maali)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "khq"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "khq"

    $cib.GregorianDateTimeFormat.AMDesignator = "Adduha"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ki"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kiribati"
    $cib.RegionNativeName = "Kiribati"
    $cib.ThreeLetterISORegionName = "KIR"
    $cib.TwoLetterISORegionName = "KI"
    $cib.ThreeLetterWindowsRegionName = "KIR"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "Kikuyu"
    $cib.CultureNativeName = "Gikuyu"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kik"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ki"

    $cib.GregorianDateTimeFormat.AMDesignator = "Kiroko"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ki-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Ciringi ya Kenya"
    $cib.CultureEnglishName = "Kikuyu (Kenya)"
    $cib.CultureNativeName = "Gikuyu (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kik"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ki"

    $cib.GregorianDateTimeFormat.AMDesignator = "Kiroko"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kkj-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kamɛrun"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Franc CFA"
    $cib.CultureEnglishName = "Kako (Cameroon)"
    $cib.CultureNativeName = "kakɔ (Kamɛrun)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kkj"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kkj"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd dd MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd dd MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kln-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Emetab Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Silingitab ya Kenya"
    $cib.CultureEnglishName = "Kalenjin (Kenya)"
    $cib.CultureNativeName = "Kalenjin (Emetab Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kln"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kln"

    $cib.GregorianDateTimeFormat.AMDesignator = "krn"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ko-KP"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "North Korea"
    $cib.RegionNativeName = "조선민주주의인민공화국"
    $cib.ThreeLetterISORegionName = "PRK"
    $cib.TwoLetterISORegionName = "KP"
    $cib.ThreeLetterWindowsRegionName = "PRK"
    $cib.CurrencyEnglishName = "North Korean Won"
    $cib.CurrencyNativeName = "조선 민주주의 인민 공화국 원"
    $cib.CultureEnglishName = "Korean (North Korea)"
    $cib.CultureNativeName = "한국어 (조선민주주의인민공화국)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kor"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ko"

    $cib.GregorianDateTimeFormat.AMDesignator = "오전"
    $cib.GregorianDateTimeFormat.DateSeparator = ". "
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy년 M월 d일 dddd tt h:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy년 M월 d일 dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tt h:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d일"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy. M. d."
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tt h:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy년 MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "₩"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ks-Arab-IN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "India"
    $cib.RegionNativeName = "ہِنٛدوستان"
    $cib.ThreeLetterISORegionName = "IND"
    $cib.TwoLetterISORegionName = "IN"
    $cib.ThreeLetterWindowsRegionName = "IND"
    $cib.CurrencyEnglishName = "Indian Rupee"
    $cib.CurrencyNativeName = "ہِندُستٲنۍ رۄپَے"
    $cib.CultureEnglishName = "Kashmiri (Perso-Arabic)"
    $cib.CultureNativeName = "کٲشُر (اَربی)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kas"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ks"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "₹"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ksb-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "shilingi ya Tanzania"
    $cib.CultureEnglishName = "Shambala (Tanzania)"
    $cib.CultureNativeName = "Kishambaa (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ksb"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ksb"

    $cib.GregorianDateTimeFormat.AMDesignator = "makeo"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ksf-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "kamɛrún"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "fráŋ"
    $cib.CultureEnglishName = "Bafia (Cameroon)"
    $cib.CultureNativeName = "rikpa (kamɛrún)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ksf"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ksf"

    $cib.GregorianDateTimeFormat.AMDesignator = "sárúwá"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ksh-DE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Germany"
    $cib.RegionNativeName = "Doütschland"
    $cib.ThreeLetterISORegionName = "DEU"
    $cib.TwoLetterISORegionName = "DE"
    $cib.ThreeLetterWindowsRegionName = "DEU"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "Colognian (Germany)"
    $cib.CultureNativeName = "Kölsch (Doütschland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ksh"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ksh"

    $cib.GregorianDateTimeFormat.AMDesignator = "v.M."
    $cib.GregorianDateTimeFormat.DateSeparator = ". "
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, 'dä' d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, 'dä' d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d. M. yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ku-Arab-IR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Iran"
    $cib.RegionNativeName = "ئێران"
    $cib.ThreeLetterISORegionName = "IRN"
    $cib.TwoLetterISORegionName = "IR"
    $cib.ThreeLetterWindowsRegionName = "IRN"
    $cib.CurrencyEnglishName = "Iranian Rial"
    $cib.CurrencyNativeName = "ڕیاڵی ئێرانی"
    $cib.CultureEnglishName = "Kurdish (Perso-Arabic, Iran)"
    $cib.CultureNativeName = "کوردی (ئێران)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "kur"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ku"

    $cib.GregorianDateTimeFormat.AMDesignator = "ب.ن"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM, yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "IRR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kw"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kuwait"
    $cib.RegionNativeName = "الكويت"
    $cib.ThreeLetterISORegionName = "KWT"
    $cib.TwoLetterISORegionName = "KW"
    $cib.ThreeLetterWindowsRegionName = "KWT"
    $cib.CurrencyEnglishName = "Kuwaiti Dinar"
    $cib.CurrencyNativeName = "دينار كويتي"
    $cib.CultureEnglishName = "Cornish"
    $cib.CultureNativeName = "kernewek"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "cor"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kw"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "kw-GB"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "United Kingdom"
    $cib.RegionNativeName = "Rywvaneth Unys"
    $cib.ThreeLetterISORegionName = "GBR"
    $cib.TwoLetterISORegionName = "GB"
    $cib.ThreeLetterWindowsRegionName = "GBR"
    $cib.CurrencyEnglishName = "British Pound"
    $cib.CurrencyNativeName = "British Pound"
    $cib.CultureEnglishName = "Cornish (United Kingdom)"
    $cib.CultureNativeName = "kernewek (Rywvaneth Unys)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "cor"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "kw"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "lag-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Taansanía"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Shilíingi ya Taansanía"
    $cib.CultureEnglishName = "Langi (Tanzania)"
    $cib.CultureNativeName = "Kɨlaangi (Taansanía)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lag"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "lag"

    $cib.GregorianDateTimeFormat.AMDesignator = "TOO"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "lg-UG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Uganda"
    $cib.RegionNativeName = "Yuganda"
    $cib.ThreeLetterISORegionName = "UGA"
    $cib.TwoLetterISORegionName = "UG"
    $cib.ThreeLetterWindowsRegionName = "UGA"
    $cib.CurrencyEnglishName = "Ugandan Shilling"
    $cib.CurrencyNativeName = "Silingi eya Yuganda"
    $cib.CultureEnglishName = "Ganda (Uganda)"
    $cib.CultureNativeName = "Luganda (Yuganda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lug"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "lg"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "USh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "lkt-US"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "United States"
    $cib.RegionNativeName = "Mílahaŋska Tȟamákȟočhe"
    $cib.ThreeLetterISORegionName = "USA"
    $cib.TwoLetterISORegionName = "US"
    $cib.ThreeLetterWindowsRegionName = "USA"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "US Dollar"
    $cib.CultureEnglishName = "Lakota (United States)"
    $cib.CultureNativeName = "Lakȟólʼiyapi (Mílahaŋska Tȟamákȟočhe)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lkt"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "lkt"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "M/d/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ln-AO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Angola"
    $cib.RegionNativeName = "Angóla"
    $cib.ThreeLetterISORegionName = "AGO"
    $cib.TwoLetterISORegionName = "AO"
    $cib.ThreeLetterWindowsRegionName = "AGO"
    $cib.CurrencyEnglishName = "Angolan Kwanza"
    $cib.CurrencyNativeName = "Kwanza ya Angóla"
    $cib.CultureEnglishName = "Lingala (Angola)"
    $cib.CultureNativeName = "lingála (Angóla)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lin"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ln"

    $cib.GregorianDateTimeFormat.AMDesignator = "ntɔ́ngɔ́"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "Kz"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ln-CD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Congo (DRC)"
    $cib.RegionNativeName = "Republíki ya Kongó Demokratíki"
    $cib.ThreeLetterISORegionName = "COD"
    $cib.TwoLetterISORegionName = "CD"
    $cib.ThreeLetterWindowsRegionName = "COD"
    $cib.CurrencyEnglishName = "Congolese Franc"
    $cib.CurrencyNativeName = "Falánga ya Kongó"
    $cib.CultureEnglishName = "Lingala (Congo DRC)"
    $cib.CultureNativeName = "lingála (Republíki ya Kongó Demokratíki)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lin"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ln"

    $cib.GregorianDateTimeFormat.AMDesignator = "ntɔ́ngɔ́"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FC"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ln-CF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Central African Republic"
    $cib.RegionNativeName = "Repibiki ya Afríka ya Káti"
    $cib.ThreeLetterISORegionName = "CAF"
    $cib.TwoLetterISORegionName = "CF"
    $cib.ThreeLetterWindowsRegionName = "CAF"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Falánga CFA BEAC"
    $cib.CultureEnglishName = "Lingala (Central African Republic)"
    $cib.CultureNativeName = "lingála (Repibiki ya Afríka ya Káti)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lin"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ln"

    $cib.GregorianDateTimeFormat.AMDesignator = "ntɔ́ngɔ́"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ln-CG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Congo"
    $cib.RegionNativeName = "Kongo"
    $cib.ThreeLetterISORegionName = "COG"
    $cib.TwoLetterISORegionName = "CG"
    $cib.ThreeLetterWindowsRegionName = "COG"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Falánga CFA BEAC"
    $cib.CultureEnglishName = "Lingala (Congo)"
    $cib.CultureNativeName = "lingála (Kongo)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lin"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ln"

    $cib.GregorianDateTimeFormat.AMDesignator = "ntɔ́ngɔ́"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "lrc-IQ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Iraq"
    $cib.RegionNativeName = "Iraq"
    $cib.ThreeLetterISORegionName = "IRQ"
    $cib.TwoLetterISORegionName = "IQ"
    $cib.ThreeLetterWindowsRegionName = "IRQ"
    $cib.CurrencyEnglishName = "Iraqi Dinar"
    $cib.CurrencyNativeName = "Iraqi Dinar"
    $cib.CultureEnglishName = "Northern Luri (Iraq)"
    $cib.CultureNativeName = "لۊری شومالی (Iraq)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lrc"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "lrc"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "د.ع.‏"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "lrc-IR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Iran"
    $cib.RegionNativeName = "Iran"
    $cib.ThreeLetterISORegionName = "IRN"
    $cib.TwoLetterISORegionName = "IR"
    $cib.ThreeLetterWindowsRegionName = "IRN"
    $cib.CurrencyEnglishName = "Iranian Rial"
    $cib.CurrencyNativeName = "Iranian Rial"
    $cib.CultureEnglishName = "Northern Luri (Iran)"
    $cib.CultureNativeName = "لۊری شومالی (Iran)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lrc"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "lrc"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM, yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "IRR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "lu"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Luxembourg"
    $cib.RegionNativeName = "Luxemburg"
    $cib.ThreeLetterISORegionName = "LUX"
    $cib.TwoLetterISORegionName = "LU"
    $cib.ThreeLetterWindowsRegionName = "LUX"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "Luba-Katanga"
    $cib.CultureNativeName = "Tshiluba"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lub"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "lu"

    $cib.GregorianDateTimeFormat.AMDesignator = "Dinda"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "FC"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "lu-CD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Congo (DRC)"
    $cib.RegionNativeName = "Ditunga wa Kongu"
    $cib.ThreeLetterISORegionName = "COD"
    $cib.TwoLetterISORegionName = "CD"
    $cib.ThreeLetterWindowsRegionName = "COD"
    $cib.CurrencyEnglishName = "Congolese Franc"
    $cib.CurrencyNativeName = "Nfalanga wa Kongu"
    $cib.CultureEnglishName = "Luba-Katanga (Congo DRC)"
    $cib.CultureNativeName = "Tshiluba (Ditunga wa Kongu)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "lub"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "lu"

    $cib.GregorianDateTimeFormat.AMDesignator = "Dinda"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "FC"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "luo-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Siling mar Kenya"
    $cib.CultureEnglishName = "Luo (Kenya)"
    $cib.CultureNativeName = "Dholuo (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "luo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "luo"

    $cib.GregorianDateTimeFormat.AMDesignator = "OD"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "luy-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Sirinji ya Kenya"
    $cib.CultureEnglishName = "Luyia (Kenya)"
    $cib.CultureNativeName = "Luluhia (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "luy"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "luy"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 2
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mas-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Iropiyianí e Kenya"
    $cib.CultureEnglishName = "Masai (Kenya)"
    $cib.CultureNativeName = "Maa (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mas"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mas"

    $cib.GregorianDateTimeFormat.AMDesignator = "Ɛnkakɛnyá"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mas-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tansania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Iropiyianí e Tanzania"
    $cib.CultureEnglishName = "Masai (Tanzania)"
    $cib.CultureNativeName = "Maa (Tansania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mas"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mas"

    $cib.GregorianDateTimeFormat.AMDesignator = "Ɛnkakɛnyá"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mer-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Kenya"
    $cib.CultureEnglishName = "Meru (Kenya)"
    $cib.CultureNativeName = "Kĩmĩrũ (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mer"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mer"

    $cib.GregorianDateTimeFormat.AMDesignator = "RŨ"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mfe-MU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mauritius"
    $cib.RegionNativeName = "Moris"
    $cib.ThreeLetterISORegionName = "MUS"
    $cib.TwoLetterISORegionName = "MU"
    $cib.ThreeLetterWindowsRegionName = "MUS"
    $cib.CurrencyEnglishName = "Mauritian Rupee"
    $cib.CurrencyNativeName = "roupi morisien"
    $cib.CultureEnglishName = "Morisyen (Mauritius)"
    $cib.CultureNativeName = "kreol morisien (Moris)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mfe"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mfe"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "Rs"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mg"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Madagascar"
    $cib.RegionNativeName = "Madagascar"
    $cib.ThreeLetterISORegionName = "MDG"
    $cib.TwoLetterISORegionName = "MG"
    $cib.ThreeLetterWindowsRegionName = "MDG"
    $cib.CurrencyEnglishName = "Malagasy Ariary"
    $cib.CurrencyNativeName = "Malagasy Ariary"
    $cib.CultureEnglishName = "Malagasy"
    $cib.CultureNativeName = "Malagasy"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mlg"
    $cib.ThreeLetterWindowsLanguageName = "MLG"
    $cib.TwoLetterISOLanguageName = "mg"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "Ar"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mg-MG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Madagascar"
    $cib.RegionNativeName = "Madagasikara"
    $cib.ThreeLetterISORegionName = "MDG"
    $cib.TwoLetterISORegionName = "MG"
    $cib.ThreeLetterWindowsRegionName = "MDG"
    $cib.CurrencyEnglishName = "Malagasy Ariary"
    $cib.CurrencyNativeName = "Ariary"
    $cib.CultureEnglishName = "Malagasy (Madagascar)"
    $cib.CultureNativeName = "Malagasy (Madagasikara)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mlg"
    $cib.ThreeLetterWindowsLanguageName = "MLG"
    $cib.TwoLetterISOLanguageName = "mg"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "Ar"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mgh-MZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mozambique"
    $cib.RegionNativeName = "Umozambiki"
    $cib.ThreeLetterISORegionName = "MOZ"
    $cib.TwoLetterISORegionName = "MZ"
    $cib.ThreeLetterWindowsRegionName = "MOZ"
    $cib.CurrencyEnglishName = "Mozambican Metical"
    $cib.CurrencyNativeName = "Mozambican Metical"
    $cib.CultureEnglishName = "Makhuwa-Meetto (Mozambique)"
    $cib.CultureNativeName = "Makua (Umozambiki)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mgh"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mgh"

    $cib.GregorianDateTimeFormat.AMDesignator = "wichishu"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "MTn"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mgo-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kamalun"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "shirè"
    $cib.CultureEnglishName = "Metaʼ (Cameroon)"
    $cib.CultureNativeName = "metaʼ (Kamalun)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mgo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mgo"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, yyyy MMMM dd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, yyyy MMMM dd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ms-SG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Singapore"
    $cib.RegionNativeName = "Singapura"
    $cib.ThreeLetterISORegionName = "SGP"
    $cib.TwoLetterISORegionName = "SG"
    $cib.ThreeLetterWindowsRegionName = "SGP"
    $cib.CurrencyEnglishName = "Singapore Dollar"
    $cib.CurrencyNativeName = "Dolar Singapura"
    $cib.CultureEnglishName = "Malay (Singapore)"
    $cib.CultureNativeName = "Melayu (Singapura)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "msa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ms"

    $cib.GregorianDateTimeFormat.AMDesignator = "PG"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mua-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "kameruŋ"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "solai BEAC"
    $cib.CultureEnglishName = "Mundang (Cameroon)"
    $cib.CultureNativeName = "MUNDAŊ (kameruŋ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mua"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mua"

    $cib.GregorianDateTimeFormat.AMDesignator = "comme"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "mzn-IR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Iran"
    $cib.RegionNativeName = "ایران"
    $cib.ThreeLetterISORegionName = "IRN"
    $cib.TwoLetterISORegionName = "IR"
    $cib.ThreeLetterWindowsRegionName = "IRN"
    $cib.CurrencyEnglishName = "Iranian Rial"
    $cib.CurrencyNativeName = "ایران ریال"
    $cib.CultureEnglishName = "Mazanderani (Iran)"
    $cib.CultureNativeName = "مازرونی (ایران)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "mzn"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "mzn"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM, yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "IRR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "naq-NA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Namibia"
    $cib.RegionNativeName = "Namibiab"
    $cib.ThreeLetterISORegionName = "NAM"
    $cib.TwoLetterISORegionName = "NA"
    $cib.ThreeLetterWindowsRegionName = "NAM"
    $cib.CurrencyEnglishName = "Namibian Dollar"
    $cib.CurrencyNativeName = "Namibia Dollari"
    $cib.CultureEnglishName = "Nama (Namibia)"
    $cib.CultureNativeName = "Khoekhoegowab (Namibiab)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "naq"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "naq"

    $cib.GregorianDateTimeFormat.AMDesignator = "ǁgoagas"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nb-SJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Svalbard and Jan Mayen"
    $cib.RegionNativeName = "Svalbard og Jan Mayen"
    $cib.ThreeLetterISORegionName = "SJM"
    $cib.TwoLetterISORegionName = "SJ"
    $cib.ThreeLetterWindowsRegionName = "SJM"
    $cib.CurrencyEnglishName = "Norwegian Krone"
    $cib.CurrencyNativeName = "norske kroner"
    $cib.CultureEnglishName = "Norwegian Bokmål (Svalbard and Jan Mayen)"
    $cib.CultureNativeName = "norsk bokmål (Svalbard og Jan Mayen)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nob"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nb"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "kr"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nd-ZW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Zimbabwe"
    $cib.RegionNativeName = "Zimbabwe"
    $cib.ThreeLetterISORegionName = "ZWE"
    $cib.TwoLetterISORegionName = "ZW"
    $cib.ThreeLetterWindowsRegionName = "ZWE"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "Dola yase Amelika"
    $cib.CultureEnglishName = "North Ndebele (Zimbabwe)"
    $cib.CultureNativeName = "isiNdebele (Zimbabwe)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nde"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nd"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nds-DE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Germany"
    $cib.RegionNativeName = "Düütschland"
    $cib.ThreeLetterISORegionName = "DEU"
    $cib.TwoLetterISORegionName = "DE"
    $cib.ThreeLetterWindowsRegionName = "DEU"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "Low German (Germany)"
    $cib.CultureNativeName = "Neddersass’sch (Düütschland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nds"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nds"

    $cib.GregorianDateTimeFormat.AMDesignator = "vm"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, 'de' d. MMMM yyyy 'Klock' H.mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, 'de' d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "'Klock' H.mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "'Kl'. H.mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = "."
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nds-NL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Netherlands"
    $cib.RegionNativeName = "Nedderlannen"
    $cib.ThreeLetterISORegionName = "NLD"
    $cib.TwoLetterISORegionName = "NL"
    $cib.ThreeLetterWindowsRegionName = "NLD"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "Low German (Netherlands)"
    $cib.CultureNativeName = "Neddersass’sch (Nedderlannen)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nds"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nds"

    $cib.GregorianDateTimeFormat.AMDesignator = "vm"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, 'de' d. MMMM yyyy 'Klock' H.mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, 'de' d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "'Klock' H.mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "'Kl'. H.mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = "."
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nl-AW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Aruba"
    $cib.RegionNativeName = "Aruba"
    $cib.ThreeLetterISORegionName = "ABW"
    $cib.TwoLetterISORegionName = "AW"
    $cib.ThreeLetterWindowsRegionName = "ABW"
    $cib.CurrencyEnglishName = "Aruban Florin"
    $cib.CurrencyNativeName = "Arubaanse gulden"
    $cib.CultureEnglishName = "Dutch (Aruba)"
    $cib.CultureNativeName = "Nederlands (Aruba)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nld"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nl"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd-MM-yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 12
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "Afl."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nl-BQ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Bonaire, Sint Eustatius and Saba"
    $cib.RegionNativeName = "Bonaire, Sint Eustatius en Saba"
    $cib.ThreeLetterISORegionName = "BES"
    $cib.TwoLetterISORegionName = "BQ"
    $cib.ThreeLetterWindowsRegionName = "BES"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "Amerikaanse dollar"
    $cib.CultureEnglishName = "Dutch (Bonaire, Sint Eustatius and Saba)"
    $cib.CultureNativeName = "Nederlands (Bonaire, Sint Eustatius en Saba)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nld"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nl"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd-MM-yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 12
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nl-CW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Curaçao"
    $cib.RegionNativeName = "Curaçao"
    $cib.ThreeLetterISORegionName = "CUW"
    $cib.TwoLetterISORegionName = "CW"
    $cib.ThreeLetterWindowsRegionName = "CUW"
    $cib.CurrencyEnglishName = "Netherlands Antillean Guilder"
    $cib.CurrencyNativeName = "Nederlands-Antilliaanse gulden"
    $cib.CultureEnglishName = "Dutch (Curaçao)"
    $cib.CultureNativeName = "Nederlands (Curaçao)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nld"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nl"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd-MM-yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 12
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "NAf."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nl-SR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Suriname"
    $cib.RegionNativeName = "Suriname"
    $cib.ThreeLetterISORegionName = "SUR"
    $cib.TwoLetterISORegionName = "SR"
    $cib.ThreeLetterWindowsRegionName = "SUR"
    $cib.CurrencyEnglishName = "Surinamese Dollar"
    $cib.CurrencyNativeName = "Surinaamse dollar"
    $cib.CultureEnglishName = "Dutch (Suriname)"
    $cib.CultureNativeName = "Nederlands (Suriname)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nld"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nl"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd-MM-yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 12
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nl-SX"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Sint Maarten"
    $cib.RegionNativeName = "Sint-Maarten"
    $cib.ThreeLetterISORegionName = "SXM"
    $cib.TwoLetterISORegionName = "SX"
    $cib.ThreeLetterWindowsRegionName = "SXM"
    $cib.CurrencyEnglishName = "Netherlands Antillean Guilder"
    $cib.CurrencyNativeName = "Nederlands-Antilliaanse gulden"
    $cib.CultureEnglishName = "Dutch (Sint Maarten)"
    $cib.CultureNativeName = "Nederlands (Sint-Maarten)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nld"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nl"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd-MM-yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 12
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "NAf."
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nmg-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kamerun"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Fraŋ CFA BEAC"
    $cib.CultureEnglishName = "Kwasio (Cameroon)"
    $cib.CultureNativeName = "Kwasio (Kamerun)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nmg"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nmg"

    $cib.GregorianDateTimeFormat.AMDesignator = "maná"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nnh-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kàmalûm"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "feláŋ CFA"
    $cib.CultureEnglishName = "Ngiemboon (Cameroon)"
    $cib.CultureNativeName = "Shwóŋò ngiembɔɔn (Kàmalûm)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nnh"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nnh"

    $cib.GregorianDateTimeFormat.AMDesignator = "mbaʼámbaʼ"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd , 'lyɛ'̌ʼ d 'na' MMMM, yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd , 'lyɛ'̌ʼ d 'na' MMMM, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nqo-GN"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guinea"
    $cib.RegionNativeName = "ߖߌ߬ߣߍ߬ ߞߊ߲ߓߍ߲"
    $cib.ThreeLetterISORegionName = "GIN"
    $cib.TwoLetterISORegionName = "GN"
    $cib.ThreeLetterWindowsRegionName = "GIN"
    $cib.CurrencyEnglishName = "Guinean Franc"
    $cib.CurrencyNativeName = "ߖߌ߬ߣߍ߬ ߕߊߡߊ߲"
    $cib.CultureEnglishName = "N'ko (Guinea)"
    $cib.CultureNativeName = "ߒߞߏ (ߖߌ߬ߣߍ߬ ߞߊ߲ߓߍ߲)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nqo"
    $cib.ThreeLetterWindowsLanguageName = "NQO"
    $cib.TwoLetterISOLanguageName = "nqo"

    $cib.GregorianDateTimeFormat.AMDesignator = "ߛ"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy tt hh:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tt hh:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM ߕߟߋ߬ dd"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tt hh:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM, yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 13
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "ߖߕ."
    $cib.NumberFormat.NegativeInfinitySymbol = "ߘߊ߲߬ߒߕߊ߲߫-"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 3
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 3
    $cib.NumberFormat.PercentDecimalDigits = 3
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 3
    $cib.NumberFormat.PercentPositivePattern = 2
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "ߘߊ߲߬ߒߕߊ߲߫+"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nr"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Nauru"
    $cib.RegionNativeName = "Nauru"
    $cib.ThreeLetterISORegionName = "NRU"
    $cib.TwoLetterISORegionName = "NR"
    $cib.ThreeLetterWindowsRegionName = "NRU"
    $cib.CurrencyEnglishName = "Australian Dollar"
    $cib.CurrencyNativeName = "Australian Dollar"
    $cib.CultureEnglishName = "South Ndebele"
    $cib.CultureNativeName = "isiNdebele"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nbl"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "R"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nr-ZA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "South Africa"
    $cib.RegionNativeName = "South Africa"
    $cib.ThreeLetterISORegionName = "ZAF"
    $cib.TwoLetterISORegionName = "ZA"
    $cib.ThreeLetterWindowsRegionName = "ZAF"
    $cib.CurrencyEnglishName = "South African Rand"
    $cib.CurrencyNativeName = "South African Rand"
    $cib.CultureEnglishName = "South Ndebele (South Africa)"
    $cib.CultureNativeName = "isiNdebele (South Africa)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nbl"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nr"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "R"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nus-SS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "South Sudan"
    $cib.RegionNativeName = "South Sudan"
    $cib.ThreeLetterISORegionName = "SSD"
    $cib.TwoLetterISORegionName = "SS"
    $cib.ThreeLetterWindowsRegionName = "SSD"
    $cib.CurrencyEnglishName = "South Sudanese Pound"
    $cib.CurrencyNativeName = "South Sudanese Pound"
    $cib.CultureEnglishName = "Nuer (South Sudan)"
    $cib.CultureNativeName = "Thok Nath (South Sudan)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nus"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nus"

    $cib.GregorianDateTimeFormat.AMDesignator = "RW"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "£"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "nyn-UG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Uganda"
    $cib.RegionNativeName = "Uganda"
    $cib.ThreeLetterISORegionName = "UGA"
    $cib.TwoLetterISORegionName = "UG"
    $cib.ThreeLetterWindowsRegionName = "UGA"
    $cib.CurrencyEnglishName = "Ugandan Shilling"
    $cib.CurrencyNativeName = "Eshiringi ya Uganda"
    $cib.CultureEnglishName = "Nyankole (Uganda)"
    $cib.CultureNativeName = "Runyankore (Uganda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "nyn"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "nyn"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "USh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "om-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Keeniyaa"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Kenyan Shilling"
    $cib.CultureEnglishName = "Oromo (Kenya)"
    $cib.CultureNativeName = "Oromoo (Keeniyaa)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "orm"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "om"

    $cib.GregorianDateTimeFormat.AMDesignator = "WD"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM d, yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM d, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "os-GE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Georgia"
    $cib.RegionNativeName = "Гуырдзыстон"
    $cib.ThreeLetterISORegionName = "GEO"
    $cib.TwoLetterISORegionName = "GE"
    $cib.ThreeLetterWindowsRegionName = "GEO"
    $cib.CurrencyEnglishName = "Georgian Lari"
    $cib.CurrencyNativeName = "Лар"
    $cib.CultureEnglishName = "Ossetic (Georgia)"
    $cib.CultureNativeName = "ирон (Гуырдзыстон)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "oss"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "os"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM, yyyy 'аз' HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM, yyyy 'аз'"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "₾"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "os-RU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Russia"
    $cib.RegionNativeName = "Уӕрӕсе"
    $cib.ThreeLetterISORegionName = "RUS"
    $cib.TwoLetterISORegionName = "RU"
    $cib.ThreeLetterWindowsRegionName = "RUS"
    $cib.CurrencyEnglishName = "Russian Ruble"
    $cib.CurrencyNativeName = "Сом"
    $cib.CultureEnglishName = "Ossetic (Russia)"
    $cib.CultureNativeName = "ирон (Уӕрӕсе)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "oss"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "os"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM, yyyy 'аз' HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM, yyyy 'аз'"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "₽"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "prg-001"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "World"
    $cib.RegionNativeName = "swītai"
    $cib.ThreeLetterISORegionName = "001"
    $cib.TwoLetterISORegionName = "001"
    $cib.ThreeLetterWindowsRegionName = "001"
    $cib.CurrencyEnglishName = "Special Drawing Rights"
    $cib.CurrencyNativeName = "Special Drawing Rights"
    $cib.CultureEnglishName = "Prussian (World)"
    $cib.CultureNativeName = "prūsiskan (swītai)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "prg"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "prg"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, yyyy 'mettas' d. MMMM HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, yyyy 'mettas' d. MMMM"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "XDR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-AO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Angola"
    $cib.RegionNativeName = "Angola"
    $cib.ThreeLetterISORegionName = "AGO"
    $cib.TwoLetterISORegionName = "AO"
    $cib.ThreeLetterWindowsRegionName = "AGO"
    $cib.CurrencyEnglishName = "Angolan Kwanza"
    $cib.CurrencyNativeName = "Kwanza angolano"
    $cib.CultureEnglishName = "Portuguese (Angola)"
    $cib.CultureNativeName = "português (Angola)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "PTA"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "Kz"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-CH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Switzerland"
    $cib.RegionNativeName = "Suíça"
    $cib.ThreeLetterISORegionName = "CHE"
    $cib.TwoLetterISORegionName = "CH"
    $cib.ThreeLetterWindowsRegionName = "CHE"
    $cib.CurrencyEnglishName = "Swiss Franc"
    $cib.CurrencyNativeName = "franco suíço"
    $cib.CultureEnglishName = "Portuguese (Switzerland)"
    $cib.CultureNativeName = "português (Suíça)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CHF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-CV"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cabo Verde"
    $cib.RegionNativeName = "Cabo Verde"
    $cib.ThreeLetterISORegionName = "CPV"
    $cib.TwoLetterISORegionName = "CV"
    $cib.ThreeLetterWindowsRegionName = "CPV"
    $cib.CurrencyEnglishName = "Cape Verdean Escudo"
    $cib.CurrencyNativeName = "escudo cabo-verdiano"
    $cib.CultureEnglishName = "Portuguese (Cabo Verde)"
    $cib.CultureNativeName = "português (Cabo Verde)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "$"
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "​"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-GQ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Equatorial Guinea"
    $cib.RegionNativeName = "Guiné Equatorial"
    $cib.ThreeLetterISORegionName = "GNQ"
    $cib.TwoLetterISORegionName = "GQ"
    $cib.ThreeLetterWindowsRegionName = "GNQ"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Franco CFA (BEAC)"
    $cib.CultureEnglishName = "Portuguese (Equatorial Guinea)"
    $cib.CultureNativeName = "português (Guiné Equatorial)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-GW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Guinea-Bissau"
    $cib.RegionNativeName = "Guiné-Bissau"
    $cib.ThreeLetterISORegionName = "GNB"
    $cib.TwoLetterISORegionName = "GW"
    $cib.ThreeLetterWindowsRegionName = "GNB"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "franco CFA (BCEAO)"
    $cib.CultureEnglishName = "Portuguese (Guinea-Bissau)"
    $cib.CultureNativeName = "português (Guiné-Bissau)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-LU"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Luxembourg"
    $cib.RegionNativeName = "Luxemburgo"
    $cib.ThreeLetterISORegionName = "LUX"
    $cib.TwoLetterISORegionName = "LU"
    $cib.ThreeLetterWindowsRegionName = "LUX"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Portuguese (Luxembourg)"
    $cib.CultureNativeName = "português (Luxemburgo)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-MO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Macao SAR"
    $cib.RegionNativeName = "RAE de Macau"
    $cib.ThreeLetterISORegionName = "MAC"
    $cib.TwoLetterISORegionName = "MO"
    $cib.ThreeLetterWindowsRegionName = "MAC"
    $cib.CurrencyEnglishName = "Macanese Pataca"
    $cib.CurrencyNativeName = "Pataca de Macau"
    $cib.CultureEnglishName = "Portuguese (Macao SAR)"
    $cib.CultureNativeName = "português (RAE de Macau)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "MOP$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-MZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mozambique"
    $cib.RegionNativeName = "Moçambique"
    $cib.ThreeLetterISORegionName = "MOZ"
    $cib.TwoLetterISORegionName = "MZ"
    $cib.ThreeLetterWindowsRegionName = "MOZ"
    $cib.CurrencyEnglishName = "Mozambican Metical"
    $cib.CurrencyNativeName = "Metical de Moçambique"
    $cib.CultureEnglishName = "Portuguese (Mozambique)"
    $cib.CultureNativeName = "português (Moçambique)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "MTn"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-ST"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "São Tomé and Príncipe"
    $cib.RegionNativeName = "São Tomé e Príncipe"
    $cib.ThreeLetterISORegionName = "STP"
    $cib.TwoLetterISORegionName = "ST"
    $cib.ThreeLetterWindowsRegionName = "STP"
    $cib.CurrencyEnglishName = "São Tomé and Príncipe Dobra"
    $cib.CurrencyNativeName = "São Tomé & Príncipe Dobra (2018)"
    $cib.CultureEnglishName = "Portuguese (São Tomé and Príncipe)"
    $cib.CultureNativeName = "português (São Tomé e Príncipe)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "Db"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "pt-TL"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Timor-Leste"
    $cib.RegionNativeName = "Timor-Leste"
    $cib.ThreeLetterISORegionName = "TLS"
    $cib.TwoLetterISORegionName = "TL"
    $cib.ThreeLetterWindowsRegionName = "TLS"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "dólar dos Estados Unidos"
    $cib.CultureEnglishName = "Portuguese (Timor-Leste)"
    $cib.CultureNativeName = "português (Timor-Leste)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "por"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "pt"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d 'de' MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "rn-BI"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Burundi"
    $cib.RegionNativeName = "Uburundi"
    $cib.ThreeLetterISORegionName = "BDI"
    $cib.TwoLetterISORegionName = "BI"
    $cib.ThreeLetterWindowsRegionName = "BDI"
    $cib.CurrencyEnglishName = "Burundian Franc"
    $cib.CurrencyNativeName = "Ifaranga ry’Uburundi"
    $cib.CultureEnglishName = "Rundi (Burundi)"
    $cib.CultureNativeName = "Ikirundi (Uburundi)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "run"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "rn"

    $cib.GregorianDateTimeFormat.AMDesignator = "Z.MU."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "FBu"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ro-MD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Moldova"
    $cib.RegionNativeName = "Republica Moldova"
    $cib.ThreeLetterISORegionName = "MDA"
    $cib.TwoLetterISORegionName = "MD"
    $cib.ThreeLetterWindowsRegionName = "MDA"
    $cib.CurrencyEnglishName = "Moldovan Leu"
    $cib.CurrencyNativeName = "leu moldovenesc"
    $cib.CultureEnglishName = "Romanian (Moldova)"
    $cib.CultureNativeName = "română (Republica Moldova)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ron"
    $cib.ThreeLetterWindowsLanguageName = "ROD"
    $cib.TwoLetterISOLanguageName = "ro"

    $cib.GregorianDateTimeFormat.AMDesignator = "a.m."
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "L"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "rof-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "heleri sa Tanzania"
    $cib.CultureEnglishName = "Rombo (Tanzania)"
    $cib.CultureNativeName = "Kihorombo (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "rof"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "rof"

    $cib.GregorianDateTimeFormat.AMDesignator = "kang’ama"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ru-BY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Belarus"
    $cib.RegionNativeName = "Беларусь"
    $cib.ThreeLetterISORegionName = "BLR"
    $cib.TwoLetterISORegionName = "BY"
    $cib.ThreeLetterWindowsRegionName = "BLR"
    $cib.CurrencyEnglishName = "Belarusian Ruble"
    $cib.CurrencyNativeName = "белорусский рубль"
    $cib.CultureEnglishName = "Russian (Belarus)"
    $cib.CultureNativeName = "русский (Беларусь)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "rus"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ru"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy 'г'. H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy 'г'."
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy 'г'."

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "Br"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ru-KG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kyrgyzstan"
    $cib.RegionNativeName = "Киргизия"
    $cib.ThreeLetterISORegionName = "KGZ"
    $cib.TwoLetterISORegionName = "KG"
    $cib.ThreeLetterWindowsRegionName = "KGZ"
    $cib.CurrencyEnglishName = "Kyrgystani Som"
    $cib.CurrencyNativeName = "киргизский сом"
    $cib.CultureEnglishName = "Russian (Kyrgyzstan)"
    $cib.CultureNativeName = "русский (Киргизия)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "rus"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ru"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy 'г'. H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy 'г'."
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy 'г'."

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "сом"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ru-KZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kazakhstan"
    $cib.RegionNativeName = "Казахстан"
    $cib.ThreeLetterISORegionName = "KAZ"
    $cib.TwoLetterISORegionName = "KZ"
    $cib.ThreeLetterWindowsRegionName = "KAZ"
    $cib.CurrencyEnglishName = "Kazakhstani Tenge"
    $cib.CurrencyNativeName = "казахский тенге"
    $cib.CultureEnglishName = "Russian (Kazakhstan)"
    $cib.CultureNativeName = "русский (Казахстан)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "rus"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ru"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy 'г'. H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy 'г'."
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy 'г'."

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "₸"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ru-UA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ukraine"
    $cib.RegionNativeName = "Украина"
    $cib.ThreeLetterISORegionName = "UKR"
    $cib.TwoLetterISORegionName = "UA"
    $cib.ThreeLetterWindowsRegionName = "UKR"
    $cib.CurrencyEnglishName = "Ukrainian Hryvnia"
    $cib.CurrencyNativeName = "украинская гривна"
    $cib.CultureEnglishName = "Russian (Ukraine)"
    $cib.CultureNativeName = "русский (Украина)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "rus"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ru"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy 'г'. HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy 'г'."
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy 'г'."

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "₴"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "rwk-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Tanzania"
    $cib.CultureEnglishName = "Rwa (Tanzania)"
    $cib.CultureNativeName = "Kiruwa (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "rwk"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "rwk"

    $cib.GregorianDateTimeFormat.AMDesignator = "utuko"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "saq-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Njilingi eel Kenya"
    $cib.CultureEnglishName = "Samburu (Kenya)"
    $cib.CultureNativeName = "Kisampur (Kenya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "saq"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "saq"

    $cib.GregorianDateTimeFormat.AMDesignator = "Tesiran"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sbp-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tansaniya"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Ihela ya Tansaniya"
    $cib.CultureEnglishName = "Sangu (Tanzania)"
    $cib.CultureNativeName = "Ishisangu (Tansaniya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sbp"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sbp"

    $cib.GregorianDateTimeFormat.AMDesignator = "Lwamilawu"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "seh-MZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mozambique"
    $cib.RegionNativeName = "Moçambique"
    $cib.ThreeLetterISORegionName = "MOZ"
    $cib.TwoLetterISORegionName = "MZ"
    $cib.ThreeLetterWindowsRegionName = "MOZ"
    $cib.CurrencyEnglishName = "Mozambican Metical"
    $cib.CurrencyNativeName = "Metical de Moçambique"
    $cib.CultureEnglishName = "Sena (Mozambique)"
    $cib.CultureNativeName = "sena (Moçambique)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "seh"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "seh"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d 'de' MMMM 'de' yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d 'de' MMMM 'de' yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM 'de' yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "MTn"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ses-ML"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Mali"
    $cib.RegionNativeName = "Maali"
    $cib.ThreeLetterISORegionName = "MLI"
    $cib.TwoLetterISORegionName = "ML"
    $cib.ThreeLetterWindowsRegionName = "MLI"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "CFA Fraŋ (BCEAO)"
    $cib.CultureEnglishName = "Koyraboro Senni (Mali)"
    $cib.CultureNativeName = "Koyraboro senni (Maali)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ses"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ses"

    $cib.GregorianDateTimeFormat.AMDesignator = "Adduha"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sg"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Singapore"
    $cib.RegionNativeName = "Singapore"
    $cib.ThreeLetterISORegionName = "SGP"
    $cib.TwoLetterISORegionName = "SG"
    $cib.ThreeLetterWindowsRegionName = "SGP"
    $cib.CurrencyEnglishName = "Singapore Dollar"
    $cib.CurrencyNativeName = "Singapore Dollar"
    $cib.CultureEnglishName = "Sango"
    $cib.CultureNativeName = "Sängö"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sag"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sg"

    $cib.GregorianDateTimeFormat.AMDesignator = "ND"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 2
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sg-CF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Central African Republic"
    $cib.RegionNativeName = "Ködörösêse tî Bêafrîka"
    $cib.ThreeLetterISORegionName = "CAF"
    $cib.TwoLetterISORegionName = "CF"
    $cib.ThreeLetterWindowsRegionName = "CAF"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "farânga CFA (BEAC)"
    $cib.CultureEnglishName = "Sango (Central African Republic)"
    $cib.CultureNativeName = "Sängö (Ködörösêse tî Bêafrîka)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sag"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sg"

    $cib.GregorianDateTimeFormat.AMDesignator = "ND"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 2
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "shi-Latn-MA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Morocco"
    $cib.RegionNativeName = "lmɣrib"
    $cib.ThreeLetterISORegionName = "MAR"
    $cib.TwoLetterISORegionName = "MA"
    $cib.ThreeLetterWindowsRegionName = "MAR"
    $cib.CurrencyEnglishName = "Moroccan Dirham"
    $cib.CurrencyNativeName = "adrim n lmɣrib"
    $cib.CultureEnglishName = "Tachelhit (Latin, Morocco)"
    $cib.CultureNativeName = "Tashelḥiyt (lmɣrib)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "shi"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "shi"

    $cib.GregorianDateTimeFormat.AMDesignator = "tifawt"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "MAD"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "shi-Tfng-MA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Morocco"
    $cib.RegionNativeName = "ⵍⵎⵖⵔⵉⴱ"
    $cib.ThreeLetterISORegionName = "MAR"
    $cib.TwoLetterISORegionName = "MA"
    $cib.ThreeLetterWindowsRegionName = "MAR"
    $cib.CurrencyEnglishName = "Moroccan Dirham"
    $cib.CurrencyNativeName = "ⴰⴷⵔⵉⵎ ⵏ ⵍⵎⵖⵔⵉⴱ"
    $cib.CultureEnglishName = "Tachelhit (Tifinagh, Morocco)"
    $cib.CultureNativeName = "ⵜⴰⵛⵍⵃⵉⵜ (ⵍⵎⵖⵔⵉⴱ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "shi"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "shi"

    $cib.GregorianDateTimeFormat.AMDesignator = "ⵜⵉⴼⴰⵡⵜ"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "MAD"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sn"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Senegal"
    $cib.RegionNativeName = "Senegal"
    $cib.ThreeLetterISORegionName = "SEN"
    $cib.TwoLetterISORegionName = "SN"
    $cib.ThreeLetterWindowsRegionName = "SEN"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "seefa yati BCEAO"
    $cib.CultureEnglishName = "Shona"
    $cib.CultureNativeName = "chiShona"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sna"
    $cib.ThreeLetterWindowsLanguageName = "SNA"
    $cib.TwoLetterISOLanguageName = "sn"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sn-Latn-ZW"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Zimbabwe"
    $cib.RegionNativeName = "Zimbabwe"
    $cib.ThreeLetterISORegionName = "ZWE"
    $cib.TwoLetterISORegionName = "ZW"
    $cib.ThreeLetterWindowsRegionName = "ZWE"
    $cib.CurrencyEnglishName = "US Dollar"
    $cib.CurrencyNativeName = "Dora re Amerika"
    $cib.CultureEnglishName = "Shona (Latin, Zimbabwe)"
    $cib.CultureNativeName = "chiShona (Zimbabwe)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sna"
    $cib.ThreeLetterWindowsLanguageName = "SNA"
    $cib.TwoLetterISOLanguageName = "sn"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "so-DJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Djibouti"
    $cib.RegionNativeName = "Jabuuti"
    $cib.ThreeLetterISORegionName = "DJI"
    $cib.TwoLetterISORegionName = "DJ"
    $cib.ThreeLetterWindowsRegionName = "DJI"
    $cib.CurrencyEnglishName = "Djiboutian Franc"
    $cib.CurrencyNativeName = "Faran Jabbuuti"
    $cib.CultureEnglishName = "Somali (Djibouti)"
    $cib.CultureNativeName = "Soomaali (Jabuuti)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "som"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "so"

    $cib.GregorianDateTimeFormat.AMDesignator = "sn."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Fdj"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "so-ET"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ethiopia"
    $cib.RegionNativeName = "Itoobiya"
    $cib.ThreeLetterISORegionName = "ETH"
    $cib.TwoLetterISORegionName = "ET"
    $cib.ThreeLetterWindowsRegionName = "ETH"
    $cib.CurrencyEnglishName = "Ethiopian Birr"
    $cib.CurrencyNativeName = "Birta Itoobbiya"
    $cib.CultureEnglishName = "Somali (Ethiopia)"
    $cib.CultureNativeName = "Soomaali (Itoobiya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "som"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "so"

    $cib.GregorianDateTimeFormat.AMDesignator = "sn."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Br"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "so-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kiiniya"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Kenyan Shilling"
    $cib.CultureEnglishName = "Somali (Kenya)"
    $cib.CultureNativeName = "Soomaali (Kiiniya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "som"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "so"

    $cib.GregorianDateTimeFormat.AMDesignator = "sn."
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sq-MK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Macedonia, FYRO"
    $cib.RegionNativeName = "Republika e Maqedonisë"
    $cib.ThreeLetterISORegionName = "MKD"
    $cib.TwoLetterISORegionName = "MK"
    $cib.ThreeLetterWindowsRegionName = "MKD"
    $cib.CurrencyEnglishName = "Macedonian Denar"
    $cib.CurrencyNativeName = "Denari maqedonas"
    $cib.CultureEnglishName = "Albanian (Macedonia, FYRO)"
    $cib.CultureNativeName = "shqip (Republika e Maqedonisë)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sqi"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sq"

    $cib.GregorianDateTimeFormat.AMDesignator = "e paradites"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d.M.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "den"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sq-XK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kosovo"
    $cib.RegionNativeName = "Kosovë"
    $cib.ThreeLetterISORegionName = "XKS"
    $cib.TwoLetterISORegionName = "XK"
    $cib.ThreeLetterWindowsRegionName = "XKS"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euroja"
    $cib.CultureEnglishName = "Albanian (Kosovo)"
    $cib.CultureNativeName = "shqip (Kosovë)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sqi"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sq"

    $cib.GregorianDateTimeFormat.AMDesignator = "e paradites"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d.M.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sr-Cyrl-XK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kosovo"
    $cib.RegionNativeName = "Косово"
    $cib.ThreeLetterISORegionName = "XKS"
    $cib.TwoLetterISORegionName = "XK"
    $cib.ThreeLetterWindowsRegionName = "XKS"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Евро"
    $cib.CultureEnglishName = "Serbian (Cyrillic, Kosovo)"
    $cib.CultureNativeName = "српски (Косово)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "srp"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sr"

    $cib.GregorianDateTimeFormat.AMDesignator = "пре подне"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, dd. MMMM yyyy. HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, dd. MMMM yyyy."
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d.M.yyyy."
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy."

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sr-Latn-XK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kosovo"
    $cib.RegionNativeName = "Kosovo"
    $cib.ThreeLetterISORegionName = "XKS"
    $cib.TwoLetterISORegionName = "XK"
    $cib.ThreeLetterWindowsRegionName = "XKS"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Evro"
    $cib.CultureEnglishName = "Serbian (Latin, Kosovo)"
    $cib.CultureNativeName = "srpski (Kosovo)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "srp"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sr"

    $cib.GregorianDateTimeFormat.AMDesignator = "pre podne"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, dd. MMMM yyyy. HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, dd. MMMM yyyy."
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d. MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d.M.yyyy."
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy."

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ss"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "South Sudan"
    $cib.RegionNativeName = "جنوب السودان"
    $cib.ThreeLetterISORegionName = "SSD"
    $cib.TwoLetterISORegionName = "SS"
    $cib.ThreeLetterWindowsRegionName = "SSD"
    $cib.CurrencyEnglishName = "South Sudanese Pound"
    $cib.CurrencyNativeName = "جنيه جنوب السودان"
    $cib.CultureEnglishName = "siSwati"
    $cib.CultureNativeName = "Siswati"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ssw"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ss"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "R"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ss-SZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Swaziland"
    $cib.RegionNativeName = "Swaziland"
    $cib.ThreeLetterISORegionName = "SWZ"
    $cib.TwoLetterISORegionName = "SZ"
    $cib.ThreeLetterWindowsRegionName = "SWZ"
    $cib.CurrencyEnglishName = "Swazi Lilangeni"
    $cib.CurrencyNativeName = "Swazi Lilangeni"
    $cib.CultureEnglishName = "siSwati (Swaziland)"
    $cib.CultureNativeName = "siSwati (Swaziland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ssw"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ss"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "E"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ss-ZA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "South Africa"
    $cib.RegionNativeName = "South Africa"
    $cib.ThreeLetterISORegionName = "ZAF"
    $cib.TwoLetterISORegionName = "ZA"
    $cib.ThreeLetterWindowsRegionName = "ZAF"
    $cib.CurrencyEnglishName = "South African Rand"
    $cib.CurrencyNativeName = "South African Rand"
    $cib.CultureEnglishName = "siSwati (South Africa)"
    $cib.CultureNativeName = "siSwati (South Africa)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ssw"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ss"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "R"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ssy-ER"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Eritrea"
    $cib.RegionNativeName = "Eretria"
    $cib.ThreeLetterISORegionName = "ERI"
    $cib.TwoLetterISORegionName = "ER"
    $cib.ThreeLetterWindowsRegionName = "ERI"
    $cib.CurrencyEnglishName = "Eritrean Nakfa"
    $cib.CurrencyNativeName = "Eritrean Nakfa"
    $cib.CultureEnglishName = "Saho (Eritrea)"
    $cib.CultureNativeName = "Saho (Eretria)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ssy"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ssy"

    $cib.GregorianDateTimeFormat.AMDesignator = "saaku"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, MMMM dd, yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, MMMM dd, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Nfk"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "st-LS"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Lesotho"
    $cib.RegionNativeName = "Lesotho"
    $cib.ThreeLetterISORegionName = "LSO"
    $cib.TwoLetterISORegionName = "LS"
    $cib.ThreeLetterWindowsRegionName = "LSO"
    $cib.CurrencyEnglishName = "South African Rand"
    $cib.CurrencyNativeName = "South African Rand"
    $cib.CultureEnglishName = "Sesotho (Lesotho)"
    $cib.CultureNativeName = "Sesotho (Lesotho)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "sot"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "st"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM d, dddd HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM d, dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "R"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sv-AX"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Åland Islands"
    $cib.RegionNativeName = "Åland"
    $cib.ThreeLetterISORegionName = "ALA"
    $cib.TwoLetterISORegionName = "AX"
    $cib.ThreeLetterWindowsRegionName = "ALA"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "euro"
    $cib.CultureEnglishName = "Swedish (Åland Islands)"
    $cib.CultureNativeName = "svenska (Åland)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "swe"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sv"

    $cib.GregorianDateTimeFormat.AMDesignator = "fm"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sw-CD"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Congo (DRC)"
    $cib.RegionNativeName = "Jamhuri ya Kidemokrasia ya Kongo"
    $cib.ThreeLetterISORegionName = "COD"
    $cib.TwoLetterISORegionName = "CD"
    $cib.ThreeLetterWindowsRegionName = "COD"
    $cib.CurrencyEnglishName = "Congolese Franc"
    $cib.CurrencyNativeName = "Faranga ya Kongo"
    $cib.CultureEnglishName = "Kiswahili (Congo DRC)"
    $cib.CultureNativeName = "Kiswahili (Jamhuri ya Kidemokrasia ya Kongo)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "swa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sw"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "FC"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sw-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Tanzania"
    $cib.CultureEnglishName = "Kiswahili (Tanzania)"
    $cib.CultureNativeName = "Kiswahili (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "swa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sw"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "sw-UG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Uganda"
    $cib.RegionNativeName = "Uganda"
    $cib.ThreeLetterISORegionName = "UGA"
    $cib.TwoLetterISORegionName = "UG"
    $cib.ThreeLetterWindowsRegionName = "UGA"
    $cib.CurrencyEnglishName = "Ugandan Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Uganda"
    $cib.CultureEnglishName = "Kiswahili (Uganda)"
    $cib.CultureNativeName = "Kiswahili (Uganda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "swa"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "sw"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "USh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ta-MY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Malaysia"
    $cib.RegionNativeName = "மலேசியா"
    $cib.ThreeLetterISORegionName = "MYS"
    $cib.TwoLetterISORegionName = "MY"
    $cib.ThreeLetterWindowsRegionName = "MYS"
    $cib.CurrencyEnglishName = "Malaysian Ringgit"
    $cib.CurrencyNativeName = "மலேஷியன் ரிங்கிட்"
    $cib.CultureEnglishName = "Tamil (Malaysia)"
    $cib.CultureNativeName = "தமிழ் (மலேசியா)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "tam"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ta"

    $cib.GregorianDateTimeFormat.AMDesignator = "முற்பகல்"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM, yyyy tt h:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tt h:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tt h:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "RM"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "ta-SG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Singapore"
    $cib.RegionNativeName = "சிங்கப்பூர்"
    $cib.ThreeLetterISORegionName = "SGP"
    $cib.TwoLetterISORegionName = "SG"
    $cib.ThreeLetterWindowsRegionName = "SGP"
    $cib.CurrencyEnglishName = "Singapore Dollar"
    $cib.CurrencyNativeName = "சிங்கப்பூர் டாலர்"
    $cib.CultureEnglishName = "Tamil (Singapore)"
    $cib.CultureNativeName = "தமிழ் (சிங்கப்பூர்)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "tam"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "ta"

    $cib.GregorianDateTimeFormat.AMDesignator = "முற்பகல்"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM, yyyy tt h:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM, yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tt h:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tt h:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "teo-KE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Kenya"
    $cib.RegionNativeName = "Kenia"
    $cib.ThreeLetterISORegionName = "KEN"
    $cib.TwoLetterISORegionName = "KE"
    $cib.ThreeLetterWindowsRegionName = "KEN"
    $cib.CurrencyEnglishName = "Kenyan Shilling"
    $cib.CurrencyNativeName = "Ango’otol lok’ Kenya"
    $cib.CultureEnglishName = "Teso (Kenya)"
    $cib.CultureNativeName = "Kiteso (Kenia)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "teo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "teo"

    $cib.GregorianDateTimeFormat.AMDesignator = "Taparachu"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Ksh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "teo-UG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Uganda"
    $cib.RegionNativeName = "Uganda"
    $cib.ThreeLetterISORegionName = "UGA"
    $cib.TwoLetterISORegionName = "UG"
    $cib.ThreeLetterWindowsRegionName = "UGA"
    $cib.CurrencyEnglishName = "Ugandan Shilling"
    $cib.CurrencyNativeName = "Ango’otol lok’ Uganda"
    $cib.CultureEnglishName = "Teso (Uganda)"
    $cib.CultureNativeName = "Kiteso (Uganda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "teo"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "teo"

    $cib.GregorianDateTimeFormat.AMDesignator = "Taparachu"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "USh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "tig-ER"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Eritrea"
    $cib.RegionNativeName = "ኤርትራ"
    $cib.ThreeLetterISORegionName = "ERI"
    $cib.TwoLetterISORegionName = "ER"
    $cib.ThreeLetterWindowsRegionName = "ERI"
    $cib.CurrencyEnglishName = "Eritrean Nakfa"
    $cib.CurrencyNativeName = "Eritrean Nakfa"
    $cib.CultureEnglishName = "Tigre (Eritrea)"
    $cib.CultureNativeName = "ትግረ (ኤርትራ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "tig"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "tig"

    $cib.GregorianDateTimeFormat.AMDesignator = "ቀደም ሰርምዕል"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd፡ dd MMMM ዮም yyyy gg h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd፡ dd MMMM ዮም yyyy gg"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Nfk"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "to"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tonga"
    $cib.RegionNativeName = "Tonga"
    $cib.ThreeLetterISORegionName = "TON"
    $cib.TwoLetterISORegionName = "TO"
    $cib.ThreeLetterWindowsRegionName = "TON"
    $cib.CurrencyEnglishName = "Tongan Paʻanga"
    $cib.CurrencyNativeName = "Tongan Paʻanga"
    $cib.CultureEnglishName = "Tongan"
    $cib.CultureNativeName = "lea fakatonga"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ton"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "to"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "T$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "to-TO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tonga"
    $cib.RegionNativeName = "Tonga"
    $cib.ThreeLetterISORegionName = "TON"
    $cib.TwoLetterISORegionName = "TO"
    $cib.ThreeLetterWindowsRegionName = "TON"
    $cib.CurrencyEnglishName = "Tongan Paʻanga"
    $cib.CurrencyNativeName = "Paʻanga fakatonga"
    $cib.CultureEnglishName = "Tongan (Tonga)"
    $cib.CultureNativeName = "lea fakatonga (Tonga)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "ton"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "to"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "T$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "tr-CY"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cyprus"
    $cib.RegionNativeName = "Kıbrıs"
    $cib.ThreeLetterISORegionName = "CYP"
    $cib.TwoLetterISORegionName = "CY"
    $cib.ThreeLetterWindowsRegionName = "CYP"
    $cib.CurrencyEnglishName = "Euro"
    $cib.CurrencyNativeName = "Euro"
    $cib.CultureEnglishName = "Turkish (Cyprus)"
    $cib.CultureNativeName = "Türkçe (Kıbrıs)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "tur"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "tr"

    $cib.GregorianDateTimeFormat.AMDesignator = "ÖÖ"
    $cib.GregorianDateTimeFormat.DateSeparator = "."
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "d MMMM yyyy dddd h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "d MMMM yyyy dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d.MM.yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "€"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 2
    $cib.NumberFormat.PercentPositivePattern = 2
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "twq-NE"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Niger"
    $cib.RegionNativeName = "Nižer"
    $cib.ThreeLetterISORegionName = "NER"
    $cib.TwoLetterISORegionName = "NE"
    $cib.ThreeLetterWindowsRegionName = "NER"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "CFA Fraŋ (BCEAO)"
    $cib.CultureEnglishName = "Tasawaq (Niger)"
    $cib.CultureNativeName = "Tasawaq senni (Nižer)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "twq"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "twq"

    $cib.GregorianDateTimeFormat.AMDesignator = "Subbaahi"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "tzm-Latn-MA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Morocco"
    $cib.RegionNativeName = "Meṛṛuk"
    $cib.ThreeLetterISORegionName = "MAR"
    $cib.TwoLetterISORegionName = "MA"
    $cib.ThreeLetterWindowsRegionName = "MAR"
    $cib.CurrencyEnglishName = "Moroccan Dirham"
    $cib.CurrencyNativeName = "Derhem Umeṛṛuki"
    $cib.CultureEnglishName = "Central Atlas Tamazight (Latin, Morocco)"
    $cib.CultureNativeName = "Tamaziɣt n laṭlaṣ (Meṛṛuk)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "tzm"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "tzm"

    $cib.GregorianDateTimeFormat.AMDesignator = "Zdat azal"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "MAD"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "uz-Arab-AF"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Afghanistan"
    $cib.RegionNativeName = "افغانستان"
    $cib.ThreeLetterISORegionName = "AFG"
    $cib.TwoLetterISORegionName = "AF"
    $cib.ThreeLetterWindowsRegionName = "AFG"
    $cib.CurrencyEnglishName = "Afghan Afghani"
    $cib.CurrencyNativeName = "افغانی"
    $cib.CultureEnglishName = "Uzbek (Perso-Arabic, Afghanistan)"
    $cib.CultureNativeName = "اوزبیک (افغانستان)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "uzb"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "uz"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy H:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "H:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "d-MMMM"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "H:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "."
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "؋"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "."
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "."
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "vai-Latn-LR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Liberia"
    $cib.RegionNativeName = "Laibhiya"
    $cib.ThreeLetterISORegionName = "LBR"
    $cib.TwoLetterISORegionName = "LR"
    $cib.ThreeLetterWindowsRegionName = "LBR"
    $cib.CurrencyEnglishName = "Liberian Dollar"
    $cib.CurrencyNativeName = "Laibhiya Dala"
    $cib.CultureEnglishName = "Vai (Latin, Liberia)"
    $cib.CultureNativeName = "Vai (Laibhiya)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "vai"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "vai"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "vai-Vaii-LR"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Liberia"
    $cib.RegionNativeName = "ꕞꔤꔫꕩ"
    $cib.ThreeLetterISORegionName = "LBR"
    $cib.TwoLetterISORegionName = "LR"
    $cib.ThreeLetterWindowsRegionName = "LBR"
    $cib.CurrencyEnglishName = "Liberian Dollar"
    $cib.CurrencyNativeName = "ꕞꔤꔫꕩ ꕜꕞꕌ"
    $cib.CultureEnglishName = "Vai (Vai, Liberia)"
    $cib.CultureNativeName = "ꕙꔤ (ꕞꔤꔫꕩ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "vai"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "vai"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "vo-001"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "World"
    $cib.RegionNativeName = "World"
    $cib.ThreeLetterISORegionName = "001"
    $cib.TwoLetterISORegionName = "001"
    $cib.ThreeLetterWindowsRegionName = "001"
    $cib.CurrencyEnglishName = "Special Drawing Rights"
    $cib.CurrencyNativeName = "Special Drawing Rights"
    $cib.CultureEnglishName = "Volapük (World)"
    $cib.CultureNativeName = "Volapük (World)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "vol"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "vo"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy MMMM'a' 'd'. d'id' HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy MMMM'a' 'd'. d'id'"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "XDR"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "vun-TZ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Tanzania"
    $cib.RegionNativeName = "Tanzania"
    $cib.ThreeLetterISORegionName = "TZA"
    $cib.TwoLetterISORegionName = "TZ"
    $cib.ThreeLetterWindowsRegionName = "TZA"
    $cib.CurrencyEnglishName = "Tanzanian Shilling"
    $cib.CurrencyNativeName = "Shilingi ya Tanzania"
    $cib.CultureEnglishName = "Vunjo (Tanzania)"
    $cib.CultureNativeName = "Kyivunjo (Tanzania)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "vun"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "vun"

    $cib.GregorianDateTimeFormat.AMDesignator = "utuko"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "TSh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "wae-CH"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Switzerland"
    $cib.RegionNativeName = "Schwiz"
    $cib.ThreeLetterISORegionName = "CHE"
    $cib.TwoLetterISORegionName = "CH"
    $cib.ThreeLetterWindowsRegionName = "CHE"
    $cib.CurrencyEnglishName = "Swiss Franc"
    $cib.CurrencyNativeName = "Swiss Franc"
    $cib.CultureEnglishName = "Walser (Switzerland)"
    $cib.CultureNativeName = "Walser (Schwiz)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "wae"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "wae"

    $cib.GregorianDateTimeFormat.AMDesignator = "AM"
    $cib.GregorianDateTimeFormat.DateSeparator = "-"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d. MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d. MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = "’"
    $cib.NumberFormat.CurrencyNegativePattern = 9
    $cib.NumberFormat.CurrencyPositivePattern = 2
    $cib.NumberFormat.CurrencySymbol = "CHF"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = "’"
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = "’"
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "wal-ET"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Ethiopia"
    $cib.RegionNativeName = "ኢትዮጵያ"
    $cib.ThreeLetterISORegionName = "ETH"
    $cib.TwoLetterISORegionName = "ET"
    $cib.ThreeLetterWindowsRegionName = "ETH"
    $cib.CurrencyEnglishName = "Ethiopian Birr"
    $cib.CurrencyNativeName = "የኢትዮጵያ ብር"
    $cib.CultureEnglishName = "Wolaytta (Ethiopia)"
    $cib.CultureNativeName = "ወላይታቱ (ኢትዮጵያ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "wal"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "wal"

    $cib.GregorianDateTimeFormat.AMDesignator = "ማለዶ"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd፥ dd MMMM ጋላሳ yyyy gg h:mm:ss tt"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd፥ dd MMMM ጋላሳ yyyy gg"
    $cib.GregorianDateTimeFormat.LongTimePattern = "h:mm:ss tt"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "h:mm tt"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = "’"
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "Br"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = "’"
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = "’"
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "xog-UG"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Uganda"
    $cib.RegionNativeName = "Yuganda"
    $cib.ThreeLetterISORegionName = "UGA"
    $cib.TwoLetterISORegionName = "UG"
    $cib.ThreeLetterWindowsRegionName = "UGA"
    $cib.CurrencyEnglishName = "Ugandan Shilling"
    $cib.CurrencyNativeName = "Silingi eya Yuganda"
    $cib.CultureEnglishName = "Soga (Uganda)"
    $cib.CultureNativeName = "Olusoga (Yuganda)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "xog"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "xog"

    $cib.GregorianDateTimeFormat.AMDesignator = "Munkyo"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "USh"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "yav-CM"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Cameroon"
    $cib.RegionNativeName = "Kemelún"
    $cib.ThreeLetterISORegionName = "CMR"
    $cib.TwoLetterISORegionName = "CM"
    $cib.ThreeLetterWindowsRegionName = "CMR"
    $cib.CurrencyEnglishName = "Central African CFA Franc"
    $cib.CurrencyNativeName = "Central African CFA Franc"
    $cib.CultureEnglishName = "Yangben (Cameroon)"
    $cib.CultureNativeName = "nuasue (Kemelún)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "yav"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "yav"

    $cib.GregorianDateTimeFormat.AMDesignator = "kiɛmɛ́ɛm"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 8
    $cib.NumberFormat.CurrencyPositivePattern = 3
    $cib.NumberFormat.CurrencySymbol = "FCFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "yo-BJ"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Benin"
    $cib.RegionNativeName = "Orílɛ́ède Bɛ̀nɛ̀"
    $cib.ThreeLetterISORegionName = "BEN"
    $cib.TwoLetterISORegionName = "BJ"
    $cib.ThreeLetterWindowsRegionName = "BEN"
    $cib.CurrencyEnglishName = "West African CFA Franc"
    $cib.CurrencyNativeName = "Faransi ti Orílɛ́ède BIKEAO"
    $cib.CultureEnglishName = "Yoruba (Benin)"
    $cib.CultureNativeName = "Èdè Yorùbá (Orílɛ́ède Bɛ̀nɛ̀)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "yor"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "yo"

    $cib.GregorianDateTimeFormat.AMDesignator = "Àárɔ̀"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd, d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd, d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "MMMM yyyy"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "CFA"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "zgh-Tfng-MA"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Morocco"
    $cib.RegionNativeName = "ⵍⵎⵖⵔⵉⴱ"
    $cib.ThreeLetterISORegionName = "MAR"
    $cib.TwoLetterISORegionName = "MA"
    $cib.ThreeLetterWindowsRegionName = "MAR"
    $cib.CurrencyEnglishName = "Moroccan Dirham"
    $cib.CurrencyNativeName = "ⴰⴷⵔⵉⵎ ⵏ ⵍⵎⵖⵔⵉⴱ"
    $cib.CultureEnglishName = "Standard Moroccan Tamazight (Tifinagh, Morocco)"
    $cib.CultureNativeName = "ⵜⴰⵎⴰⵣⵉⵖⵜ (ⵍⵎⵖⵔⵉⴱ)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "zgh"
    $cib.ThreeLetterWindowsLanguageName = "ZHG"
    $cib.TwoLetterISOLanguageName = "zgh"

    $cib.GregorianDateTimeFormat.AMDesignator = "ⵜⵉⴼⴰⵡⵜ"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "dddd d MMMM yyyy HH:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "dddd d MMMM yyyy"
    $cib.GregorianDateTimeFormat.LongTimePattern = "HH:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "MMMM d"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "HH:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy MMMM"

    $cib.NumberFormat.CurrencyDecimalSeparator = ","
    $cib.NumberFormat.CurrencyGroupSeparator = " "
    $cib.NumberFormat.CurrencyNegativePattern = 5
    $cib.NumberFormat.CurrencyPositivePattern = 1
    $cib.NumberFormat.CurrencySymbol = "MAD"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = ","
    $cib.NumberFormat.NumberGroupSeparator = " "
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = ","
    $cib.NumberFormat.PercentGroupSeparator = " "
    $cib.NumberFormat.PercentNegativePattern = 0
    $cib.NumberFormat.PercentPositivePattern = 0
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "zh-Hans-HK"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Hong Kong SAR"
    $cib.RegionNativeName = "香港特别行政区"
    $cib.ThreeLetterISORegionName = "HKG"
    $cib.TwoLetterISORegionName = "HK"
    $cib.ThreeLetterWindowsRegionName = "HKG"
    $cib.CurrencyEnglishName = "Hong Kong Dollar"
    $cib.CurrencyNativeName = "港元"
    $cib.CultureEnglishName = "Chinese (Simplified Han, Hong Kong SAR)"
    $cib.CultureNativeName = "中文 (香港特别行政区)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "zho"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "zh"

    $cib.GregorianDateTimeFormat.AMDesignator = "上午"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy年M月d日dddd tth:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy年M月d日dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tth:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "M月d日"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tth:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy年M月"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }

try
{    
    $newCulture = "zh-Hans-MO"
    $cib = New-Object "System.Globalization.CultureAndRegionInfoBuilder" -Args $newCulture, None
    $ci = New-Object "System.Globalization.CultureInfo" -Args ""
    $cib.LoadDataFromCultureInfo($ci)
    $ri = New-Object "System.Globalization.RegionInfo" -Args "en-US"
    $cib.LoadDataFromRegionInfo($ri)

    $cib.RegionEnglishName = "Macao SAR"
    $cib.RegionNativeName = "澳门特别行政区"
    $cib.ThreeLetterISORegionName = "MAC"
    $cib.TwoLetterISORegionName = "MO"
    $cib.ThreeLetterWindowsRegionName = "MAC"
    $cib.CurrencyEnglishName = "Macanese Pataca"
    $cib.CurrencyNativeName = "澳门币"
    $cib.CultureEnglishName = "Chinese (Simplified Han, Macao SAR)"
    $cib.CultureNativeName = "中文 (澳门特别行政区)"
    $cib.IetfLanguageTag = $newCulture
    $cib.ThreeLetterISOLanguageName = "zho"
    $cib.ThreeLetterWindowsLanguageName = "ZZZ"
    $cib.TwoLetterISOLanguageName = "zh"

    $cib.GregorianDateTimeFormat.AMDesignator = "上午"
    $cib.GregorianDateTimeFormat.DateSeparator = "/"
    $cib.GregorianDateTimeFormat.FullDateTimePattern = "yyyy年M月d日dddd tth:mm:ss"
    $cib.GregorianDateTimeFormat.LongDatePattern = "yyyy年M月d日dddd"
    $cib.GregorianDateTimeFormat.LongTimePattern = "tth:mm:ss"
    $cib.GregorianDateTimeFormat.MonthDayPattern = "M月d日"
    $cib.GregorianDateTimeFormat.ShortDatePattern = "d/M/yyyy"
    $cib.GregorianDateTimeFormat.ShortTimePattern = "tth:mm"
    $cib.GregorianDateTimeFormat.TimeSeparator = ":"
    $cib.GregorianDateTimeFormat.YearMonthPattern = "yyyy年M月"

    $cib.NumberFormat.CurrencyDecimalSeparator = "."
    $cib.NumberFormat.CurrencyGroupSeparator = ","
    $cib.NumberFormat.CurrencyNegativePattern = 1
    $cib.NumberFormat.CurrencyPositivePattern = 0
    $cib.NumberFormat.CurrencySymbol = "MOP$"
    $cib.NumberFormat.NegativeInfinitySymbol = "-∞"
    $cib.NumberFormat.NegativeSign = "-"
    $cib.NumberFormat.NumberDecimalDigits = 2
    $cib.NumberFormat.NumberDecimalSeparator = "."
    $cib.NumberFormat.NumberGroupSeparator = ","
    $cib.NumberFormat.NumberNegativePattern = 1
    $cib.NumberFormat.PercentDecimalDigits = 2
    $cib.NumberFormat.PercentDecimalSeparator = "."
    $cib.NumberFormat.PercentGroupSeparator = ","
    $cib.NumberFormat.PercentNegativePattern = 1
    $cib.NumberFormat.PercentPositivePattern = 1
    $cib.NumberFormat.PercentSymbol = "%"
    $cib.NumberFormat.PerMilleSymbol = "‰"
    $cib.NumberFormat.PositiveInfinitySymbol = "∞"
    $cib.NumberFormat.PositiveSign = "+"

    $cib.Register();
}
catch { }
