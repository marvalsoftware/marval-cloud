﻿param
(
    [String]$msmVersion="0.0.0.0",
    [String]$auditVersion="0.0.0.0",
    [String]$licencePath='C:\Config\licence.mlic',
    [String]$connectionPath='C:\Config\connectionStrings.config',
    [String]$selfServiceUrl = 'http://localhost/MSMSelfService',
    [String]$signalRUrl = 'http://localhost/MSM/signalr',
    [String]$msmBaseUrl = 'http://localhost/MSM'
)

$ErrorActionPreference = 'Stop'

function Split-ConnectionString([string]$connectionString) {
    $parts = $connectionString.Split(';')
    $server = $parts[0].Split('=')[1]
    $port = 1433
    if ($server.split(',').Count -gt 1)
    {
        $port = $server.split(',')[1]
        $server = $server.split(',')[0]
    }
    $database = $parts[1].Split('=')[1]
    $user = $parts[2].Split('=')[1]
    $password = $parts[3].Split('=')[1]
    return @{ Server = $server; Port = $port; Name = $database; User = $user; Password = $password }
}

Import-Module SqlServer
[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null

$xml = [String](Get-Content -Path $connectionPath)
Select-Xml -Content $xml -XPath "//add" | ForEach-Object { 

        $nodeKey = [String]$_.node.key
        if ($nodeKey -Match "DatabaseConnectionString") {
            $databaseConnectionString = $_.node.value            
        }
        if ($nodeKey -Match "AuditConnectionString") {
            $auditConnectionString = $_.node.value
        }
        if ($nodeKey -Match "ElasticsearchUrl") {
            $elasticSearchUrl = $_.node.value
        }
        if ($nodeKey -Match "ElasticsearchIndex") {
            $easticsearchIndex = $_.node.value
        }
        if ($nodeKey -Match "RedisHost") {
            $redisHost = $_.node.value
        }
        if ($nodeKey -Match "RedisPort") {
            $redisPort = $_.node.value
        }
        if ($nodeKey -Match "RedisPassword") {
            $redisPassword = $_.node.value
        }
        if ($nodeKey -Match "RedisAdditionalParameters") {
            $redisAdditionalParams = $_.node.value
        }
}

$databaseConnectionObject = Split-ConnectionString -connectionString $databaseConnectionString
$auditConnectionObject = Split-ConnectionString -connectionString $auditConnectionString

try 
{
    Add-Type -Path "C:\Program Files\Marval Software\MSM\CLI\MarvalSoftware.Sql.dll";
    $sqlCon = New-Object Microsoft.Data.SqlClient.SqlConnection($databaseConnectionString)
    $serverConn = New-Object Microsoft.SqlServer.Management.Common.ServerConnection($sqlCon)    
    $msmServer = New-Object Microsoft.SqlServer.Management.SMO.Server($serverConn)
    $msmDatabase = New-Object Microsoft.SqlServer.Management.SMO.Database($msmServer, $databaseConnectionObject.Name)
    $requiresUpgrade = $False
    $results = $msmDatabase.ExecuteWithResults("SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'databaseVersion'")
    $hasDatabaseVersionTable = $results.Tables[0].Rows.Count -gt 0;

    if ($hasDatabaseVersionTable)
    {
        $results = $msmDatabase.ExecuteWithResults("SELECT CONCAT(ISNULL(major, '0'), '.', ISNULL(minor, '0'), '.',  ISNULL(build, '0'), '.',  ISNULL(revision, '0')) AS version FROM databaseVersion")
        $msmDatabaseVersionMatch = $results.Tables[0].Rows[0].version -eq $msmVersion
        $requiresUpgrade = !$msmDatabaseVersionMatch
    }
    else
    {
        $requiresUpgrade = !$hasDatabaseVersionTable
    }

    if ($requiresUpgrade)
    {
        "Upgrading the MSM database..."
        $dbUpperConfiguration = [MarvalSoftware.Sql.DbUpperConfiguration]::CreateUpgrade($databaseConnectionString, [MarvalSoftware.Sql.TargetDatabase]::Msm).AndLogToConsole($true);
        $installationParameters = New-Object "System.Collections.Generic.Dictionary[string,string]";
        $installationParameters.Add("INITIAL_USER_FIRST_NAME", "System");
        $installationParameters.Add("INITIAL_USER_LAST_NAME", "Administrator");
        $installationParameters.Add("INITIAL_USER_USERNAME", "Administrator");
        $installationParameters.Add("SELF_SERVICE_URL", $selfServiceUrl);
        $installationParameters.Add("SIGNALR_URL", $signalRUrl);
        $installationParameters.Add("MSM_BASE_URL", $msmBaseUrl);
        $installationParameters.Add("XTRACTION_URL", $null);
        $database = New-Object MarvalSoftware.Sql.Database($dbUpperConfiguration);
        $database.Execute($installationParameters);    
        $msmDatabase.ExecuteNonQuery("UPDATE databaseVersion SET [major] = " + $msmVersion.Split('.')[0] + ", [minor] = " + $msmVersion.Split('.')[1] + ", [build] = " + $msmVersion.Split('.')[2] + ", [revision] = " + $msmVersion.Split('.')[3] + ", [runOnce] = 1");    
        $msmDatabase.ExecuteNonQuery("DELETE FROM file_ where type = 7")
    }

    $asqlCon = New-Object Microsoft.Data.SqlClient.SqlConnection($auditConnectionString)
    $aserverConn = New-Object Microsoft.SqlServer.Management.Common.ServerConnection($asqlCon)    
    $auditServer = New-Object Microsoft.SqlServer.Management.SMO.Server($aserverConn)
    $auditDatabase = New-Object Microsoft.SqlServer.Management.SMO.Database($auditServer, $auditConnectionObject.Name)
    $results = $auditDatabase.ExecuteWithResults("SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'databaseVersion'")
    $hasDatabaseVersionTable = $results.Tables[0].Rows.Count -gt 0;

    if ($hasDatabaseVersionTable)
    {
        $results = $auditDatabase.ExecuteWithResults("SELECT CONCAT(major, '.', minor, '.',  build, '.',  revision) AS version FROM databaseVersion")
        $auditDatabaseVersionMatch = $results.Tables[0].Rows[0].version -eq $auditVersion    
        $requiresUpgrade = !$auditDatabaseVersionMatch
    }
    else
    {
        $requiresUpgrade = !$hasDatabaseVersionTable
    }

    if ($requiresUpgrade) 
    {
        "Upgrading the Audit database"
        $dbUpperConfiguration = [MarvalSoftware.Sql.DbUpperConfiguration]::CreateUpgrade($auditConnectionString, [MarvalSoftware.Sql.TargetDatabase]::Audit).AndLogToConsole($true);
        $database = New-Object MarvalSoftware.Sql.Database($dbUpperConfiguration);
        $database.Execute();
        $auditDatabase.ExecuteNonQuery("UPDATE databaseVersion SET [major] = " + $auditVersion.Split('.')[0] + ", [minor] = " + $auditVersion.Split('.')[1] + ", [build] = " + $auditVersion.Split('.')[2] + ", [revision] = " + $auditVersion.Split('.')[3]);    
    }

    $licenceData = Get-Content -Path $licencePath
    $results = $msmDatabase.ExecuteWithResults("SELECT licence FROM systemConfiguration")
    $licenceMatch = $results.Tables[0].Rows[0].licence -eq $licenceData
    if (!$licenceMatch) {
        "Installing license..."       
        & 'C:\Program Files\Marval Software\MSM\CLI\msm-configure-cli.exe' license set --file=$licencePath --dbConnectionString=$databaseConnectionString
        if (!$?)
        {
            throw "Elastic search error."
        }
    }
}
catch {
    $_.Exception | Format-List -Force
    throw $_.Exception
}  

# Key is expected as an enviroment variable for security purposes
"Upgrading encryptions"
& 'C:\Program Files\Marval Software\MSM\CLI\msm-configure-cli.exe' encryption upgrade --dbConnectionString=$databaseConnectionString

if (!$?)
{
    throw "Encryption upgrade error."
}

"Upgrading Elastic Search..."
& 'C:\Program Files\Marval Software\MSM\CLI\msm-configure-cli.exe' elasticsearch upgrade --url=$elasticSearchUrl --index=$easticsearchIndex

if (!$?)
{
    throw "Elastic search error."
}

"Testing redis configuration"
& 'C:\Program Files\Marval Software\MSM\CLI\msm-configure-cli.exe' redis test --rhost=$redisHost --rport=$redisPort --password=$redisPassword --additional-options=$redisAdditionalParams

if (!$?)
{
    throw "Redis error."
}
