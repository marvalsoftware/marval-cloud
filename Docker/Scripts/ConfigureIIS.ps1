﻿param([string]$path="");

$appPoolName = "DefaultAppPool";

[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]"Ssl3,Tls,Tls11,Tls12"
Install-PackageProvider -Name NuGet -Force
Install-Module -Name Invoke-MsBuild -Force
Import-Module WebAdministration;

$appPoolPath = "IIS:\AppPools\${appPoolName}";
if (Test-Path $appPoolPath) {
    "Stopping app pool.";
    if ((Get-WebAppPoolState -Name $appPoolName).Value -ne "Stopped") {
        Stop-WebAppPool -Name $appPoolName;
        while ((Get-WebAppPoolState -Name $appPoolName).Value -ne "Stopped") {
            Write-Warning "Waiting for app pool '$appPoolName' to stop.";
            Start-Sleep -MilliSeconds 200;
        }
    }
}

# configure app pool
"Configuring app pool.";
$appPool = if (!(Test-Path $appPoolPath)) { New-Item $appPoolPath; } else { Get-Item $appPoolPath; };
$appPool.ManagedRuntimeVersion = "v4.0";
$appPool.ProcessModel.IdentityType = "NetworkService";
$appPool | Set-Item;

# configure web application
"Configuring web application.";
$webApplications = @{
    MSM = "Service Desk";   
    MSMSelfService = "Self Service";
    MSMSelfServiceWebService = "Self Service Web Service";
    MSMWebService = "Web Service";
    MSMAuthenticationAzure = "Custom Authentication Provider\Azure";
    MSMAuthenticationSAML2 = "Custom Authentication Provider\SAML2";
    MSMAuthenticationOpenId = "Custom Authentication Provider\OpenID";
	MSMAuthenticationLDAP = "Custom Authentication Provider\LDAP";
    MSMAuthenticationADFS = "Custom Authentication Provider\ADFS";
};
foreach ($webApplication in $webApplications.GetEnumerator()) {    
    $applicationPath = "IIS:\Sites\Default Web Site\$($webApplication.Key)";
    $physicalPath = "$($path)\$($webApplication.Value)";
    if (Test-Path $physicalPath) {        
        if (Test-Path $applicationPath) {
            Remove-Item $applicationPath | Out-Null;
        }
        New-Item $applicationPath -Type Application -ApplicationPool $appPool.Name -PhysicalPath $physicalPath | Out-Null;
        "Installing IIS rewrite modue"
        Invoke-WebRequest https://download.microsoft.com/download/1/2/8/128E2E22-C1B9-44A4-BE2A-5859ED1D4592/rewrite_amd64_en-US.msi -OutFile rewrite_amd64_en-US.msi;
        Start-Process msiexec -ArgumentList "/i `"rewrite_amd64_en-US.msi`" /quiet" -Wait;

        if ($webApplication.key -eq "MSM")
        {                        
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site\MSM" -Filter "system.web/httpCookies" -Name "requireSSL" -Value "false";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site\MSM" -Filter "system.web/authentication/forms" -Name "requireSSL" -Value "false";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site\MSM" -Filter "system.web/compilation" -Name "debug" -Value "false";
            Add-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules" -Name "." -Value @{ name = "Redirect root to MSM"; stopProcessing = "True" };
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSM']/match" -Name "url" -Value "^$";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSM']/action" -Name "type" -Value "Redirect";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSM']/action" -Name "url" -Value "{URL}MSM";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSM']/action" -Name "redirectType" -Value "SeeOther";
        }
        if ($webApplication.key -eq "MSMSelfService")
        {
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site\MSMSelfService" -Filter "system.web/httpCookies" -Name "requireSSL" -Value "false";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site\MSMSelfService" -Filter "system.web/compilation" -Name "debug" -Value "false";
            Add-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules" -Name "." -Value @{ name = "Redirect root to MSMSelfService"; stopProcessing = "True" };
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/match" -Name "url" -Value "^$";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/action" -Name "type" -Value "Redirect";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/action" -Name "url" -Value "{URL}MSMSelfService";
            Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/action" -Name "redirectType" -Value "SeeOther";
        }
    }
}

# start app pool
"Starting app pool.";
Start-WebAppPool -Name $appPoolName;
while ((Get-WebAppPoolState -Name $appPoolName).Value -ne "Started") {
    Write-Warning "Waiting for app pool '$appPoolName' to start.";
    Start-Sleep -MilliSeconds 200;
}

"Setting folder permissions"
$sid = [Security.Principal.SecurityIdentifier]'S-1-5-20'
$acct = $sid.Translate([Security.Principal.NTAccount]).Value
$ace = New-Object Security.AccessControl.FileSystemAccessRule($acct, 'FullControl', 'ContainerInherit,ObjectInherit', 'None', 'Allow')
$Acl = (Get-Item $path).GetAccessControl('Access')
$Acl.SetAccessRule($ace)
Set-Acl $path $Acl