﻿param
(
        [Parameter(Mandatory = $true)][string] $serverUrl = "",
        [Parameter(Mandatory = $true)][string] $buildVersion = "",
        [Parameter(Mandatory = $true)][string] $acrUser = "",
        [Parameter(Mandatory = $true)][string] $acrPass = ""
)

try {
        $curDis = $pwd
        "Copy Prerequisites and Source Files"
        Copy-Item "..\MSM\Source\Setup\Requisites\TimeZones.reg" -Destination "Scripts" -Force
        Copy-Item "..\MSM\Source\Setup\Requisites\InstallCustomCultures.ps1" -Destination "Scripts" -Force

        "Copy all appropriate source release folders"
        if (Test-Path "Source\") {
                Remove-Item "Source\" -Recurse -Force
        }
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.Utilities.Configure.CLI\bin\Release\" -Destination "Source\ConfigureCLI\ConfigureCLI\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.BackgroundService\bin\Release\" -Destination "Source\BackgroundService\BackgroundService\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.Servers.DirectoryReplicate\bin\Release\" -Destination "Source\BackgroundService\BackgroundService\Directory Replicate\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.Servers.Discover\bin\Release\" -Destination "Source\BackgroundService\BackgroundService\Discover\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.Servers.SNMP\bin\Release\" -Destination "Source\BackgroundService\BackgroundService\SNMP\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebUI.ServiceDesk\Published\" -Destination "Source\MSM\MSM\Service Desk\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebService.ServiceDesk\Published\" -Destination "Source\MSM\MSM\Web Service\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebService.SelfService\Published\" -Destination "Source\SS\MSM\Self Service Web Service\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.SelfService.Published\" -Destination "Source\SS\MSM\Self Service\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebUI.Authentication.Azure\Published\" -Destination "Source\CAP\MSM\Custom Authentication Provider\Azure\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebUI.Authentication.SAML2\Published\" -Destination "Source\CAP\MSM\Custom Authentication Provider\SAML2\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebUI.Authentication.LDAP\Published\" -Destination "Source\CAP\MSM\Custom Authentication Provider\LDAP\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebUI.Authentication.OpenId\Published\" -Destination "Source\CAP\MSM\Custom Authentication Provider\OpenId\" -Recurse -Force
        Copy-Item -Path "..\MSM\Source\ServiceDesk\MarvalSoftware.UI.WebUI.Authentication.ADFS\Published\" -Destination "Source\CAP\MSM\Custom Authentication Provider\ADFS\" -Recurse -Force

        #"Docker login"
        # credentials stored securely on server with build agent
        #az login --service-principal -u $acrUser -p $acrPass --tenant $tenantId

        "Building Initizialize Image"
        Copy-Item "MSMInitDockerfile" -Destination ".\Scripts\" -Force
        Set-Location .\Scripts
        az acr build --platform windows -t initialize:$($buildVersion) -r $serverUrl -f .\MSMInitDockerfile . --timeout 7200
        Remove-Item "MSMInitDockerfile" -Force
        Set-Location $curDis

        "Build Configure Image"
        Copy-Item "MSMConfigureCLIDockerfile" -Destination ".\Source\ConfigureCLI\" -Force
        Copy-Item -Path ".\Scripts\" -Destination ".\Source\ConfigureCLI\Scripts\" -Recurse -Force
        Set-Location .\Source\ConfigureCLI
        az acr build --platform windows -t configure:$($buildVersion) -r $serverUrl -f .\MSMConfigureCLIDockerfile . --timeout 7200
        Set-Location $curDis

        "Build Background Service Container"
        Copy-Item -Path "MSMBackgroundServiceDockerfile" -Destination ".\Source\BackgroundService\" -Force
        Copy-Item -Path ".\Scripts\" -Destination ".\Source\BackgroundService\Scripts\" -Recurse -Force
        Set-Location .\Source\BackgroundService
        az acr build --platform windows -t backgroundservice:$($buildVersion) -r $serverUrl -f .\MSMBackgroundServiceDockerfile . --timeout 7200
        Set-Location $curDis

        "Build Service Desk Container"
        Copy-Item -Path "MSMDockerfile" -Destination ".\Source\MSM\" -Force
        Copy-Item -Path ".\Scripts\" -Destination ".\Source\MSM\Scripts\" -Recurse -Force
        Set-Location .\Source\MSM
        az acr build --platform windows -t servicedesk:$($buildVersion) -r $serverUrl -f .\MSMDockerfile . --timeout 7200
        Set-Location $curDis

        "Build Self Service Container"
        Copy-Item -Path "MSMSelfServiceDockerfile" -Destination ".\Source\SS\" -Force
        Copy-Item -Path ".\Scripts\" -Destination ".\Source\SS\Scripts\" -Recurse -Force
        Set-Location .\Source\SS
        az acr build --platform windows -t selfservice:$($buildVersion) -r $serverUrl -f .\MSMSelfServiceDockerfile . --timeout 7200
        Set-Location $curDis

        "Build CAP"
        Copy-Item -Path "MSMCAPDockerfile" -Destination ".\Source\CAP\" -Force
        Copy-Item -Path ".\Scripts\" -Destination ".\Source\CAP\Scripts\" -Recurse -Force
        Set-Location .\Source\CAP
        az acr build --platform windows -t msmcap:$($buildVersion) -r $serverUrl -f .\MSMCAPDockerfile . --timeout 7200
        Set-Location $curDis

        #"Logout"
        #az logout
}
catch {
        $_.Exception | Format-List -Force
        throw $_.Exception
}  
