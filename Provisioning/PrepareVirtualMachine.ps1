param($serverUri, $tenantName, $environmentName, $octopusDeployApiKey, $minioGatewayType, $minioRootUser, $minioRootPassword, $virtualMachineName, $domain)

$ErrorActionPreference = "Stop";

Start-Transcript C:\PrepareVirtualMachineTranscript.txt;

# functions
function installService($path, $definition) {
    if (!(Test-Path "WinSW.NET461.exe")) {
        Invoke-WebRequest https://marvaloctopusdeploy.s3.ap-southeast-2.amazonaws.com/WinSW.NET461.exe -OutFile WinSW.NET461.exe;
    }

    Copy-Item .\WinSW.NET461.exe "$path\WinSW.NET461.exe";
    Set-Content -Path "$path\WinSW.NET461.xml" -Value $definition;
    & "$path\WinSW.NET461.exe" install | Out-Null;
}

"setting up region and time zone...";
Set-WinSystemLocale en-GB;
Set-TimeZone "GMT Standard Time";

"installing .net 4.8...";
# Invoke-WebRequest https://go.microsoft.com/fwlink/?linkid=2088631 -OutFile ndp48-x86-x64-allos-enu.exe;
# Start-Process -FilePath ndp48-x86-x64-allos-enu.exe -ArgumentList "/q /norestart" -Wait 
# .\ndp48-x86-x64-allos-enu.exe /q | Out-Null;

"creating working directory...";
mkdir "C:\Marval Cloud" | Out-Null;
Set-Location "C:\Marval Cloud";


"installing and setting up octopus deploy tentacle...";
Invoke-WebRequest https://octopus.com/downloads/latest/WindowsX64/OctopusTentacle -OutFile Octopus.Tentacle.msi;
Start-Process msiexec -ArgumentList "/i `"C:\Marval Cloud\Octopus.Tentacle.msi`" /quiet" -Wait;
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" create-instance --instance "Tentacle" --config "C:\Octopus\Tentacle.config" | Out-Null;
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" new-certificate --instance "Tentacle" --if-blank | Out-Null;
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" configure --instance "Tentacle" --reset-trust | Out-Null;
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" configure --instance "Tentacle" --app "C:\Octopus\Applications" --port "10933" --noListen "True" | Out-Null;
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" polling-proxy --instance "Tentacle" --proxyEnable "False" --proxyUsername "" --proxyPassword "" --proxyHost "" --proxyPort "" | Out-Null;
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" register-with --instance "Tentacle" --server $serverUri --name $virtualMachineName --comms-style "TentacleActive" --server-comms-port "10943" --apiKey $octopusDeployApiKey --space "Default" --environment $environmentName --tenant $tenantName --role "MSM" --policy "Default Machine Policy" | Out-Null;
& "C:\Program Files\Octopus Deploy\Tentacle\Tentacle.exe" service --instance "Tentacle" --install --stop --start | Out-Null;

"setting up firewall rules...";
New-NetFirewallRule -DisplayName RDP -LocalPort 3389 -Protocol TCP | Out-Null;
New-NetFirewallRule -DisplayName HTTP -LocalPort 80 -Protocol TCP | Out-Null;
New-NetFirewallRule -DisplayName HTTPS -LocalPort 443 -Protocol TCP | Out-Null;

"installing and setting up internet information services...";
$iisFeatures = "Web-WebServer", "Web-Default-Doc", "Web-Http-Errors", "Web-Static-Content", "Web-Http-Logging", "Web-Stat-Compression", "Web-Dyn-Compression", "Web-Filtering", "Web-Asp-Net45", "Web-Mgmt-Console";
Install-WindowsFeature -Name $iisFeatures | Out-Null;
New-WebBinding -Name "Default Web Site" -IPAddress "*" -Port 80 -HostHeader $domain;
Invoke-WebRequest https://download.microsoft.com/download/1/2/8/128E2E22-C1B9-44A4-BE2A-5859ED1D4592/rewrite_amd64_en-US.msi -OutFile rewrite_amd64_en-US.msi;
Start-Process msiexec -ArgumentList "/i `"C:\Marval Cloud\rewrite_amd64_en-US.msi`" /quiet" -Wait;
Set-WebConfigurationProperty -Filter "system.applicationHost/applicationPools/applicationPoolDefaults" -Name "startMode" -Value "AlwaysRunning";
Set-WebConfigurationProperty -Filter "system.applicationHost/applicationPools/applicationPoolDefaults/processModel" -Name "idleTimeout" -Value "00:00:00";
Add-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules" -Name "." -Value @{ name = "Ignore .well-known"; stopProcessing = "True" };
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Ignore .well-known']/match" -Name "url" -Value "(^\.well-known\/?$|^\.well-known\/.*$)";
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Ignore .well-known']/action" -Name "type" -Value "None";
Add-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules" -Name "." -Value @{ name = "Redirect root to MSMSelfService"; stopProcessing = "True" };
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/match" -Name "url" -Value "^$";
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/action" -Name "type" -Value "Redirect";
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/action" -Name "url" -Value "{URL}MSMSelfService";
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='Redirect root to MSMSelfService']/action" -Name "redirectType" -Value "SeeOther";
Add-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules" -Name "." -Value @{ name = "HTTP to HTTPS redirect"; patternSyntax = "Wildcard"; stopProcessing = "True" };
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect']/match" -Name "url" -Value "*";
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect']/conditions" -Name "." -Value @{ input = "{HTTPS}";  pattern = "off" }, @{ input = "{HTTP_HOST}"; pattern = "localhost"; negate = "True" };
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect']/action" -Name "type" -Value "Redirect";
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect']/action" -Name "url" -Value "https://{HTTP_HOST}{REQUEST_URI}";
Set-WebConfigurationProperty -PSPath "IIS:\Sites\Default Web Site" -Filter "system.webServer/rewrite/rules/rule[@name='HTTP to HTTPS redirect']/action" -Name "redirectType" -Value "SeeOther";
$machineConfiguration = [System.Configuration.ConfigurationManager]::OpenMachineConfiguration();
$machineConfiguration.GetSection("system.web/deployment").Retail = $true;
$machineConfiguration.Save();

"setting up certificate with win-acme...";
Invoke-WebRequest https://github.com/win-acme/win-acme/releases/download/v2.1.15/win-acme.v2.1.15.1008.x64.trimmed.zip -OutFile win-acme.v2.1.15.1008.x64.trimmed.zip;
Expand-Archive win-acme.v2.1.15.1008.x64.trimmed.zip;
win-acme.v2.1.15.1008.x64.trimmed\wacs.exe --target iis --host $domain --accepttos --emailaddress devops@marval.co.uk --installation iis --setuptaskscheduler | Out-Null;

"setting up cryptography with iis crypto...";
Invoke-WebRequest https://www.nartac.com/Downloads/IISCrypto/IISCryptoCli.exe -OutFile IISCryptoCli.exe;
.\IISCryptoCli.exe /template pci32 | Out-Null;

"stopping the world wide web publishing service pending restart to indicate availability after completion...";
Stop-Service W3SVC;

if ($minioGatewayType) {
    "setting up minio...";
    mkdir minio | Out-Null;
    Invoke-WebRequest https://marvaloctopusdeploy.s3.ap-southeast-2.amazonaws.com/minio.exe -OutFile minio\minio.exe;
    installService "minio" ('<service>
    <id>minio</id>
    <name>MinIO</name>
    <description>This service provides a gateway to an Azure Storage account.</description>
    <executable>minio.exe</executable>
    <arguments>gateway {0}</arguments>
    <env name="MINIO_ROOT_USER" value="{1}" />
    <env name="MINIO_ROOT_PASSWORD" value="{2}" />
</service>' -f $minioGatewayType, $minioRootUser, $minioRootPassword);
}

"setting up clamav...";
mkdir clamav | Out-Null;
Invoke-WebRequest https://www.clamav.net/downloads/production/clamav-0.103.1-win-x64-portable.zip -UserAgent "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36" -OutFile clamav\clamav-0.103.1-win-x64-portable.zip;
Expand-Archive "clamav\clamav-0.103.1-win-x64-portable.zip" -DestinationPath "clamav\clamav-0.103.1-win-x64-portable";
Get-Content clamav\clamav-0.103.1-win-x64-portable\conf_examples\freshclam.conf.sample | Where-Object {$_ -notmatch "Example"} | Set-Content clamav\clamav-0.103.1-win-x64-portable\freshclam.conf;
Get-Content clamav\clamav-0.103.1-win-x64-portable\conf_examples\clamd.conf.sample | Where-Object {$_ -notmatch "Example"} | Set-Content clamav\clamav-0.103.1-win-x64-portable\clamd.conf;
clamav\clamav-0.103.1-win-x64-portable\freshclam.exe | Out-Null;
installService "clamav" '<service>
    <id>clamav</id>
    <name>ClamAV</name>
    <description>This service provides a server to scan files for viruses.</description>
    <executable>clamav-0.103.1-win-x64-portable\clamd.exe</executable>
</service>';
$freshclamScheduledTaskTrigger = New-ScheduledTaskTrigger -Daily -At 2am;
$freshclamScheduledTaskAction = New-ScheduledTaskAction "C:\Marval Cloud\clamav\clamav-0.103.1-win-x64-portable\freshclam.exe";
$freshclamScheduledTaskPrincipal = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest;
$freshclamScheduledTaskSettingsSet = New-ScheduledTaskSettingsSet;
Register-ScheduledTask freshclam -Principal $freshclamScheduledTaskPrincipal -Trigger $freshclamScheduledTaskTrigger -Action $freshclamScheduledTaskAction -Settings $freshclamScheduledTaskSettingsSet | Out-Null;

"starting the world wide web publishing service...";
Start-Sleep -Seconds 10
Start-Service W3SVC;
