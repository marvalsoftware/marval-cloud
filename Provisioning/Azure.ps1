$ErrorActionPreference = "Stop";

# system variables
$serverUri = $OctopusParameters["Octopus.Web.ServerUri"];
$tenantId = $OctopusParameters["Octopus.Deployment.Tenant.Id"];
$tenantName = $OctopusParameters["Octopus.Deployment.Tenant.Name"];
$projectId = $OctopusParameters["Octopus.Project.Id"];
$environmentId = $OctopusParameters["Octopus.Environment.Id"];
$environmentName = $OctopusParameters["Octopus.Environment.Name"];

# global variables
$azureLocationString = $OctopusParameters["Hosted.Location"];
$alternateAlias = $OctopusParameters["Hosted.AlternateDomainAlias"];
$domainAlias = "";
$elasticCloudRegion = ($azureLocationString -split ',')[0];
$azureDatabaseHost = ($azureLocationString -split ',')[1];
$azureRegion = ($azureLocationString -split ',')[2];
$azureElasticPoolName = ($azureLocationString -split ',')[3];


# $azureDatabaseHost = $OctopusParameters["Azure.Database.Host"];
$azureDatabaseHostShortName = ($azureDatabaseHost -split '\.')[0];
$azureDatabasePort = $OctopusParameters["Azure.Database.Port"];
$azureDatabaseUser = $OctopusParameters["Azure.Database.User"];
if ($OctopusParameters["Azure.AU.Database.User"]) {
   $azureDatabaseUser = $OctopusParameters["Azure.AU.Database.User"];
   $azureDatabasePassword = $OctopusParameters["Azure.AU.Database.Password"];
}
$azureDatabasePassword = $OctopusParameters["Azure.Database.Password"];
# $azureRegionElasticPoolName = $OctopusParameters["Azure.Regoin"];
# $azureRegion = ($azureRegionElasticPoolName -split '\,')[0];
# $azureElasticPoolName = ($azureRegionElasticPoolName -split '\,')[1];
$elasticCloudApiKey = $OctopusParameters["ElasticCloud.ApiKey"];
# $elasticCloudRegion = $OctopusParameters["ElasticCloud.Region"];
# Removed to retrieve version from Elasticsearch API
# $elasticCloudElasticsearchVersion = $OctopusParameters["ElasticCloud.ElasticsearchVersion"];
# $elasticCloudKibanaVersion = $OctopusParameters["ElasticCloud.KibanaVersion"];
$octopusDeployApiKey = $OctopusParameters["OctopusDeploy.ApiKey"];


# tenant variables
$tenantAlias = $OctopusParameters["Tenant.Alias"];
$domainAlias = if ($alternateAlias -ne "none") { $alternateAlias } else { $tenantAlias };
"Alternate alias is $alternateAlias..."
"Using domain alias $domainAlias..."
"Have tenant alias $tenantAlias..."



# script variables
$resourceName = "{0}-{1}" -f $tenantAlias, $environmentName.ToLower();
$resourceNameDNS = "{0}-{1}" -f $domainAlias, $environmentName.ToLower();

# pre-sanity check
"sanity checking to ensure the the VM disk has not been created in Azure..."
$OSDiskName = az disk list | ConvertFrom-Json | Where-Object {$_.name -Match $resourceName} |  foreach-object {  $_.name };
if ($OSDiskName -eq $resourceName) {
"The OS disk $resourceName that we need to create already exists. Please ensure the resources are cleaned up prior to running this deployment.";
exit -1;
}


# functions
function generatePassword($size) {
    $tokenSets = @(
        [Char[]]"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        [Char[]]"abcdefghijklmnopqrstuvwxyz",
        [Char[]]"0123456789"
    );
    
    $chars = $tokenSets | ForEach-Object { $_ | Get-Random };

    while ($chars.Count -lt $size) {
        $chars += $tokenSets | Get-Random;
    }

    ($chars | Sort-Object { Get-Random }) -join "";
}

function executeSqlCommand($database, $commandText) {
    $sqlConnection = New-Object System.Data.SqlClient.SqlConnection;
    $sqlConnection.ConnectionString = "Server=$azureDatabaseHost,$azureDatabasePort;Database=$database;User=$azureDatabaseUser;Password=$azureDatabasePassword;Encrypt=true";
    $sqlConnection.Open();
    $sqlCommand = $sqlConnection.CreateCommand();
    $sqlCommand.CommandText = $commandText;
    $sqlCommand.ExecuteNonQuery() | Out-Null;
    $sqlCommand.Dispose();
    $sqlConnection.Dispose();
}

function createDatabase($name, $user) {
    az sql db create --resource-group shared-resources --server $azureDatabaseHostShortName --name $name --elastic-pool $azureElasticPoolName | Out-Null;
    executeSqlCommand $name "CREATE USER ""$user"" FOR LOGIN ""$user"" WITH DEFAULT_SCHEMA = dbo; EXEC sp_addrolemember 'db_owner', '$user';";
}

function setTenantVariable($tenantVariables, $name, $value) {
    "Setting tenant variables based on $tenantVariables, $name, $value"
    $id = ($tenantVariables.ProjectVariables.$projectId.Templates | Where-Object { $_.Name -eq $name }).Id;
    "Have ID back as $id"
    if ($null -eq $tenantVariables.ProjectVariables.$projectId.Variables.$environmentId.$id) {
        $tenantVariables.ProjectVariables.$projectId.Variables.$environmentId | Add-Member -MemberType NoteProperty -Name $id -Value $value;
    } else {
        $tenantVariables.ProjectVariables.$projectId.Variables.$environmentId.$id = $value;
    }
}


"creating database login on tenant $tenantAlias on server $azureDatabaseHostShortName ...";
$databaseUser = "{0}-{1}" -f $tenantAlias, $environmentName.ToLower();
$databasePassword = generatePassword 40;
executeSqlCommand "master" "CREATE LOGIN ""$databaseUser"" WITH PASSWORD = '$databasePassword';";

"creating msm database...";
$databaseMsm = "$resourceName-msm";
createDatabase $databaseMsm $databaseUser;

"creating msm audit database...";
$databaseMsmAudit = "$resourceName-msm-audit";
createDatabase $databaseMsmAudit $databaseUser;

"creating elasticsearch instance...";
$elasticsearchDeployment = Invoke-WebRequest https://bitbucket.org/marvalsoftware/marval-cloud/raw/master/Provisioning/Elasticsearch.json -UseBasicParsing | ConvertFrom-Json;
$elasticsearchDeployment.name = $resourceName;
# Updated to retrieve elastic search version from API
$ElasticVersionResponse = Invoke-WebRequest "https://api.elastic-cloud.com/api/v1/regions/$elasticCloudRegion/stack/versions" -Method "GET" -Headers @{ "Authorization" = "ApiKey $elasticCloudApiKey"; "Content-Type" = "application/json" } -UseBasicParsing | ConvertFrom-Json;
$elasticCloudKibanaVersion = $ElasticVersionResponse.stacks  | Where-Object {$_.version -Match "^7\."} |  foreach-object {  $_.version } | Sort -Descending | Select-Object -Index 0;
$elasticCloudElasticsearchVersion = $elasticCloudKibanaVersion;

"using elasticsearch and kibana version $elasticCloudElasticsearchVersion...." ;
$elasticsearchDeployment.resources.elasticsearch.plan.elasticsearch.version = $elasticCloudElasticsearchVersion;
$elasticsearchDeployment.resources.elasticsearch[0].region = $elasticCloudRegion;
$elasticsearchDeployment.resources.kibana.plan.kibana.version = $elasticCloudKibanaVersion;
$elasticsearchDeployment.resources.kibana[0].region = $elasticCloudRegion;
"deploying elasticsearch with deployment definition ";
$elasticsearchDeployment | ConvertTo-Json -Depth 8

$result = Invoke-WebRequest https://api.elastic-cloud.com/api/v1/deployments -Method "POST" -Headers @{ "Authorization" = "ApiKey $elasticCloudApiKey"; "Content-Type" = "application/json" } -Body ($elasticsearchDeployment | ConvertTo-Json -Depth 8) -UseBasicParsing | ConvertFrom-Json;
"result response from elasticsearch deployment..."
$result
$elasticsearchHost, $elasticsearchPort = (([Text.Encoding]::Utf8.GetString([Convert]::FromBase64String($result.resources[0].cloud_id.Split(':')[1])).Split('$') | Select-Object -First 2 | Sort-Object { (--$script:i) }) -join ".").Split(':');
$elasticsearchUsername = $result.resources.credentials.username;
$elasticsearchPassword = $result.resources.credentials.password;


"creating resource group...";
az group create --name $tenantAlias --location $azureRegion | Out-Null;

"creating redis instance...";
$redisName = "{0}-{1}" -f $resourceName, (New-Guid).ToString("N");
$result = az redis create --resource-group $tenantAlias --name $redisName --location $azureRegion --sku Standard --vm-size C0 --minimum-tls-version 1.2 | ConvertFrom-Json;
$redisHost = $result.hostName;
$redisPort = $result.sslPort;
$result = az redis list-keys --resource-group $tenantAlias --name $redisName | ConvertFrom-Json;
$redisPassword = $result.primaryKey;

"creating storage account...";
$awsAccessKey = $null;

while ($true) {
    $awsAccessKey = (New-Guid).ToString("N").Substring(0, 24);
    # $result = az storage account create --resource-group $tenantAlias --name $awsAccessKey --location $azureRegion --sku Standard_LRS 2>&1;
    try {
    # Run the az command
    $result = az storage account create --resource-group $tenantAlias --name $awsAccessKey --location $azureRegion --sku Standard_LRS 2>&1
    Write-Host "Storage account created successfully."
 
    # Check the exit code of the last executed command
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Warning: Non-zero exit code detected ($LASTEXITCODE)."
    }
} catch {
    Write-Host "Error creating storage account: $_"
    # Handle the error as needed
 
    # Set the exit code to 1 if an error occurs
}

    if (!$result -or $result.GetType().Name -ne "ErrorRecord" -or $result.ToString() -ne "ERROR: (StorageAccountAlreadyTaken) The storage account named test is already taken.") {
        break;
    }
}

$awsSecretKey = (az storage account keys list --resource-group $tenantAlias --account-name $awsAccessKey | ConvertFrom-Json)[0].Value;
$awsBucketName = "msm";
az storage container create --name $awsBucketName --account-name $awsAccessKey --account-key $awsSecretKey | Out-Null;

"creating virtual machine...";
az network nsg create --resource-group $tenantAlias --location $azureRegion --name $resourceName | Out-Null;
az network nsg rule create --resource-group $tenantAlias --nsg-name $resourceName --priority 300 --name HTTP --destination-port-range 80 --protocol Tcp | Out-Null;
az network nsg rule create --resource-group $tenantAlias --nsg-name $resourceName --priority 310 --name HTTPS --destination-port-range 443 --protocol Tcp | Out-Null;
$publicIpId = (az network public-ip create --resource-group $tenantAlias --name $resourceName --allocation-method Static | ConvertFrom-Json).publicIp.id;
az network vnet create --resource-group $tenantAlias --name $resourceName --subnet-name $resourceName | Out-Null;
az network nic create --resource-group $tenantAlias --name $resourceName --vnet-name $resourceName --subnet $resourceName --network-security-group $resourceName --public-ip-address $resourceName | Out-Null;
$virtualMachineSize = if ($environmentName -eq "Staging") { "Standard_B2ms" } else { "Standard_B4ms" };
$virtualMachineAdminPassword = generatePassword 40;
$virtualMachineAdminPassword | az vm create --resource-group $tenantAlias --name $resourceName --size $virtualMachineSize --image "MicrosoftWindowsServer:WindowsServer:2022-datacenter-azure-edition:latest" --computer-name $resourceName.Substring(0, [math]::Min($resourceName.Length, 15)) --admin-username $tenantAlias --admin-password "@-" --os-disk-name $resourceName --nics $resourceName | Out-Null;
# $virtualMachineAdminPassword | az vm create --resource-group $tenantAlias --name $resourceName --size $virtualMachineSize --image "win2019datacenter" --computer-name $resourceName.Substring(0, [math]::Min($resourceName.Length, 15)) --admin-username $tenantAlias --admin-password "@-" --os-disk-name $resourceName --nics $resourceName | Out-Null;
# az vm extension set --resource-group $tenantAlias --vm-name $resourceName --publisher Microsoft.Azure.ActiveDirectory --name AADLoginForWindows | Out-Null;

"creating key vault...";
az keyvault create --resource-group $tenantAlias --name $resourceName --enable-rbac-authorization --only-show-errors | Out-Null;
$virtualMachineAdminPassword | az keyvault secret set --vault-name $resourceName --name "virtual-machine-admin-password" --value "@-" | Out-Null;

"creating dns address record...";
$recordSetName = if ($environmentName -eq "Production") { $domainAlias } else { $resourceNameDNS };
$domain = (az network dns record-set a create --resource-group shared-resources --zone-name marval.cloud --name $recordSetName --target-resource $publicIpId | ConvertFrom-Json).fqdn.TrimEnd(".");

"preparing virtual machine...";
(Invoke-WebRequest https://bitbucket.org/marvalsoftware/marval-cloud/raw/master/Provisioning/PrepareVirtualMachine.ps1 -UseBasicParsing).Content | az vm run-command invoke --resource-group $tenantAlias --name $resourceName --command-id RunPowerShellScript --scripts "@-" --parameters "serverUri=$serverUri" "tenantName=$tenantName" "environmentName=$environmentName" "octopusDeployApiKey=$octopusDeployApiKey" "minioGatewayType=azure" "minioRootUser=$awsAccessKey" "minioRootPassword=$awsSecretKey" "virtualMachineName=$resourceName" "domain=$domain" | Out-Null;

while ($true) {
    try {
        "waiting for response...";
        Invoke-WebRequest $domain -TimeoutSec 10 -UseBasicParsing;
    } catch {
        if ($_.Exception.Response) {
            break;
        }
    }
}

"setting octopus deploy variables...";
$tenantVariables = Invoke-WebRequest "$serverUri/api/tenants/$tenantId/variables" -Headers @{ "X-Octopus-ApiKey" = $octopusDeployApiKey } -UseBasicParsing | ConvertFrom-Json;
setTenantVariable $tenantVariables "Domain" $domain;
setTenantVariable $tenantVariables "Database.Host" $azureDatabaseHost;
setTenantVariable $tenantVariables "Database.Port" $azureDatabasePort;
setTenantVariable $tenantVariables "Database.MSM" $databaseMsm;
setTenantVariable $tenantVariables "Database.MSMAudit" $databaseMsmAudit;
setTenantVariable $tenantVariables "Database.User" $databaseUser;
setTenantVariable $tenantVariables "Database.Password" @{ HasValue = $true; NewValue = $databasePassword };
setTenantVariable $tenantVariables "Elasticsearch.Scheme" "https";
setTenantVariable $tenantVariables "Elasticsearch.Host" $elasticsearchHost;
setTenantVariable $tenantVariables "Elasticsearch.Port" $elasticsearchPort;
setTenantVariable $tenantVariables "Elasticsearch.Username" $elasticsearchUsername;
setTenantVariable $tenantVariables "Elasticsearch.Password" @{ HasValue = $true; NewValue = $elasticsearchPassword };
setTenantVariable $tenantVariables "RedisHost" $redisHost;
setTenantVariable $tenantVariables "RedisPort" $redisPort;
setTenantVariable $tenantVariables "RedisPassword" @{ HasValue = $true; NewValue = $redisPassword };
setTenantVariable $tenantVariables "RedisAdditionalParameters" "ssl=True,sslProtocols=Tls12";
setTenantVariable $tenantVariables "AWSAccessKey" $awsAccessKey;
setTenantVariable $tenantVariables "AWSSecretKey" @{ HasValue = $true; NewValue = $awsSecretKey };
setTenantVariable $tenantVariables "AWSServiceURL" "http://127.0.0.1:9000";
setTenantVariable $tenantVariables "AWSForcePathStyle" "True";
setTenantVariable $tenantVariables "AWSBucketName" $awsBucketName;
setTenantVariable $tenantVariables "ClamAVServer" "localhost";
setTenantVariable $tenantVariables "ClamAVServerPort" "3310";
setTenantVariable $tenantVariables "InstallDir" "C:\Program Files\Marval Software\MSM";
setTenantVariable $tenantVariables "SelfServiceUrl" "https://$domain/MSMSelfService";
setTenantVariable $tenantVariables "SignalRUrl" "https://$domain/MSM/signalr";
setTenantVariable $tenantVariables "MsmBaseUrl" "https://$domain/MSM";
Invoke-WebRequest "$serverUri/api/tenants/$tenantId/variables" -Method "PUT" -Headers @{ "X-Octopus-ApiKey" = $octopusDeployApiKey } -Body ($tenantVariables | ConvertTo-Json -Depth 100) -UseBasicParsing | Out-Null;
