# Octopus Variables (passed as parameters or set in Octopus UI)
param (
    [string]$bacpacFile = "#{quickstart:bacpac:name}",    
    [string]$storageaccount = "#{quickstart:storage:name}",
    [string]$container = "#{quickstart:container:name}",
    [string]$sqlServerName = "#{aks:mssql:server:name}.database.windows.net",
    [string]$databaseName = "#{tenant:sql:database}",
    [string]$auditDatabaseName = "#{tenant:sql:audit:database}",
    [string]$sqlAdminUsername = "#{aks:mssql:admin:user}",
    [string]$sqlAdminPassword = "#{aks:mssql:admin:pass}",
    [string]$appUsername = "#{global:database:username}",
    [string]$appPassword = "#{global:database:password}"
)

"Downloading BACPAC..."
if (-not (Test-Path $bacpacfile)) {
    az storage blob download --account-name $storageaccount  -f $bacpacfile -c $container -n $bacpacfile
}

"Importing BACPAC using sqlpackage with admin credentials"
& sqlpackage /Action:Import `
    /TargetConnectionString:"Server=tcp:$sqlServerName,1433;Initial Catalog=$databaseName;User Id=$sqlAdminUsername;Password=$sqlAdminPassword;TrustServerCertificate=True" `
    /SourceFile:$bacpacfile `
    /p:DatabaseEdition=Standard `
    /p:DatabaseServiceObjective=S0

"Check and Create SQL User in master database"
$userCheckQuery = "SET NOCOUNT ON;SELECT name FROM sys.sql_logins WHERE name = '$appUsername'"
$userExists = & "sqlcmd" -S "$sqlServerName,1433" -U $sqlAdminUsername -P $sqlAdminPassword -d "master" -Q $userCheckQuery -h -1
-not($($userExists))

if (-not($userExists)) {
    $createUserInMaster = "CREATE LOGIN [$appUsername] WITH PASSWORD = '$appPassword'"
    & "sqlcmd" -S "$sqlServerName,1433" -U $sqlAdminUsername -P $sqlAdminPassword -d "master" -Q $createUserInMaster
    Write-Output "Created SQL Login: $appUsername in master database"
} else {
    Write-Output "SQL Login $appUsername already exists in master database"
}

"Create or Update User in Application Database with db_owner"
$createUserInAppDb = "
    IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = '$appUsername')
        CREATE USER [$appUsername] FOR LOGIN [$appUsername];
    ALTER ROLE db_owner ADD MEMBER [$appUsername];
"
& "sqlcmd" -S "$sqlServerName,1433" -U $sqlAdminUsername -P $sqlAdminPassword -d $databaseName -Q $createUserInAppDb
Write-Output "Configured $appUsername as db_owner in $databaseName"

& "sqlcmd" -S "$sqlServerName,1433" -U $sqlAdminUsername -P $sqlAdminPassword -d $auditDatabaseName -Q $createUserInAppDb
Write-Output "Configured $appUsername as db_owner in $auditDatabaseName"

"Clean up"
Remove-Item $bacpacfile