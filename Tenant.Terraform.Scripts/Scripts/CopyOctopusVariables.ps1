$octopusURL="https://octopusdeploy.marval.co.uk"
$octopusAPIKey="XXXX"
$spaceName1 = "Azure Deployments"
$spaceName2 = "Marval GO"

$path = Join-Path (Get-Item ((Get-Package Octopus.Client).source)).Directory.FullName "lib/net462/Octopus.Client.dll"
Add-Type -Path $path

$endpoint = New-Object Octopus.Client.OctopusServerEndpoint $octopusURL, $octopusAPIKey
$repository = New-Object Octopus.Client.OctopusRepository $endpoint
$client = New-Object Octopus.Client.OctopusClient $endpoint

$space1 = $repository.Spaces.FindByName($spaceName1)   
$repositoryForSpace1 = $client.ForSpace($space1)

$space2 = $repository.Spaces.FindByName($spaceName2)   
$repositoryForSpace2 = $client.ForSpace($space2)

$librarySets = $repositoryForSpace1.LibraryVariableSets.FindAll()
foreach ($librarySet in $librarySets)
{
    $copyLibrarySet = $repositoryForSpace2.LibraryVariableSets.CreateOrModify($librarySet.Name, $librarySet.Description)    
    
    foreach ($template in $librarySet.Templates)
    {
        $copyLibrarySet.VariableTemplates.AddOrUpdateVariableTemplate($template.Name, $template.Label, $template.DisplaySettings, $template.DefaultValue.Value, $template.HelpText)
    }    
    $copyLibrarySet.Save()
}