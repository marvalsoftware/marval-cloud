
variable "azure_tenant_subscription" {
  default = "#{tenant:subscription:id}"
  type = string
  description = "Tenant Suscription for Resources"
}

variable "location" {
  default = "#{tenant:region}"
  type = string
  description = "Tenant Region where all these resources will be provisioned"
}

variable "aks_mssql_server_resource_group_name" {
  default = "#{aks:mssql:server:resource:group}"
  type = string
  description = "Azure SQL Resource Group Name"  
}

variable "aks_mssql_server_name" {
  default = "#{aks:mssql:server:name}"
  type = string
  description = "Azure SQL Server Name"  
}

variable "aks_mssql_server_pool_resource_group_name" {
  default = "#{aks:mssql:server:pool:resource:group}"
  type = string
  description = "Azure SQL Elastic Pool Resource Group Name"  
}

variable "aks_mssql_server_pool_name" {
  default = "#{aks:mssql:server:pool:name}"
  type = string
  description = "Azure SQL Elastic Pool Name"  
}

variable "mssql_admin_password" {    
  default = "#{aks:mssql:admin:pass}"
  type = string
  description = "Azure SQL Server Admin User Password"   
}

variable "azurerm_resource_group_name" {
  default = "#{tenant:resource:group}"
  type = string
  description = "Tenant Resource Group Name"  
}

variable "azurerm_key_vault_name" {
  default = "#{tenant:key:vault}"
  type = string
  description = "Tenant Key Vault Name"  
}

variable "azurerm_storage_account_name" {
  default = "#{tenant:storage:account}"
  type = string
  description = "Tenant Storage Account Name"  
}

variable "azure_container_name" {
  default = "#{tenant:container:name}"
  type = string
  description = "Tenant Container Name"    
}

variable "azure_sql_database_name" {    
  default = "#{tenant:sql:database}"
  type = string
  description = "Tenant SQL Database name"   
}

variable "azure_sql_database_audit_name" {    
  default = "#{tenant:sql:audit:database}"
  type = string
  description = "Tenant SQL Audit Database name"   
}