output "primary_connection_string" {
  value       = azurerm_storage_account.tenant_storage_account.primary_connection_string
  sensitive   = true
  description = "The primary access key for the storage account."
}