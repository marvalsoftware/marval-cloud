terraform {
  required_version = ">= 1.0"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.18.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "3.1.0"
    }
  }

  # Terraform State Storage to Azure Storage Container
  backend "azurerm" {
    subscription_id       = "#{azure:principle:subscription}"
    client_id             = "#{azure:principle:client}"
    client_secret         = "#{azure:principle:key}"
    tenant_id             = "#{azure:principle:tenant}"
    resource_group_name   = "#{backend:resource:group}"
    storage_account_name  = "#{backend:storage:account}"
    container_name        = "#{backend:container:name}"
    key                   = "#{backend:state:filename}"
  }  
}

provider "azurerm" {
  subscription_id       = "${var.azure_tenant_subscription}"
  features {
     resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

data "azurerm_mssql_server" "mssql" {
  name                = "${var.aks_mssql_server_name}"
  resource_group_name = "${var.aks_mssql_server_resource_group_name}"
}

data "azurerm_mssql_elasticpool" "elasticpool" {
  name                = "${var.aks_mssql_server_pool_name}"
  resource_group_name = data.azurerm_mssql_server.mssql.resource_group_name
  server_name         = data.azurerm_mssql_server.mssql.name
}

# Create a resource group
resource "azurerm_resource_group" "tenant_resource_group" {
  name     = "${var.azurerm_resource_group_name}"
  location = "${var.location}"
}

# Create a storage account
resource "azurerm_storage_account" "tenant_storage_account" {
  name                     = "${var.azurerm_storage_account_name}"
  resource_group_name      = azurerm_resource_group.tenant_resource_group.name
  location                 = azurerm_resource_group.tenant_resource_group.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

# Create a container within the storage account
resource "azurerm_storage_container" "tetant_container" {
  name                  = "${var.azure_container_name}"
  storage_account_id    = azurerm_storage_account.tenant_storage_account.id
  container_access_type = "private"
}

# Create Key Vault
resource "azurerm_key_vault" "tenant_key_vault" {
  name                       = "${var.azurerm_key_vault_name}"
  location                   = azurerm_resource_group.tenant_resource_group.location
  resource_group_name        = azurerm_resource_group.tenant_resource_group.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false
  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id
    storage_permissions = [
      "Get",
      "List",
      "Delete",
      "Set",
      "Update"
    ]

    key_permissions = [
      "Get",
      "List",
      "Delete",      
      "Update"
    ]

    secret_permissions = [
      "Get",
      "List",
      "Delete",
      "Set"
    ]
  }
}

# SQL Database from database Storage (Quick Start Database)
resource "azurerm_mssql_database" "tenant_sql_database" {
  name                = "${var.azure_sql_database_name}"
  server_id           = data.azurerm_mssql_server.mssql.id 
  elastic_pool_id     = data.azurerm_mssql_elasticpool.elasticpool.id  
}

resource "azurerm_mssql_database" "tenant_sql_audit_database" {
  name                = "${var.azure_sql_database_audit_name}"
  server_id           = data.azurerm_mssql_server.mssql.id 
  elastic_pool_id     = data.azurerm_mssql_elasticpool.elasticpool.id
}

# Data sources for tenant and object ID
data "azurerm_client_config" "current" {}