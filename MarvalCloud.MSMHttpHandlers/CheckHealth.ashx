﻿<%@ WebHandler Language="C#" Class="CheckHealth" %>

using System;
using System.Configuration;
using System.Web;
using Amazon;
using Amazon.S3;
using nClam;
using Nest;
using MarvalSoftware.Configuration;
using MarvalSoftware.Drogon.Database;
using MarvalSoftware.Search;
using MarvalSoftware.Servers;

public class CheckHealth : IHttpHandler
{
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.QueryString["accessToken"] == ConfigurationManager.AppSettings["accessToken"])
        {
            try
            {
                DbConnectionHelper.Open("System.Data.SqlClient", ConnectionStringManager.Settings[ConnectionStringManager.DatabaseConnectionString]).Dispose();
                DbConnectionHelper.Open("System.Data.SqlClient", ConnectionStringManager.Settings[ConnectionStringManager.AuditDatabaseConnectionString]).Dispose();
                if (!new ElasticClient(new ConnectionSettings(ElasticSearchManager.connectionPool).DefaultIndex(ConnectionStringManager.Settings[ConnectionStringManager.ElasticSearchIndex])).RootNodeInfo().IsValid) throw new Exception("Could not connect to Elasticsearch");
                if (!RedisConfiguration.TestRedis(RedisConfiguration.RedisConnectionString, TimeSpan.Zero)) throw new Exception("Could not connect to Redis");

                if (ConnectionStrings.AwsEnabled)
                {
                    bool awsForcePathStyle;
                    Boolean.TryParse(ConnectionStringManager.Settings[ConnectionStringManager.AWSForcePathStyle], out awsForcePathStyle);

                    AmazonS3Config amazonS3Config = new AmazonS3Config()
                    {
                        ForcePathStyle = awsForcePathStyle
                    };

                    string awsServiceUrl = ConnectionStringManager.Settings[ConnectionStringManager.AWSServiceUrl];

                    if (!String.IsNullOrEmpty(awsServiceUrl))
                    {
                        amazonS3Config.ServiceURL = awsServiceUrl;
                    }
                    else
                    {
                        amazonS3Config.RegionEndpoint = RegionEndpoint.GetBySystemName(ConnectionStringManager.Settings[ConnectionStringManager.AWSRegion]);
                    }

                    using (IAmazonS3 amazonS3 = new AmazonS3Client(ConnectionStringManager.Settings[ConnectionStringManager.AWSAccessKey], ConnectionStringManager.Settings[ConnectionStringManager.AWSSecretKey], amazonS3Config))
                    {
                        if (!amazonS3.DoesS3BucketExist(ConnectionStringManager.Settings[ConnectionStringManager.AWSBucketName])) throw new Exception("The S3 bucket does not exist");
                    }
                }

                int clamAVServerPort;

                if (!String.IsNullOrEmpty(ConnectionStringManager.ClamAVServer) && Int32.TryParse(ConnectionStringManager.ClamAVServerPort, out clamAVServerPort))
                {
                    new ClamClient(ConnectionStringManager.ClamAVServer, clamAVServerPort).PingAsync().Wait();
                }
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 500;
                context.Response.Write(e.Message + Environment.NewLine + e.StackTrace);
            }
        }
        else
        {
            context.Response.StatusCode = 404;
        }
    }
}
