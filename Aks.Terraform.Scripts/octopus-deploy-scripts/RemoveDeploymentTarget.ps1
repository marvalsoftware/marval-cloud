﻿param
(
    [Parameter(Mandatory)]$packagePath,
	[Parameter(Mandatory)]$octopusURL,
    [Parameter(Mandatory)]$octopusAPIKey,
    [Parameter(Mandatory)]$spaceName,
    [Parameter(Mandatory)]$azureAksClusterName,
    [Parameter(Mandatory)]$tageSetName,
    [Parameter(Mandatory)]$variableName,
    [Parameter(Mandatory)]$nginxProjectName
);

"Install Octopus Deploy Client"
$path = Join-Path $packagePath "lib/net462/Octopus.Client.dll"
Add-Type -Path $path
$endpoint = New-Object Octopus.Client.OctopusServerEndpoint $octopusURL, $octopusAPIKey
$repository = New-Object Octopus.Client.OctopusRepository $endpoint
$client = New-Object Octopus.Client.OctopusClient $endpoint

try
{
    # Get space
    $space = $repository.Spaces.FindByName($spaceName)
    $repositoryForSpace = $client.ForSpace($space)   

    $machine = $repositoryForSpace.Machines.FindByName($azureAksClusterName)
    if ($machine)
    {        
        "Removing deployment target."        
        $repositoryForSpace.Machines.Delete($machine)
    }            

    $tenantTagSet = $repositoryForSpace.TagSets.FindByName($tageSetName)
    if ($tenantTagSet) 
    {
        "Removing tag set."   
        $repositoryForSpace.TagSets.Delete($tenantTagSet)
    }  

    "Done"
}
catch
{
    Write-Host $_.Exception.Message
    throw $_.Exception
}
