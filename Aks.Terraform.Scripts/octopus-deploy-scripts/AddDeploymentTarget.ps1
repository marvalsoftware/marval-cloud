﻿param
(
    [Parameter(Mandatory)]$packagePath,
	[Parameter(Mandatory)]$octopusURL,
    [Parameter(Mandatory)]$octopusAPIKey,
    [Parameter(Mandatory)]$spaceName,
    [Parameter(Mandatory)]$azureServicePrincipalName,
    [Parameter(Mandatory)]$azureResourceGroupName,
    [Parameter(Mandatory)]$azureAksClusterName,
    [Parameter(Mandatory)][array]$environmentNames,
    [Parameter(Mandatory)][array] $roles,
    [Parameter(Mandatory)]$tageSetName,
    [Parameter(Mandatory)][array]$tags,
    [Parameter(Mandatory)]$tagColor,
    [Parameter(Mandatory)]$publicstaticip,
    [Parameter(Mandatory)]$resourceGroupName,    
    [Parameter(Mandatory)]$nodeResourceGroupName
);

"Install Octopus Deploy Client"
$path = Join-Path $packagePath "lib/net462/Octopus.Client.dll"
Add-Type -Path $path
$endpoint = New-Object Octopus.Client.OctopusServerEndpoint $octopusURL, $octopusAPIKey
$repository = New-Object Octopus.Client.OctopusRepository $endpoint
$client = New-Object Octopus.Client.OctopusClient $endpoint

try
{
    # Get space
    $space = $repository.Spaces.FindByName($spaceName)   
    $repositoryForSpace = $client.ForSpace($space)
    $environments = $repositoryForSpace.Environments.FindAll() | Where-Object {$environmentNames -contains $_.Name}
    $azureAccount = $repositoryForSpace.Accounts.FindByName($azureServicePrincipalName)  
    $machine = $repositoryForSpace.Machines.FindByName($azureAksClusterName)
    if (!$machine)
    {       
        $azureKubernetesTarget = New-Object Octopus.Client.Model.Endpoints.KubernetesEndpointResource
        $azureKubernetesTargetAuth = New-Object Octopus.Client.Model.Endpoints.KubernetesAzureAuthenticationResource
        $machine = New-Object Octopus.Client.Model.MachineResource

        $azureKubernetesTargetAuth.AccountId = $azureAccount.Id
        $azureKubernetesTargetAuth.ClusterResourceGroup = $azureResourceGroupName
        $azureKubernetesTargetAuth.ClusterName = $azureAksClusterName
        $azureKubernetesTarget.Authentication = $azureKubernetesTargetAuth        
        $machine.Endpoint = $azureKubernetesTarget
        $machine.Name = $azureAksClusterName        

        foreach ($environment in $environments)
        {
            $machine.EnvironmentIds.Add($environment.Id)
        }

        foreach ($role in $roles)
        {
            $machine.Roles.Add($role)
        }

        $repositoryForSpace.Machines.Create($machine)
    }            

    $machine = $repositoryForSpace.Machines.FindByName($azureAksClusterName)
    $tenantTagSet = $repositoryForSpace.TagSets.FindByName($tageSetName)
    if (!$tenantTagSet) 
    {
        $tenantTagSet = New-Object Octopus.Client.Model.TagSetResource
        $tenantTagSet.Name = $tageSetName
        foreach ($tag in $tags)
        {
           $tagResource = New-Object Octopus.Client.Model.TagResource
           $tagResource.Name = $tag 
           $tagResource.Color = $tagColor
             
           $tenantTagSet.Tags.Add($tagResource)
           $machine.TenantTags.Add($tageSetName + "/" + $tag)
        }    

        $repositoryForSpace.TagSets.Create($tenantTagSet)

        $machine.TenantedDeploymentParticipation = [Octopus.Client.Model.TenantedDeploymentMode]::TenantedOrUntenanted
        $repositoryForSpace.Machines.Modify($machine)
    }  
}
catch
{
    Write-Host $_.Exception.Message
    throw $_.Exception
}