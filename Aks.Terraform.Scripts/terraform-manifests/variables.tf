# Define Input Variables
# 1. Azure Location (CentralUS)
# 2. Azure Resource Group Name 
# 3. Azure AKS Environment Name (Dev, QA, Prod)

# Azure Location
variable "location" {
  default = "#{azure:region}"
  type = string
  description = "Azure Region where all these resources will be provisioned"
}

variable "kubernetes_version" {
  default = "#{aks:version}"
  type = string
  description = "Azure AKS version"
}

# Azure Resource Group Name
variable "resource_group_name" {
  default = "#{azure:resource:group}"
  type = string
  description = "This variable defines the Resource Group"
}

variable "cluster_name" {
  default = "#{aks:cluster:name}"
  type = string
  description = "This variable defines the cluster name"
}

variable "mssql_server_name" {
  default = "#{aks:mssql:name}"
  type = string
  description = "This variable defines the mssql server name"
}

variable "mssql_server_version" {
  default = "#{aks:mssql:version}"
  type = string
  description = "This variable defines the mssql server version"
}

variable "mssql_pool_name" {
  default = "#{aks:mssql:pool:name}"
  type = string
  description = "This variable defines the elastic pool name"
}

variable "mssql_admin_user" {    
  default = "#{aks:mssql:admin:user}"
  type = string
  description = "Azure SQL Server Admin User"   
}

variable "mssql_admin_password" {    
  default = "#{aks:mssql:admin:pass}"
  type = string
  description = "Azure SQL Server Admin User password"   
}

# Azure AKS Environment Name
variable "environment" {
  default = "#{aks:enviroment}"
  type = string  
  description = "This variable defines the Environment"
}

# AKS Input Variables

# SSH Public Key for Linux VMs
variable "ssh_public_key" {
  default = "#{aks:linuxnode:ssh:public:key}"
  description = "This variable defines the SSH Public Key for Linux k8s Worker nodes"  
}

variable "linux_username" {
  default = "#{aks:linuxnode:username}"
  description = "This variable defines the SSH Public Key for Linux k8s Worker nodes"  
}

# Windows Admin Username for k8s worker nodes
variable "windows_admin_username" {
  default = "#{aks:windowsnode:username}"
  type = string
  description = "This variable defines the Windows admin username k8s Worker nodes"  
}

# Windows Admin Password for k8s worker nodes
variable "windows_admin_password" {
  default = "#{aks:windowsnode:password}"
  type = string
  description = "This variable defines the Windows admin password k8s Worker nodes"  
}

variable "linux_vm_size" {
  default = "#{aks:linux:vm:size}"
  type = string
  description = "This variable defines the VM Size to use in the linux scalibility set."  
}

variable "windows_vm_size" {
  default = "#{aks:win:vm:size}"
  type = string
  description = "This variable defines the VM Size to use in the windows scalibility set."  
}

variable "aks_deployment_secret" {
  default = "#{aks:deploy:principle:secret}"
  type = string
  description = "Azure AD user secret for helm deployments"  
}