resource "azurerm_public_ip" "clusterip" {
  name                = "${var.cluster_name}-ip"
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name #azurerm_kubernetes_cluster.aks_cluster.node_resource_group
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_kubernetes_cluster" "aks_cluster" {
  dns_prefix          = "${azurerm_resource_group.aks_rg.name}"
  location            = azurerm_resource_group.aks_rg.location
  name                = "${var.cluster_name}"
  resource_group_name = azurerm_resource_group.aks_rg.name
  kubernetes_version  = var.kubernetes_version
  node_resource_group = "${azurerm_resource_group.aks_rg.name}-nrg"
  sku_tier            = "Standard"
  image_cleaner_enabled = true
  image_cleaner_interval_hours = 48
  
  key_vault_secrets_provider {
    secret_rotation_enabled = true
  }

  default_node_pool {
    name       = "systempool"
    vm_size    = var.linux_vm_size   
    orchestrator_version = var.kubernetes_version
    zones = [1, 2]
    auto_scaling_enabled = true
    os_disk_size_gb      = 100
    max_count            = 10
    min_count            = 1
    type           = "VirtualMachineScaleSets"
    
    node_labels = {
      "nodepool-type" = "system"
      "environment"   = var.environment
      "nodepoolos"    = "linux"
      "app"           = "system-apps"
    }
    tags = {
      "nodepool-type" = "system"
      "environment"   = var.environment
      "nodepoolos"    = "linux"
      "app"           = "system-apps"
    }    
  }

  # Identity (System Assigned or Service Principal)
  identity { type = "SystemAssigned" }

  # Add On Profiles  
  azure_policy_enabled = true
  
  # RBAC and Azure AD Integration Block
  azure_active_directory_role_based_access_control {    
    admin_group_object_ids = [azuread_group.aks_administrators.object_id]    
  }

  # Windows Admin Profile
  windows_profile {
    admin_username            = var.windows_admin_username
    admin_password            = var.windows_admin_password
  }

  # Linux Profile
  linux_profile {
    admin_username = var.linux_username
    ssh_key {
        key_data = "${var.ssh_public_key}"
    }
  }

  # Network Profile
  network_profile {   
    load_balancer_sku = "standard" 
    network_plugin = "azure"
    load_balancer_profile {
      outbound_ip_address_ids = [azurerm_public_ip.clusterip.id]
    }
  }

  # AKS Cluster Tags 
  tags = {
    Environment = var.environment
  }
}