provider "helm" {  
  kubernetes {     
    host = azurerm_kubernetes_cluster.aks_cluster.kube_config[0].host    
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks_cluster.kube_config[0].cluster_ca_certificate)    
    exec {
        api_version = "client.authentication.k8s.io/v1beta1"
        command     = "kubelogin"
        args = [
        "get-token",
        "--login",
        "spn",          
        "--tenant-id",
        azurerm_kubernetes_cluster.aks_cluster.azure_active_directory_role_based_access_control[0].tenant_id,
        "--server-id",
        data.azuread_service_principal.aks.client_id,
        "--client-id",
        data.azuread_application.octopus_deploy.client_id,
        "--client-secret",
        "#{aks:deploy:principle:secret}"
        ]
    }  
  }

  registry {
    url = "oci://registry-1.docker.io/bitnamicharts"
    username = "#{bitnami:feed:user}"
    password = "#{bitnami:feed:pass}"
  }
}

resource "helm_release" "ingress-nginx" {
  name       = "ingress-nginx"  
  repository = "https://kubernetes.github.io/ingress-nginx/"
  chart      = "ingress-nginx"
  namespace  = "ingress-nginx"
  create_namespace = true 
  timeout = 1200
  wait = false
  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.clusterip.ip_address
  }
  set {
    name  = "controller.extraArgs.update-status"
    value = "true"
    type = "string" 
  }
  set {
    name  = "controller.service.annotations.service.beta.kubernetes.io/azure-load-balancer-health-probe-request-path"
    value = "/healthz"
  }  
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"  
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "cert-manager"
  namespace  = "cert-manager"
  create_namespace = true 
  timeout = 1200
  wait = false
  set {
    name  = "controller.nodeSelector.agentpool" 
    value = "linux101"
  }
  set {
    name  = "webhook.nodeSelector.agentpool"
    value = "linux101"    
  }
  set {
    name  = "cainjector.nodeSelector.agentpool"
    value = "linux101"
  }
  set {
    name  = "installCRDs"
    value = "true"
    type = "string"
  } 
}

resource "helm_release" "external-dns" {
  name       = "external-dns"  
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "external-dns"
  namespace  = "external-dns"
  create_namespace = true 
  timeout = 1200
  wait = false
  set {
    name = "nodeSelector.agentpool"
    value = "linux101"
  }
  set {
    name = "provider"
    value = "azure"
  }
  set {
    name = "policy"
    value = "sync"
  }
  set {
    name = "txtOwnerId"
    value = "${azurerm_kubernetes_cluster.aks_cluster.name}"
  }
  set {
    name = "azure.tenantId"
    value = "#{azure:tenantId}"
  }
  set {
    name = "azure.subscriptionId"
    value = "#{azure:subscriptionId}"
  }
  set {
    name = "azure.aadClientId"
    value = "#{azure:aadClientId}"
  }
  set {
    name = "azure.aadClientSecret"
    value = "#{azure:aadClientSecret}"
  }  
  set {
    name = "azure.resourceGroup"
    value = "#{azure:resourceGroup}"
  }
  set {
    name = "azure.cloud"
    value = "AzurePublicCloud"
  }
}

resource "helm_release" "eck-operator" {
  name       = "eck-operator"  
  repository = "https://helm.elastic.co"
  chart      = "eck-operator"
  namespace  = "eck-operator"
  create_namespace = true 
  timeout = 1200
  wait = false
  set {
    name = "nodeSelector.agentpool"
    value = "linux101"
  }
}

resource "helm_release" "clamav" {
  name       = "clamav"  
  repository = "https://wiremind.github.io/wiremind-helm-charts"
  chart      = "clamav"
  namespace  = "clamav"
  create_namespace = true 
  timeout = 1200
  wait = false
  set {
    name = "nodeSelector.agentpool"
    value = "linux101"
  }
}