# We will define 
# 1. Terraform Settings Block
# 1. Required Version Terraform
# 2. Required Terraform Providers
# 3. Terraform Remote State Storage with Azure Storage Account (last step of this section)
# 2. Terraform Provider Block for AzureRM
# 3. Terraform Resource Block: Define a Random Pet Resource

# 1. Terraform Settings Block
terraform {
  # 1. Required Version Terraform
  required_version = ">= 1.0"
  # 2. Required Terraform Providers  
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "4.16.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "3.1.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.6.3"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.17.0"
    }
  }

# Terraform State Storage to Azure Storage Container
  backend "azurerm" {
    subscription_id       = "#{azure:principle:subscription}"
    client_id             = "#{azure:principle:client}"
    client_secret         = "#{azure:principle:key}"
    tenant_id             = "#{azure:principle:tenant}"
    resource_group_name   = "#{backend:resource:group}"
    storage_account_name  = "#{backend:storage:account}"
    container_name        = "#{backend:container:name}"
    key                   = "#{backend:state:filename}"
  }  
}

# 2. Terraform Provider Block for AzureRM
provider "azurerm" {
  subscription_id       = "#{azure:principle:subscription}"
  features {
     resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

# 3. Terraform Resource Block: Define a Random Pet Resource
resource "random_pet" "aksrandom" {
}

data "azuread_application" "octopus_deploy" {
  display_name = "#{aks:deploy:principal}"
}

data "azuread_service_principal" "octopus_deploy_principal" {
  display_name = "#{aks:deploy:principal}"
}

data "azuread_service_principal" "aks" {
  display_name = "Azure Kubernetes Service AAD Server"
}