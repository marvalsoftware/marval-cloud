resource "azurerm_mssql_server" "tenant_sql_server" {
  name                         = "${var.mssql_server_name}"
  resource_group_name          =  azurerm_resource_group.aks_rg.name
  location                     = "${var.location}"
  version                      = "${var.mssql_server_version}"
  administrator_login          = "${var.mssql_admin_user}"
  administrator_login_password = "${var.mssql_admin_password}"
}

resource "azurerm_mssql_firewall_rule" "tenant_mssql_firewall_rule" {
  name                = "${var.mssql_server_name}-allow-azure-ip"  
  server_id         = azurerm_mssql_server.tenant_sql_server.id
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

resource "azurerm_mssql_elasticpool" "tenant_sql_elastic_pool" {  
  name                = "${var.mssql_pool_name}"
  resource_group_name = azurerm_resource_group.aks_rg.name
  location            = "${var.location}"
  server_name         = azurerm_mssql_server.tenant_sql_server.name
  max_size_gb         = 100
  sku {
    name = "StandardPool"
    tier = "Standard"
    capacity = 200
  }
  per_database_settings {
    min_capacity = 0
    max_capacity = 100
  }
}