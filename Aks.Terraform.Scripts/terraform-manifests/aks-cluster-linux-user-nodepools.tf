# Create Linux Azure AKS Node Pool
resource "azurerm_kubernetes_cluster_node_pool" "linux101" {  
  kubernetes_cluster_id = azurerm_kubernetes_cluster.aks_cluster.id
  zones                 = [1, 2]
  auto_scaling_enabled  = true
  max_count             = 100
  min_count             = 1
  mode                  = "User"
  name                  = "linux101"
  orchestrator_version  = var.kubernetes_version
  os_type               = "Linux"
  os_disk_size_gb       = 100
  vm_size               = var.linux_vm_size
  priority              = "Regular"
  node_labels = {
    "nodepool-type" = "user"
    "environment"   = var.environment
    "nodepoolos"    = "linux"
    "applications"  = "java-apps"
    "agentpool"     = "linux101"
  }
  tags = {
    "NodepoolType" = "user"
    "Environment"   = var.environment
    "NodePoolOS"    = "linux"
    "App"           = "java-apps"
  }
}
