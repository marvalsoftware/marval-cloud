# Create Azure AD Group in Active Directory for AKS Admins
resource "azuread_group" "aks_administrators" {
  display_name        = "${azurerm_resource_group.aks_rg.name}-${var.environment}-administrators"
  description = "Azure AKS Kubernetes administrators for the ${azurerm_resource_group.aks_rg.name}-${var.environment} cluster."
  security_enabled = true
}

resource "azuread_group_member" "aks_member" {
  group_object_id  = azuread_group.aks_administrators.object_id
  member_object_id = data.azuread_service_principal.octopus_deploy_principal.object_id
}