# Create Windows Azure AKS Node Pool
resource "azurerm_kubernetes_cluster_node_pool" "win101" {  
  kubernetes_cluster_id = azurerm_kubernetes_cluster.aks_cluster.id
  zones                 = [1, 2]
  auto_scaling_enabled  = true
  max_count             = 100
  min_count             = 1
  mode                  = "User"
  name                  = "win022"
  orchestrator_version  = var.kubernetes_version
  os_type               = "Windows"
  os_disk_size_gb       = 100
  vm_size               = var.windows_vm_size
  priority              = "Regular"
  node_labels = {
    "nodepool-type" = "user"
    "environment"   = var.environment
    "nodepoolos"    = "windows"
    "applications"  = "dotnet2022"
    "agentpool"     = "win022"
  }
  tags = {
    "NodepoolType" = "user"
    "Environment"   = var.environment
    "NodePoolOS"    = "windows"
    "App"           = "dotnet-apps"
  }
}

